# Services

Service | Link
------------ | -------------
REGISTRY-SERVICE | **http://localhost:8761**
SECURITY-SERVICE | **http://localhost:9999**
GATEWAY-SERVICE | **http://localhost:8888**
DATA-INITIALIZER-SERVICE | **http://localhost:7777**
AUTUMN-2020-SERVICE | **http://localhost:8081**
SPRING-2021-SERVICE | **http://localhost:8082**
FRONT-END | **http://localhost:3000**
