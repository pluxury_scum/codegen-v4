import {render, unmountComponentAtNode} from "react-dom";
import MainPageButton from "./MainPageButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderMainPageButton", () => {
  render(<MainPageButton path="http://localhost:8888/path" text="Зима 2061" isServiceEnabled="[работает]" />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:8888/path");
});
