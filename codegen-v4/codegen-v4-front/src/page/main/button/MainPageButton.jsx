const MainPageButton = props => {
  const path = props.path;
  const text = props.text;
  const isServiceEnabled = props.isServiceEnabled === undefined ? "" : props.isServiceEnabled;
  const className = props.className;
  const onClickEvent = props.onClickEvent;

  return (
    <a href={path}>
      <button className={className} type="submit" onClick={onClickEvent}>{`${text} ${isServiceEnabled}`}</button>
    </a>
  );
};

export default MainPageButton;