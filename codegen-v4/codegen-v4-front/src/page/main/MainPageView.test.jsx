import {render, unmountComponentAtNode} from "react-dom";
import MainPageView from "./MainPageView";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../config/token/token";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderMainPageAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<MainPageView />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Осень 2020 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Весна 2021 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("Осень 2020 Swagger UI [отключен]");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("Весна 2021 Swagger UI [отключен]");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("Загрузить данные Security сервиса [отключен]");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("Загрузить данные сервиса Autumn2020 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("Загрузить данные сервиса Spring2021 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("Eureka Dashboard ");
});

it("shouldRenderMainPageA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<MainPageView />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Осень 2020 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Осень 2020 Swagger UI [отключен]");
});

it("shouldRenderMainPageA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<MainPageView />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Осень 2020 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Осень 2020 Swagger UI [отключен]");
});

it("shouldRenderMainPageA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<MainPageView />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Осень 2020 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Осень 2020 Swagger UI [отключен]");
});

it("shouldRenderMainPageS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<MainPageView />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Весна 2021 [отключен]");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Весна 2021 Swagger UI [отключен]");
});