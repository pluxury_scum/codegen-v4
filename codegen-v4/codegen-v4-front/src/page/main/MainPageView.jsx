import {autumn2020MenuPath, spring2021MenuPath} from "../../config/path/front/menu/path";
import {eurekaDashboardPath} from "../../config/path/back/eureka/path";
import {useEffect, useState} from "react";
import axios from "axios";
import ButtonAuthWrapper from "./authwrapper/ButtonAuthWrapper";
import {AUTUMN2020_USERS, getConfigWithAuthorizationHeader, SPRING2021_USERS} from "../../service/security";
import {
  dataInitializerActuatorPath,
  initializeAutumn2020DataPath,
  initializeSecurityDataPath,
  initializeSpring2021DataPath
} from "../../config/path/back/datainitializer/path";
import {autumn2020ActuatorPath, autumn2020SwaggerUiPath} from "../../config/path/back/autumn2020/thirdparty/path";
import {spring2021ActuatorPath, spring2021SwaggerUiPath} from "../../config/path/back/spring2021/thirdparty/path";

const MainPageView = () => {
  const config = getConfigWithAuthorizationHeader();
  const [isAutumn2020ServiceEnabled, setAutumn2020ServiceEnabled] = useState("[отключен]");
  const [isSpring2021ServiceEnabled, setSpring2021ServiceEnabled] = useState("[отключен]");
  const [isDataInitializerServiceEnabled, setDataInitializerServiceEnabled] = useState("[отключен]");

  useEffect(() => {
    axios.get(autumn2020ActuatorPath, config)
      .then(() => setAutumn2020ServiceEnabled("[работает]"));

    axios.get(spring2021ActuatorPath, config)
      .then(() => setSpring2021ServiceEnabled("[работает]"));

    axios.get(dataInitializerActuatorPath, config)
      .then(() => setDataInitializerServiceEnabled("[работает]"));
  });

  return (
    <div>
      <div>
        <ButtonAuthWrapper
          path={autumn2020MenuPath}
          text="Осень 2020"
          isServiceEnabled={isAutumn2020ServiceEnabled}
          rolesAllowed={AUTUMN2020_USERS}
          className="overview"
        />
        <ButtonAuthWrapper
          path={spring2021MenuPath}
          text="Весна 2021"
          isServiceEnabled={isSpring2021ServiceEnabled}
          rolesAllowed={SPRING2021_USERS}
          className="overview"
        />
      </div>
      <div>
        <ButtonAuthWrapper
          path={autumn2020SwaggerUiPath}
          text="Осень 2020 Swagger UI"
          isServiceEnabled={isAutumn2020ServiceEnabled}
          rolesAllowed={AUTUMN2020_USERS}
          className="swagger-ui-button"
        />
        <ButtonAuthWrapper
          path={spring2021SwaggerUiPath}
          text="Весна 2021 Swagger UI"
          isServiceEnabled={isSpring2021ServiceEnabled}
          rolesAllowed={SPRING2021_USERS}
          className="swagger-ui-button"
        />
      </div>
      <div>
        <ButtonAuthWrapper
          text="Загрузить данные Security сервиса"
          isServiceEnabled={isDataInitializerServiceEnabled}
          rolesAllowed={["ROLE_ADMIN"]}
          className="data-initializer-button"
          onClickEvent={() => {
            axios.get(initializeSecurityDataPath, config)
              .then(response => alert(response.data))
              .catch(error => alert(error.response.data));
          }}
        />
        <ButtonAuthWrapper
          text="Загрузить данные сервиса Autumn2020"
          isServiceEnabled={isDataInitializerServiceEnabled}
          rolesAllowed={["ROLE_ADMIN"]}
          className="data-initializer-button"
          onClickEvent={() => {
            axios.get(initializeAutumn2020DataPath, config)
              .then(response => alert(response.data))
              .catch(error => alert(error.response.data));
          }}
        />
        <ButtonAuthWrapper
          text="Загрузить данные сервиса Spring2021"
          isServiceEnabled={isDataInitializerServiceEnabled}
          rolesAllowed={["ROLE_ADMIN"]}
          className="data-initializer-button"
          onClickEvent={() => {
            axios.get(initializeSpring2021DataPath, config)
              .then(response => alert(response.data))
              .catch(error => alert(error.response.data));
          }}
        />
      </div>
      <div>
        <ButtonAuthWrapper
          path={eurekaDashboardPath}
          text="Eureka Dashboard"
          rolesAllowed={["ROLE_ADMIN"]}
          className="eureka-dashboard-button"
        />
      </div>
    </div>
  );
};

export default MainPageView;