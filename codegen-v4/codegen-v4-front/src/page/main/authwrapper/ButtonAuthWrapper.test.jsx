import {render, unmountComponentAtNode} from "react-dom";
import ButtonAuthWrapper from "./ButtonAuthWrapper";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../config/token/token";

let adminContainer = null;
let a20201Container = null;
let a20202Container = null;
let a20203Container = null;
let s2021Container = null;

beforeEach(() => {
  adminContainer = document.createElement("div");
  a20201Container = document.createElement("div");
  a20202Container = document.createElement("div");
  a20203Container = document.createElement("div");
  s2021Container = document.createElement("div");

  document.body.appendChild(adminContainer);
  document.body.appendChild(a20201Container);
  document.body.appendChild(a20202Container);
  document.body.appendChild(a20203Container);
  document.body.appendChild(s2021Container);
});

afterEach(() => {
  unmountComponentAtNode(adminContainer);
  unmountComponentAtNode(a20201Container);
  unmountComponentAtNode(a20202Container);
  unmountComponentAtNode(a20203Container);
  unmountComponentAtNode(s2021Container);

  adminContainer.remove();
  a20201Container.remove();
  a20202Container.remove();
  a20203Container.remove();
  s2021Container.remove();

  adminContainer = null;
  a20201Container = null;
  a20202Container = null;
  a20203Container = null;
  s2021Container = null;
});

const path = "http://localhost:8888/path";
const text = "Зима 2061";
const isServiceEnabled = "[работает]";

it("shouldRenderMainPageButtonAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_ADMIN"]}
    />, adminContainer
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20201"]}
    />, a20201Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20202"]}
    />, a20202Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20203"]}
    />, a20203Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_S2021"]}
    />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderMainPageButtonA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_ADMIN"]}
    />, adminContainer
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20201"]}
    />, a20201Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20202"]}
    />, a20202Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20203"]}
    />, a20203Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_S2021"]}
    />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderMainPageButtonA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_ADMIN"]}
    />, adminContainer
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20201"]}
    />, a20201Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20202"]}
    />, a20202Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20203"]}
    />, a20203Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_S2021"]}
    />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderMainPageButtonA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_ADMIN"]}
    />, adminContainer
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20201"]}
    />, a20201Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20202"]}
    />, a20202Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20203"]}
    />, a20203Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_S2021"]}
    />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderMainPageButtonS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_ADMIN"]}
    />, adminContainer
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20201"]}
    />, a20201Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20202"]}
    />, a20202Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_A20203"]}
    />, a20203Container
  );

  render(
    <ButtonAuthWrapper
      path={path}
      text={text}
      isServiceEnabled={isServiceEnabled}
      rolesAllowed={["ROLE_S2021"]}
    />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0].textContent).toBe("Зима 2061 [работает]");
});
