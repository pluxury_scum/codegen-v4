import {file, manual} from "../../../../../../config/label/tablebutton/spring2021/label";

const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{file}</th>
        <th scope="col">{manual}</th>
      </tr>
    </thead>
  );
};

export default TableHeader;