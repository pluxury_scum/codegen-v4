import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {SPRING2021_USERS} from "../../../../../../../service/security";
import {noOop, withOop} from "../../../../../../../config/label/tablebutton/spring2021/label";
import {cpp} from "../../../../../../../config/label/lang/label";
import {spring2021ExerciseInfoFormPath} from "../../../../../../../config/path/front/spring2021/path";

const CppTableRow = () => {
  return (
    <tr>
      <th scope="row">{cpp}</th>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="false"
          text={noOop}
          rolesAllowed={SPRING2021_USERS}
        />
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="false"
          text={noOop}
          rolesAllowed={SPRING2021_USERS}
        />
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
    </tr>
  );
};

export default CppTableRow;