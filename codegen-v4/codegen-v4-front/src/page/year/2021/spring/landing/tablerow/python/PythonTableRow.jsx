import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {SPRING2021_USERS} from "../../../../../../../service/security";
import {spring2021ExerciseInfoFormPath} from "../../../../../../../config/path/front/spring2021/path";
import {python} from "../../../../../../../config/label/lang/label";
import {noOop, withOop} from "../../../../../../../config/label/tablebutton/spring2021/label";

const PythonTableRow = () => {
  return (
    <tr>
      <th scope="row">{python}</th>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="python"
          file="true"
          oop="false"
          text={noOop}
          rolesAllowed={SPRING2021_USERS}
        />
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="python"
          file="true"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="python"
          file="false"
          oop="false"
          text={noOop}
          rolesAllowed={SPRING2021_USERS}
        />
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="python"
          file="false"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
    </tr>
  );
};

export default PythonTableRow;