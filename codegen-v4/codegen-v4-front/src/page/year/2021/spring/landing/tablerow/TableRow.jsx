import CppTableRow from "./cpp/CppTableRow";
import JavaTableRow from "./java/JavaTableRow";
import PythonTableRow from "./python/PythonTableRow";

const TableRow = props => {
  if (props.lang === "cpp") {
    return <CppTableRow />
  } else if (props.lang === "java") {
    return <JavaTableRow />
  } else if (props.lang === "python") {
    return <PythonTableRow />
  }
};

export default TableRow;