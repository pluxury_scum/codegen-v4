import {render, unmountComponentAtNode} from "react-dom";
import TableHeader from "./TableHeader";
import {file, manual} from "../../../../../../config/label/tablebutton/spring2021/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFirstHeader", () => {
  render(<TableHeader/>, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
});

it("shouldRenderSecondHeader", () => {
  render(<TableHeader/>, container);

  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
});