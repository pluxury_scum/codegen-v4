const TableQuery = props => {
  return (
    <form action={props.path} method="get" target="_blank">
      <input type="hidden" name="lang" value={props.lang} />
      <input type="hidden" name="file" value={props.file} />
      <input type="hidden" name="oop" value={props.oop} />
      <button type="submit" className="btn btn-link landing">{props.text}</button>
    </form>
  );
};

export default TableQuery;