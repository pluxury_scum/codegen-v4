import {render, unmountComponentAtNode} from "react-dom";
import Landing from "./Landing";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../config/token/token";
import {file, manual, noOop, withOop} from "../../../../../config/label/tablebutton/spring2021/label";
import {cpp, java, python} from "../../../../../config/label/lang/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLandingAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(withOop);
});

it("shouldRenderLandingA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(file);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(manual);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(withOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(withOop);
});
