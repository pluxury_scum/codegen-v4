import "../../../../../index.css";
import {Dropdown, DropdownButton} from "react-bootstrap";
import {spring2021InputFileNames} from "../../../../../config/label/inputfilename/spring2021/label";
import {getSpring2021ActionOverviewPath} from "../../../../../config/path/front/spring2021/path";

const InputFileForm = props => {
  const routerSearch = props.location.search;
  const searchParams = new URLSearchParams(routerSearch);
  const lang = searchParams.get("lang");
  const file = searchParams.get("file");
  const oop = searchParams.get("oop");

  return (
    <div>
      <div className="ttl">Выберите файл наблюдений</div>
      <DropdownButton id="dropdown-basic-button" title="Выбрать">
        {
          spring2021InputFileNames.map(inputFileName => {
            const path = getSpring2021ActionOverviewPath(inputFileName, lang, file, oop);
            return (<Dropdown.Item title={inputFileName} href={path}>{inputFileName}</Dropdown.Item>);
          })
        }
      </DropdownButton>
    </div>
  );
};

export default InputFileForm;