import {render, unmountComponentAtNode} from "react-dom";
import InputFileForm from "./InputFileForm";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldGiveInputForm", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=false"
  };

  render(<InputFileForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Выберите файл наблюдений");
});