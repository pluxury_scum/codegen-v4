import {render, unmountComponentAtNode} from "react-dom";
import ExerciseInfo from "./ExerciseInfo";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const satellite1Data = {
  number: 7,
  md: [1, 2, 3, 4, 5],
  td: [6, 7, 8, 9, 10],
  mw: [11, 12, 13, 14, 15],
  tw: [16, 17, 18, 19, 20],
  elevation: [21, 22, 23, 24, 25]
};

const satellite2Data = {
  number: 14,
  md: [26, 27, 28, 29, 30],
  td: [31, 32, 33, 34, 35],
  mw: [36, 37, 38, 39, 40],
  tw: [41, 42, 43, 44, 45],
  elevation: [46, 47, 48, 49, 50]
};

const satellite3Data = {
  number: 21,
  md: [51, 52, 53, 54, 55],
  td: [56, 57, 58, 59, 60],
  mw: [61, 62, 63, 64, 65],
  tw: [66, 67, 68, 69, 70],
  elevation: [71, 72, 73, 74, 75]
};

const inputFileName = "arti_6hours.dat";

it("shouldRenderExerciseInfoTitle", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Дополнительная информация");
});

it("shouldRenderExerciseInfoFileNameLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[0].textContent).toBe("Название файла:arti_6hours.dat");
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent).toBe("Номер первого спутника:7");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent).toBe("Номер второго спутника:14");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent).toBe("Номер третьего спутника:21");
});

it("shouldRenderExerciseInfoSatellite1MdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[2].textContent).toBe("Компонент md первого спутника:1, 2, 3, 4, 5");
});

it("shouldRenderExerciseInfoSatellite1TdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[3].textContent).toBe("Компонент td первого спутника:6, 7, 8, 9, 10");
});

it("shouldRenderExerciseInfoSatellite1MwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[4].textContent).toBe("Компонент mw первого спутника:11, 12, 13, 14, 15");
});

it("shouldRenderExerciseInfoSatellite1TwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[5].textContent).toBe("Компонент tw первого спутника:16, 17, 18, 19, 20");
});

it("shouldRenderExerciseInfoSatellite2MdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[6].textContent).toBe("Компонент md второго спутника:26, 27, 28, 29, 30");
});

it("shouldRenderExerciseInfoSatellite2TdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[7].textContent).toBe("Компонент td второго спутника:31, 32, 33, 34, 35");
});

it("shouldRenderExerciseInfoSatellite2MwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[8].textContent).toBe("Компонент mw второго спутника:36, 37, 38, 39, 40");
});

it("shouldRenderExerciseInfoSatellite2TwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[9].textContent).toBe("Компонент tw второго спутника:41, 42, 43, 44, 45");
});

it("shouldRenderExerciseInfoSatellite3MdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[10].textContent).toBe("Компонент md третьего спутника:51, 52, 53, 54, 55");
});

it("shouldRenderExerciseInfoSatellite3TdLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[11].textContent).toBe("Компонент td третьего спутника:56, 57, 58, 59, 60");
});

it("shouldRenderExerciseInfoSatellite3MwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[12].textContent).toBe("Компонент mw третьего спутника:61, 62, 63, 64, 65");
});

it("shouldRenderExerciseInfoSatellite3TwLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName} />, container
  );

  expect(container.getElementsByClassName("groupval")[13].textContent).toBe("Компонент tw третьего спутника:66, 67, 68, 69, 70");
});

it("shouldRenderExerciseInfoSatellite1ElevationLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[14].textContent).toBe("Углы возвышения первого спутника:21, 22, 23, 24, 25");
});

it("shouldRenderExerciseInfoSatellite2ElevationLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[15].textContent).toBe("Углы возвышения второго спутника:46, 47, 48, 49, 50");
});

it("shouldRenderExerciseInfoSatellite3ElevationLine", () => {
  render(
    <ExerciseInfo satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} inputFileName={inputFileName}/>, container
  );

  expect(container.getElementsByClassName("groupval")[16].textContent).toBe("Углы возвышения третьего спутника:71, 72, 73, 74, 75");
});