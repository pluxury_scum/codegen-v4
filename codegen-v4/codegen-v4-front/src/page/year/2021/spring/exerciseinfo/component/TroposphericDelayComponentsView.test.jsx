import {render, unmountComponentAtNode} from "react-dom";
import TroposphericDelayComponentsView from "./TroposphericDelayComponentsView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTroposphericDelayComponentsView", () => {
  const md = ["1", "2", "3", "4", "5"];
  const td = ["6", "7", "8", "9", "10"];
  const mw = ["11", "12", "13", "14", "15"];
  const tw = ["16", "17", "18", "19", "20"];
  const expectedText = "Компонент md первого спутника:1, 2, 3, 4, 5" +
    "Компонент td первого спутника:6, 7, 8, 9, 10" +
    "Компонент mw первого спутника:11, 12, 13, 14, 15" +
    "Компонент tw первого спутника:16, 17, 18, 19, 20";

  render(<TroposphericDelayComponentsView order={"первого"} md={md} td={td} mw={mw} tw={tw}/>, container);

  expect(container.textContent).toBe(expectedText);
});

it("shouldRenderEmptyTroposphericDelayComponentsView", () => {
  const expectedText = "Компонент md первого спутника:Загрузка..." +
    "Компонент td первого спутника:Загрузка..." +
    "Компонент mw первого спутника:Загрузка..." +
    "Компонент tw первого спутника:Загрузка...";

  render(<TroposphericDelayComponentsView order={"первого"} />, container);

  expect(container.textContent).toBe(expectedText);
});