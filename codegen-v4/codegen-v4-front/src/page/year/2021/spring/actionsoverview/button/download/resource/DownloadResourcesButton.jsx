import {downloadResources} from "../../../../../../../../config/label/overviewbutton/label";
import {spring2021DownloadResourcesPath} from "../../../../../../../../config/path/back/spring2021/task/path";
import DownloadButton from "../../../../../../../../service/api/download/DownloadButton";
import {resourcesFileName} from "../../../../../../../../config/label/outputfilename/label";

const DownloadResourcesButton = () => {
  return (
    <DownloadButton
      path={spring2021DownloadResourcesPath}
      fileName={resourcesFileName}
      label={downloadResources}
    />
  );
};

export default DownloadResourcesButton;