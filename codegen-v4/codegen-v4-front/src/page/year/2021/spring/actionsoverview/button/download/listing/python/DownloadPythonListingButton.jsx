import {downloadMainPython} from "../../../../../../../../../config/label/overviewbutton/label";
import {getSpring2021DownloadListingPath} from "../../../../../../../../../config/path/back/spring2021/task/path";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";
import {pythonMainFileName} from "../../../../../../../../../config/label/outputfilename/label";

const DownloadPythonListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getSpring2021DownloadListingPath(preferenceInfo, inputFileName, pythonMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={pythonMainFileName} label={downloadMainPython} />
  );
};

export default DownloadPythonListingButton;