import DownloadJavaListingButton from "./java/DownloadJavaListingButton";
import DownloadCppListingButton from "./cpp/DownloadCppListingButton";
import DownloadPythonListingButton from "./python/DownloadPythonListingButton";

const DownloadListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const lang = preferenceInfo.lang;
  const inputFileName = props.inputFileName;

  if (lang === "cpp") {
    return <DownloadCppListingButton preferenceInfo={preferenceInfo} inputFileName={inputFileName} />
  } else if (lang === "java") {
    return <DownloadJavaListingButton preferenceInfo={preferenceInfo} inputFileName={inputFileName} />
  } else if (lang === "python") {
    return <DownloadPythonListingButton preferenceInfo={preferenceInfo} inputFileName={inputFileName} />
  }
};

export default DownloadListingButton;