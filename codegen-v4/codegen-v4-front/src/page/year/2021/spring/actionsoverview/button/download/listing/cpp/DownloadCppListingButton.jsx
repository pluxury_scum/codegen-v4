import {downloadMainCpp} from "../../../../../../../../../config/label/overviewbutton/label";
import {getSpring2021DownloadListingPath} from "../../../../../../../../../config/path/back/spring2021/task/path";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";
import {cppMainFileName} from "../../../../../../../../../config/label/outputfilename/label";

const DownloadCppListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getSpring2021DownloadListingPath(preferenceInfo, inputFileName, cppMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={cppMainFileName} label={downloadMainCpp} />
  );
};

export default DownloadCppListingButton;