import {downloadCharts} from "../../../../../../../../config/label/overviewbutton/label";
import DownloadButton from "../../../../../../../../service/api/download/DownloadButton";
import {chartsFileName} from "../../../../../../../../config/label/outputfilename/label";
import {getSpring2021DownloadChartsPath} from "../../../../../../../../config/path/back/spring2021/task/path";

const DownloadChartsButton = props => {
  const inputFileName = props.inputFileName;
  const chartsPath = getSpring2021DownloadChartsPath(inputFileName);

  return (
    <DownloadButton path={chartsPath} fileName={chartsFileName} label={downloadCharts} />
  );
};

export default DownloadChartsButton;