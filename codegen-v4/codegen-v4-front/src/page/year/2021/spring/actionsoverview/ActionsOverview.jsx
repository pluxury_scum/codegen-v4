import "../../../../../index.css";
import DownloadListingButton from "./button/download/listing/DownloadListingButton";
import DownloadChartsButton from "./button/download/chart/DownloadChartsButton";
import DownloadResourcesButton from "./button/download/resource/DownloadResourcesButton";
import GetExerciseInfoButton from "./GetExerciseInfoButton";
import {extractFile, extractLang, extractOop} from "../../../../../service/queryParamsExtraction";

const ActionsOverview = props => {
  const inputFileName = props.match.params.inputFileName;
  const routerSearch = props.location.search;

  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);
  const preferenceInfo = {
    lang, file, oop
  };

  return (
    <div>
      <div className="ttl">
        <p>Весна, 2021 год</p>
        <span>Язык программирования: {lang.charAt(0).toUpperCase() + lang.slice(1)}, </span>
        <span>{file === "file" ? "с чтением файла, " : "без чтения файла, "}</span>
        <span>{oop === "withoop" ? "с ООП" : "без ООП"}</span>
      </div>
      <DownloadListingButton preferenceInfo={preferenceInfo} inputFileName={inputFileName} />
      <DownloadChartsButton inputFileName={inputFileName} />
      <DownloadResourcesButton />
      <GetExerciseInfoButton inputFileName={inputFileName} />
    </div>
  );
};

export default ActionsOverview;