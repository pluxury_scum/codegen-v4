import {render, unmountComponentAtNode} from "react-dom";
import DownloadPythonListingButton from "./DownloadPythonListingButton";
import {downloadMainPython} from "../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPythonDownloadListingButton", () => {
  const preferenceInfo = {
    lang: "cpp",
    file: "file",
    oop: "withoop"
  };

  render(
    <DownloadPythonListingButton
      preferenceInfo={preferenceInfo}
      inputFileName="arti_6hours.dat"
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
});