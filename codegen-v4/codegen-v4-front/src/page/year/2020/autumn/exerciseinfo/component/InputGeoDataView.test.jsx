import {render, unmountComponentAtNode} from "react-dom";
import InputGeoDataView from "./InputGeoDataView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderInputGeoDataView", () => {
  render(
    <InputGeoDataView
      latitude="55.0"
      longitude="162"
      smallerLatitude="52.5"
      biggerLatitude="57.5"
      smallerLongitude="160"
      biggerLongitude="165"
      satelliteNumber="7"
    />, container
  );

  expect(container.getElementsByTagName("DIV")[1].textContent).toBe("Широта города:55.0");
  expect(container.getElementsByTagName("DIV")[2].textContent).toBe("Долгота города:162");
  expect(container.getElementsByTagName("DIV")[3].textContent).toBe("Меньшая широта точек для интерполяции:52.5");
  expect(container.getElementsByTagName("DIV")[4].textContent).toBe("Большая широта точек для интерполяции:57.5");
  expect(container.getElementsByTagName("DIV")[5].textContent).toBe("Меньшая долгота точек для интерполяции:160");
  expect(container.getElementsByTagName("DIV")[6].textContent).toBe("Большая долгота точек для интерполяции:165");
  expect(container.getElementsByTagName("DIV")[7].textContent).toBe("Номер спутника для сбора времени GPS:7");
});

it("shouldRenderEmptyInputGeoDataView", () => {
  render(
    <InputGeoDataView />, container
  );

  expect(container.getElementsByTagName("DIV")[1].textContent).toBe("Широта города:Загрузка...");
  expect(container.getElementsByTagName("DIV")[2].textContent).toBe("Долгота города:Загрузка...");
  expect(container.getElementsByTagName("DIV")[3].textContent).toBe("Меньшая широта точек для интерполяции:Загрузка...");
  expect(container.getElementsByTagName("DIV")[4].textContent).toBe("Большая широта точек для интерполяции:Загрузка...");
  expect(container.getElementsByTagName("DIV")[5].textContent).toBe("Меньшая долгота точек для интерполяции:Загрузка...");
  expect(container.getElementsByTagName("DIV")[6].textContent).toBe("Большая долгота точек для интерполяции:Загрузка...");
  expect(container.getElementsByTagName("DIV")[7].textContent).toBe("Номер спутника для сбора времени GPS:Загрузка...");
});

