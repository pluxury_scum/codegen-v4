import {useState, useEffect} from "react";
import ExerciseInfo from "./ExerciseInfo";
import {extractLatitude, extractLongitude, extractSatelliteNumber} from "../../../../../../service/queryParamsExtraction";
import {getComponentsData} from "../../../../../../service/api/autumn2020/var3/getComponentsData";

const FetchAutumn2020Var3ExerciseInfo = props => {
  const [componentsData, setComponentsData] = useState({
    interpolationCoordinates: {},
    gpsTime: [],
    alpha: [],
    beta: [],
    forecastTecValues: [],
    preciseTecValues: []
  });

  const [inputData, setInputData] = useState({});
  
  const latitude = extractLatitude(props.routerSearch);
  const longitude = extractLongitude(props.routerSearch);
  const satelliteNumber = extractSatelliteNumber(props.routerSearch);

  useEffect(() => {
    getComponentsData(latitude, longitude, satelliteNumber)
      .then(data => {
        setComponentsData({
          interpolationCoordinates: data.interpolationCoordinates,
          gpsTime: data.gpsTime,
          alpha: data.alpha.ionCoefficients,
          beta: data.beta.ionCoefficients,
          forecastTecValues: data.forecastTecValues,
          preciseTecValues: data.preciseTecValues
        });
        setInputData({
          latitude: latitude,
          longitude: longitude,
          satelliteNumber: satelliteNumber
        });
      });
  }, [latitude, longitude, satelliteNumber]);

  return <ExerciseInfo componentsData={componentsData} inputData={inputData}/>;
};

export default FetchAutumn2020Var3ExerciseInfo;