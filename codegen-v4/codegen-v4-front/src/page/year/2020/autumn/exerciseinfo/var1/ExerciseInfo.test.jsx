import {render, unmountComponentAtNode} from "react-dom";
import ExerciseInfo from "./ExerciseInfo";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const inputFileName = "POTS_6hours.dat";

const satellite1Data = {
  number: 7,
  p1: ["1", "2", "3", "4", "5"],
  p2: ["6", "7", "8", "9", "10"]
};

const satellite2Data = {
  number: 14,
  p1: ["11", "12", "13", "14", "15"],
  p2: ["16", "17", "18", "19", "20"]
};

const satellite3Data = {
  number: 21,
  p1: ["21", "22", "23", "24", "25"],
  p2: ["26", "27", "28", "29", "30"]
};

it("shouldRenderExerciseInfoTitle", () => {
  render(
    <ExerciseInfo varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Дополнительная информация");
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const inputData = container.getElementsByClassName("groupval")[0];

  expect(inputData.getElementsByTagName("DIV")[0].textContent).toBe("Номер варианта:1");
  expect(inputData.getElementsByTagName("DIV")[1].textContent).toBe("Название файла:POTS_6hours.dat");
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent).toBe("Номер первого спутника:7");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent).toBe("Номер второго спутника:14");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent).toBe("Номер третьего спутника:21");
});

it("shouldRenderExerciseInfoSatellite1P1", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[2].textContent).toBe("P1 первого спутника:1, 2, 3, 4, 5");
});

it("shouldRenderExerciseInfoSatellite1P2", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[3].textContent).toBe("P2 первого спутника:6, 7, 8, 9, 10");
});

it("shouldRenderExerciseInfoSatellite2P1", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[4].textContent).toBe("P1 второго спутника:11, 12, 13, 14, 15");
});

it("shouldRenderExerciseInfoSatellite2P2", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[5].textContent).toBe("P2 второго спутника:16, 17, 18, 19, 20");
});

it("shouldRenderExerciseInfoSatellite3P1", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[6].textContent).toBe("P1 третьего спутника:21, 22, 23, 24, 25");
});

it("shouldRenderExerciseInfoSatellite3P2", () => {
  render(
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[7].textContent).toBe("P2 третьего спутника:26, 27, 28, 29, 30");
});