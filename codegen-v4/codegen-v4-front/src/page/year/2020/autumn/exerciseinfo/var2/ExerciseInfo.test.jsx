import {render, unmountComponentAtNode} from "react-dom";
import ExerciseInfo from "./ExerciseInfo";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const inputFileName = "POTS_6hours.dat";

const satellite1Data = {
  number: 7,
  elevation: ["1", "2", "3", "4", "5"]
};

const satellite2Data = {
  number: 14,
  elevation: ["6", "7", "8", "9", "10"]
};

const satellite3Data = {
  number: 21,
  elevation: ["11", "12", "13", "14", "15"]
};

it("shouldRenderExerciseInfoTitle", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Дополнительная информация");
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const inputData = container.getElementsByClassName("groupval")[0];

  expect(inputData.getElementsByTagName("DIV")[0].textContent).toBe("Номер варианта:2");
  expect(inputData.getElementsByTagName("DIV")[1].textContent).toBe("Название файла:POTS_6hours.dat");
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent).toBe("Номер первого спутника:7");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent).toBe("Номер второго спутника:14");
  expect(satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent).toBe("Номер третьего спутника:21");
});

it("shouldRenderExerciseInfoSatellite1Elevation", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[2].textContent).toBe("Углы возвышения первого спутника:1, 2, 3, 4, 5");
});

it("shouldRenderExerciseInfoSatellite2Elevation", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[3].textContent).toBe("Углы возвышения второго спутника:6, 7, 8, 9, 10");
});

it("shouldRenderExerciseInfoSatellite3Elevation", () => {
  render(
    <ExerciseInfo
      varNumber="2"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  expect(container.getElementsByClassName("groupval")[4].textContent).toBe("Углы возвышения третьего спутника:11, 12, 13, 14, 15");
});