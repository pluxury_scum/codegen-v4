import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const TecValuesView = props => {
  let tecValues = null;

  if (props.tecValues !== undefined) {
    tecValues = getValuesDividedByComma(props.tecValues.tec);
  }

  return (
    <div>Величина TEC точки {props.pointName} {props.fileType} файла:
      <span className="inf">{tecValues === null ? "Загрузка..." : tecValues}</span>
    </div>
  );
};

export default TecValuesView;