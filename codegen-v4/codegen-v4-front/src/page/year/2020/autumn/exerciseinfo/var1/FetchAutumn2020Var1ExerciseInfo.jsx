import {useEffect, useState} from "react";
import ExerciseInfo from "./ExerciseInfo";
import {getSatellitesData} from "../../../../../../service/api/autumn2020/var1/getSatellitesData";

const FetchAutumn2020Var1ExerciseInfo = props => {
  const inputFileName = props.match.params.inputFileName;
  const [satellite1Data, setSatellite1Data] = useState({});
  const [satellite2Data, setSatellite2Data] = useState({});
  const [satellite3Data, setSatellite3Data] = useState({});

  useEffect(() => {
    getSatellitesData(inputFileName).then(satellitesData => {
      const satellite1 = satellitesData.satellite1;
      setSatellite1Data({
        number: satellite1.number,
        p1: satellite1.p1,
        p2: satellite1.p2
      });
      const satellite2 = satellitesData.satellite2;
      setSatellite2Data({
        number: satellite2.number,
        p1: satellite2.p1,
        p2: satellite2.p2
      });
      const satellite3 = satellitesData.satellite3;
      setSatellite3Data({
        number: satellite3.number,
        p1: satellite3.p1,
        p2: satellite3.p2
      });
    });
  }, [inputFileName]);

  return (
    <ExerciseInfo
      varNumber="1"
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />
  );
};

export default FetchAutumn2020Var1ExerciseInfo;