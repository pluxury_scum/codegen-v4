const VarNumberView = props => {
  return (
    <div>Номер варианта:
      <span className="inf">{props.varNumber}</span>
    </div>
  );
};

export default VarNumberView;