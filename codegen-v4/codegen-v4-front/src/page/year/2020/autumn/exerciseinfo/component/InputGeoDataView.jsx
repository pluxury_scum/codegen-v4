const InputGeoDataView = props => {
  let latitude = null;
  let longitude = null;
  let smallerLatitude = null;
  let biggerLatitude = null;
  let smallerLongitude = null;
  let biggerLongitude = null;
  let satelliteNumber = null;

  const areAllValuesDefined = () => {
    return props.latitude !== undefined && props.longitude !== undefined &&
      props.smallerLatitude !== undefined && props.biggerLatitude !== undefined &&
      props.smallerLongitude !== undefined && props.biggerLongitude !== undefined &&
      props.satelliteNumber !== undefined;
  };

  if (areAllValuesDefined()) {
    latitude = props.latitude;
    longitude = props.longitude;
    smallerLatitude = props.smallerLatitude;
    biggerLatitude = props.biggerLatitude;
    smallerLongitude = props.smallerLongitude;
    biggerLongitude = props.biggerLongitude;
    satelliteNumber = props.satelliteNumber;
  }

  return (
    <div>
      <div>Широта города:
        <span className="inf">{latitude === null ? "Загрузка..." : latitude}</span>
      </div>
      <div>Долгота города:
        <span className="inf">{longitude === null ? "Загрузка..." : longitude}</span>
      </div>
      <div>Меньшая широта точек для интерполяции:
        <span className="inf">{smallerLatitude === null ? "Загрузка..." : smallerLatitude}</span>
      </div>
      <div>Большая широта точек для интерполяции:
        <span className="inf">{biggerLatitude === null ? "Загрузка..." : biggerLatitude}</span>
      </div>
      <div>Меньшая долгота точек для интерполяции:
        <span className="inf">{smallerLongitude === null ? "Загрузка..." : smallerLongitude}</span>
      </div>
      <div>Большая долгота точек для интерполяции:
        <span className="inf">{biggerLongitude === null ? "Загрузка..." : biggerLongitude}</span>
      </div>
      <div>Номер спутника для сбора времени GPS:
        <span className="inf">{satelliteNumber === null ? "Загрузка..." : satelliteNumber}</span>
      </div>
    </div>
  );
};

export default InputGeoDataView;