import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const PseudoRangesView = props => {
  let p1 = null;
  let p2 = null;

  if (props.p1 !== undefined && props.p2 !== undefined) {
    p1 = getValuesDividedByComma(props.p1);
    p2 = getValuesDividedByComma(props.p2);
  }

  return (
    <div>
      <div className="groupval">P1 {props.order} спутника:
        <div className="inf">{p1 === null ? "Загрузка..." : p1}</div>
      </div>
      <div className="groupval">P2 {props.order} спутника:
        <div className="inf">{p2 === null ? "Загрузка..." : p2}</div>
      </div>
    </div>
  );
};

export default PseudoRangesView;