import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const GpsTimeView = props => {
  let gpsTime = null;

  if (props.gpsTime !== undefined) {
    gpsTime = getValuesDividedByComma(props.gpsTime.gpsTime);
  }

  return (
    <div>Массив времен GPS:
      <span className="inf">{gpsTime === null ? "Загрузка..." : gpsTime}</span>
    </div>
  );
};

export default GpsTimeView;