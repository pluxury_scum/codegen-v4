const InputFileNameView = props => {
  return (
    <div>Название файла:
      <span className="inf">{props.inputFileName}</span>
    </div>
  );
};

export default InputFileNameView;