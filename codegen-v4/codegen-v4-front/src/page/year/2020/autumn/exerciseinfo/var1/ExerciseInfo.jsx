import "../../../../../../index.css";
import VarNumberView from "../component/VarNumberView";
import SatelliteInfoView from "../component/SatelliteInfoView";
import PseudoRangesView from "../component/PseudoRangesView";
import InputFileNameView from "../component/InputFileNameView";

const ExerciseInfo = props => {
  const satellite1 = props.satellite1Data;
  const satellite2 = props.satellite2Data;
  const satellite3 = props.satellite3Data;

  return (
    <div>
      <div className="ttl">Дополнительная информация</div>
      <div className="groupval">
        <VarNumberView varNumber="1" />
        <InputFileNameView inputFileName={props.inputFileName} />
      </div>
      <div className="groupval">
        <SatelliteInfoView order="первого" number={satellite1.number} />
        <SatelliteInfoView order="второго" number={satellite2.number} />
        <SatelliteInfoView order="третьего" number={satellite3.number} />
      </div>
      <PseudoRangesView order="первого" p1={satellite1.p1} p2={satellite1.p2}/>
      <PseudoRangesView order="второго" p1={satellite2.p1} p2={satellite2.p2}/>
      <PseudoRangesView order="третьего" p1={satellite3.p1} p2={satellite3.p2}/>
    </div>
  );
};

export default ExerciseInfo;