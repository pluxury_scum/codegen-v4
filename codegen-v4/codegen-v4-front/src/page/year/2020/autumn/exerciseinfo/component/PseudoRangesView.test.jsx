import {render, unmountComponentAtNode} from "react-dom";
import PseudoRangesView from "./PseudoRangesView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPseudoRangesView", () => {
  const p1 = ["1", "2", "3", "4", "5"];
  const p2 = ["6", "7", "8", "9", "10"];

  render(<PseudoRangesView order="первого" p1={p1} p2={p2} />, container);

  expect(container.getElementsByClassName("groupval")[0].textContent).toBe("P1 первого спутника:1, 2, 3, 4, 5");
  expect(container.getElementsByClassName("groupval")[1].textContent).toBe("P2 первого спутника:6, 7, 8, 9, 10");
});

it("shouldRenderEmptyPseudoRangesView", () => {
  render(<PseudoRangesView order="первого" />, container);

  expect(container.getElementsByClassName("groupval")[0].textContent).toBe("P1 первого спутника:Загрузка...");
  expect(container.getElementsByClassName("groupval")[1].textContent).toBe("P2 первого спутника:Загрузка...");
});