import {checkUserAccess} from "../../../../../../service/security";
import TableQuery from "./TableQuery";

const TableQueryAuthWrapper = props => {
  const path = props.path;
  const text = props.text;
  const rolesAllowed = props.rolesAllowed;

  const lang = props.lang;
  const file = props.file;
  const oop = props.oop;
  const preferenceInfo = {
    lang, file, oop
  };

  const hasAccess = checkUserAccess(rolesAllowed);

  if (hasAccess) {
    return <TableQuery path={path} text={text} preferenceInfo={preferenceInfo} />
  } else {
    return <></>
  }
};

export default TableQueryAuthWrapper;