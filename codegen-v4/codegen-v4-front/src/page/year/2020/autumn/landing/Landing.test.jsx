import {render, unmountComponentAtNode} from "react-dom";
import Landing from "./Landing";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../config/token/token";
import {var1, var2, var3} from "../../../../../config/label/var/label";
import {cpp, java, python} from "../../../../../config/label/lang/label";
import {fileNoOop, fileWithOop, manualNoOop, manualWithOop} from "../../../../../config/label/tablebutton/autumn2020/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLandingAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(var1);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(var2);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(var3);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[6].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[10].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[11].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[12].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[13].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[14].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[15].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[16].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[17].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[18].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[19].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[20].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[21].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[22].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[23].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[24].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[25].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[26].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[27].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[28].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[29].textContent).toBe(fileWithOop);
});

it("shouldRenderLandingA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(var1);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(var2);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(var3);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[6].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(fileWithOop);
});

it("shouldRenderLandingA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(var1);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(var2);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(var3);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[6].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(fileWithOop);
});

it("shouldRenderLandingA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(var1);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(var2);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(var3);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[6].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe(fileWithOop);
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe(fileWithOop);
});

it("shouldRenderLandingS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe(var1);
  expect(container.getElementsByTagName("TH")[2].textContent).toBe(var2);
  expect(container.getElementsByTagName("TH")[3].textContent).toBe(var3);
  expect(container.getElementsByTagName("TH")[4].textContent).toBe(cpp);
  expect(container.getElementsByTagName("TH")[5].textContent).toBe(python);
  expect(container.getElementsByTagName("TH")[6].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});