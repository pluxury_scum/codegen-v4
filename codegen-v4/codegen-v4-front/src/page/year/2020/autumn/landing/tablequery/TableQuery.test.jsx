import {render, unmountComponentAtNode} from "react-dom";
import TableQuery from "./TableQuery";
import {manualWithOop} from "../../../../../../config/label/tablebutton/autumn2020/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTableQuery", () => {
  const preferenceInfo = {
    lang: "cpp",
    file: "file",
    oop: "withoop"
  };

  render(
    <TableQuery
      path="http://localhost:8888/path"
      text={manualWithOop}
      preferenceInfo={preferenceInfo}
    />, container
  );

  expect(container.getElementsByTagName("FORM")[0].action).toBe("http://localhost:8888/path");
  expect(container.getElementsByTagName("INPUT")[0].name).toBe("lang");
  expect(container.getElementsByTagName("INPUT")[0].value).toBe("cpp");
  expect(container.getElementsByTagName("INPUT")[1].name).toBe("file");
  expect(container.getElementsByTagName("INPUT")[1].value).toBe("file");
  expect(container.getElementsByTagName("INPUT")[2].name).toBe("oop");
  expect(container.getElementsByTagName("INPUT")[2].value).toBe("withoop");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
});