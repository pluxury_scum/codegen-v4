import {render, unmountComponentAtNode} from "react-dom";
import TableRow from "./TableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../config/token/token";
import {cpp, java, python} from "../../../../../../config/label/lang/label";
import {fileNoOop, fileWithOop, manualNoOop, manualWithOop} from "../../../../../../config/label/tablebutton/autumn2020/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderCppTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderCppTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderCppTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderCppTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderJavaTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(python);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderPythonTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(python);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderPythonTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(python);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderPythonTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(python);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualNoOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(fileNoOop);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(fileWithOop);
});

it("shouldRenderPythonTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(python);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});
