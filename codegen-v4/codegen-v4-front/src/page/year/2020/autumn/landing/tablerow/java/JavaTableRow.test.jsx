import {render, unmountComponentAtNode} from "react-dom";
import JavaTableRow from "./JavaTableRow";
import {adminAccessToken, a20201AccessToken, a20202AccessToken, a20203AccessToken, s2021AccessToken} from "../../../../../../../config/token/token";
import {java} from "../../../../../../../config/label/lang/label";
import {fileWithOop, manualWithOop} from "../../../../../../../config/label/tablebutton/autumn2020/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderJavaTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(manualWithOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(fileWithOop);
});

it("shouldRenderJavaTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(java);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});