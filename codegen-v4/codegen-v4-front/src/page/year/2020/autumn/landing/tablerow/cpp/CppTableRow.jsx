import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {AUTUMN2020_VAR1_USERS, AUTUMN2020_VAR2_USERS, AUTUMN2020_VAR3_USERS} from "../../../../../../../service/security";
import {autumn2020Var1ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var1/path";
import {autumn2020Var2ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var2/path";
import {autumn2020Var3ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var3/path";
import {cpp} from "../../../../../../../config/label/lang/label";
import {fileNoOop, fileWithOop, manualNoOop, manualWithOop} from "../../../../../../../config/label/tablebutton/autumn2020/label";

const CppTableRow = () => {
  return (
    <tr>
      <th scope="row">{cpp}</th>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="false"
          text={manualNoOop}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="true"
          text={manualWithOop} rolesAllowed={AUTUMN2020_VAR1_USERS} />
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="false"
          text={fileNoOop}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="false"
          text={manualNoOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="true"
          text={manualWithOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="false"
          text={fileNoOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="false"
          text={manualNoOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="cpp"
          file="false"
          oop="true"
          text={manualWithOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="false"
          text={fileNoOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="cpp"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
      </td>
    </tr>
  );
};

export default CppTableRow;