import {render, unmountComponentAtNode} from "react-dom";
import Autumn2020Var3ExtraInfoForm from "./Autumn2020Var3ExtraInfoForm";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTitle", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=true"
  };

  render(<Autumn2020Var3ExtraInfoForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите координаты города и координаты точек для интерполяции");
  expect(container.getElementsByTagName("LABEL")[0].textContent).toBe("Широта города");
  expect(container.getElementsByTagName("LABEL")[1].textContent).toBe("Долгота города");
  expect(container.getElementsByTagName("LABEL")[2].textContent).toBe("Номер спутника для сбора времени GPS");
});