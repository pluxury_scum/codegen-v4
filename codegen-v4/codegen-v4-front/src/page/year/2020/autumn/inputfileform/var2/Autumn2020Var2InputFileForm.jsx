import {Dropdown, DropdownButton} from "react-bootstrap";
import {autumn2020InputFileNames} from "../../../../../../config/label/inputfilename/autumn2020/label";
import {getAutumn2020Var2ActionOverviewPath} from "../../../../../../config/path/front/autumn2020/var2/path";

const Autumn2020Var2InputFileForm = props => {
  const routerSearch = props.location.search;
  const searchParams = new URLSearchParams(routerSearch);

  const lang = searchParams.get("lang");
  const file = searchParams.get("file");
  const oop = searchParams.get("oop");
  const preferenceInfo = {
    lang, file, oop
  };

  return (
    <div>
      <div className="ttl">Выберите файл наблюдений</div>
      <DropdownButton id="dropdown-basic-button" title="Выбрать">
        {
          autumn2020InputFileNames.map(inputFileName => {
            const path = getAutumn2020Var2ActionOverviewPath(preferenceInfo, inputFileName);
            return (<Dropdown.Item title={inputFileName} href={path}>{inputFileName}</Dropdown.Item>);
          })
        }
      </DropdownButton>
    </div>
  );
};

export default Autumn2020Var2InputFileForm;