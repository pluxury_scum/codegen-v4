const Autumn2020Var3ExtraInfoForm = props => {
  const routerSearch = props.location.search;
  const searchParams = new URLSearchParams(routerSearch);
  const lang = searchParams.get("lang");
  const file = searchParams.get("file");
  const oop = searchParams.get("oop");

  return (
    <form action="actions-overview" method="get">
      <input type="hidden" name="var" value="3" />
      <input type="hidden" name="lang" value={lang} />
      <input type="hidden" name="file" value={file} />
      <input type="hidden" name="oop" value={oop} />
      <div className="ttl">Введите координаты города и координаты точек для интерполяции</div>
      <div>
        <label htmlFor="lat">Широта города</label>
        <input name="lat" type="text" id="lat" placeholder="Введите текст" required={true} />
      </div>
      <div>
        <label htmlFor="lon">Долгота города</label>
        <input name="lon" type="text" id="lon" placeholder="Введите текст" required={true} />
      </div>
      <div>
        <label htmlFor="satnum">Номер спутника для сбора времени GPS</label>
        <input name="satnum" type="text" id="satnum" placeholder="Введите текст" required={true} />
      </div>
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
};

export default Autumn2020Var3ExtraInfoForm;