import DownloadCppListingButton from "./cpp/DownloadCppListingButton";
import DownloadJavaListingButton from "./java/DownloadJavaListingButton";
import DownloadPythonListingButton from "./python/DownloadPythonListingButton";

const DownloadListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const lang = preferenceInfo.lang;
  const extraInfo = props.extraInfo;

  if (lang === "cpp") {
    return <DownloadCppListingButton preferenceInfo={preferenceInfo} extraInfo={extraInfo} />
  } else if (lang === "java") {
    return <DownloadJavaListingButton preferenceInfo={preferenceInfo} extraInfo={extraInfo} />
  } else if (lang === "python") {
    return <DownloadPythonListingButton preferenceInfo={preferenceInfo} extraInfo={extraInfo} />
  }
};

export default DownloadListingButton;