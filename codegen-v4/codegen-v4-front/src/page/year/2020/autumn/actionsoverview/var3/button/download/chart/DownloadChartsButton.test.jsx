import {render, unmountComponentAtNode} from "react-dom";
import DownloadChartsButton from "./DownloadChartsButton";
import {downloadCharts} from "../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadChartsButton", () => {
  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(<DownloadChartsButton extraInfo={extraInfo} />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadCharts);
});