import {downloadMainPython} from "../../../../../../../../../../config/label/overviewbutton/label";
import {pythonMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import {getAutumn2020Var2DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var2/path";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadPythonListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getAutumn2020Var2DownloadListingPath(preferenceInfo, inputFileName, pythonMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={pythonMainFileName} label={downloadMainPython} />
  );
};

export default DownloadPythonListingButton;