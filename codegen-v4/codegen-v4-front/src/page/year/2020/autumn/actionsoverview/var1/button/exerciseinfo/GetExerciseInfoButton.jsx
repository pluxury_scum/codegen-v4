import {getAutumn2020Var1ExerciseInfoPagePath} from "../../../../../../../../config/path/front/autumn2020/var1/path";
import {getExerciseInfo} from "../../../../../../../../config/label/overviewbutton/label";

const GetExerciseInfoButton = props => {
  const inputFileName = props.inputFileName;
  const exerciseInfoPath = getAutumn2020Var1ExerciseInfoPagePath(inputFileName);

  return (
    <a href={exerciseInfoPath}>
      <button className="overview" type="submit">{getExerciseInfo}</button>
    </a>
  );
};

export default GetExerciseInfoButton;