import {getAutumn2020Var3ExerciseInfoPagePath} from "../../../../../../../../config/path/front/autumn2020/var3/path";
import {getExerciseInfo} from "../../../../../../../../config/label/overviewbutton/label";

const GetExerciseInfoButton = props => {
  const extraInfo = props.extraInfo;
  const exerciseInfoPath = getAutumn2020Var3ExerciseInfoPagePath(extraInfo);

  return (
    <a href={exerciseInfoPath}>
      <button className="overview" type="submit">{getExerciseInfo}</button>
    </a>
  );
};

export default GetExerciseInfoButton;