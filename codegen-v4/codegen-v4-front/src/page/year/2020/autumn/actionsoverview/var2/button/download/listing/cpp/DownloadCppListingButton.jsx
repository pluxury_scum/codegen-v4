import {downloadMainCpp} from "../../../../../../../../../../config/label/overviewbutton/label";
import {cppMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import {getAutumn2020Var2DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var2/path";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadCppListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getAutumn2020Var2DownloadListingPath(preferenceInfo, inputFileName, cppMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={cppMainFileName} label={downloadMainCpp} />
  );
};

export default DownloadCppListingButton;