import {render, unmountComponentAtNode} from "react-dom";
import Autumn2020Var1ActionsOverview from "./Autumn2020Var1ActionsOverview";
import {
  downloadCharts,
  downloadGraphDrawerJava,
  downloadMainCpp,
  downloadMainJava,
  downloadMainPython,
  downloadReference,
  downloadResources,
  getExerciseInfo
} from "../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderValuesForVar1CppManualNoOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=false"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1CppManualWithOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1CppFileNoOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=false"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1CppFileWithOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1PythonManualNoOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=false"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1PythonManualWithOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1PythonFileNoOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=false"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1PythonFileWithOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1JavaManualWithOop", () => {
  const location = {
    search: "?lang=java&file=false&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainJava);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadGraphDrawerJava);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(getExerciseInfo);
});

it("shouldRenderValuesForVar1JavaFileWithOop", () => {
  const location = {
    search: "?lang=java&file=true&oop=true"
  };

  const match = {
    params: {
      inputFileName: "POTS_6hours.dat"
    }
  };

  render(<Autumn2020Var1ActionsOverview location={location} match={match} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainJava);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadGraphDrawerJava);
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe(downloadCharts);
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe(downloadResources);
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe(downloadReference);
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe(getExerciseInfo);
});