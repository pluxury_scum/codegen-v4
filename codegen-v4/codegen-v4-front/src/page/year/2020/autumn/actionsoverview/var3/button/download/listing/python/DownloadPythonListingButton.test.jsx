import {render, unmountComponentAtNode} from "react-dom";
import DownloadPythonListingButton from "./DownloadPythonListingButton";
import {downloadMainPython} from "../../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPythonDownloadListingButton", () => {
  const preferenceInfo = {
    lang: "python",
    file: "file",
    oop: "withoop"
  };

  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(
    <DownloadPythonListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
});