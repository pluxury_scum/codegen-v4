import {downloadMainPython} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var1DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var1/path";
import {pythonMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadPythonListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getAutumn2020Var1DownloadListingPath(preferenceInfo, inputFileName, pythonMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={pythonMainFileName} label={downloadMainPython} />
  );
};

export default DownloadPythonListingButton;