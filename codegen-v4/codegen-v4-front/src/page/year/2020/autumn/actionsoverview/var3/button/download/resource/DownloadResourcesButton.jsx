import {autumn2020DownloadResourcesPath} from "../../../../../../../../../config/path/back/autumn2020/task/resource/path";
import {downloadResources} from "../../../../../../../../../config/label/overviewbutton/label";
import {resourcesFileName} from "../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";

const DownloadResourcesButton = () => {
  return (
    <DownloadButton
      path={autumn2020DownloadResourcesPath}
      fileName={resourcesFileName}
      label={downloadResources}
    />
  );
};

export default DownloadResourcesButton;