import {render, unmountComponentAtNode} from "react-dom";
import GetExerciseInfoButton from "./GetExerciseInfoButton";
import {getExerciseInfo} from "../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(<GetExerciseInfoButton extraInfo={extraInfo} />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(getExerciseInfo);
});

it("shouldRenderVar3GetExerciseInfoButton", () => {
  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(<GetExerciseInfoButton varNumber="3" extraInfo={extraInfo} />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2020/autumn/var3/exercise-info?lat=52.5&lon=162&satnum=7");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(getExerciseInfo);
});