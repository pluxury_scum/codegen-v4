import {downloadCharts} from "../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var2DownloadChartsPath} from "../../../../../../../../../config/path/back/autumn2020/task/var2/path";
import {chartsFileName} from "../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";

const DownloadChartsButton = props => {
  const inputFileName = props.inputFileName;
  const chartsPath = getAutumn2020Var2DownloadChartsPath(inputFileName);

  return (
    <DownloadButton path={chartsPath} fileName={chartsFileName} label={downloadCharts} />
  );
};

export default DownloadChartsButton;