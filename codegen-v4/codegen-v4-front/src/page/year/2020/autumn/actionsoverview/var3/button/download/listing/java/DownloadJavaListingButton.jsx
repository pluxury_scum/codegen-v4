import {downloadGraphDrawerJava, downloadMainJava} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var3DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var3/path";
import {javaGraphDrawerFileName, javaMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadJavaListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const extraInfo = props.extraInfo;

  const mainPath = getAutumn2020Var3DownloadListingPath(preferenceInfo, javaMainFileName, extraInfo);
  const graphDrawerPath = getAutumn2020Var3DownloadListingPath(preferenceInfo, javaGraphDrawerFileName, extraInfo);

  return (
    <span>
      <DownloadButton path={mainPath} fileName={javaMainFileName} label={downloadMainJava} />
      <DownloadButton path={graphDrawerPath} fileName={javaGraphDrawerFileName} label={downloadGraphDrawerJava} />
    </span>
  );
};

export default DownloadJavaListingButton;