import {render, unmountComponentAtNode} from "react-dom";
import DownloadReferenceButton from "./DownloadReferenceButton";
import {downloadReference} from "../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadReferenceButton", () => {
  render(<DownloadReferenceButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadReference);
});