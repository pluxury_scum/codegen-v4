import {render, unmountComponentAtNode} from "react-dom";
import DownloadCppListingButton from "./DownloadCppListingButton";
import {downloadMainCpp} from "../../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadListingButton", () => {
  const preferenceInfo = {
    lang: "cpp",
    file: "file",
    oop: "withoop"
  };

  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(
    <DownloadCppListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
});