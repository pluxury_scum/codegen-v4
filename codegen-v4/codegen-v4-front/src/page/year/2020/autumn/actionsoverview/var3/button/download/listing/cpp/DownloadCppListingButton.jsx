import {downloadMainCpp} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var3DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var3/path";
import {cppMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadCppListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const extraInfo = props.extraInfo;

  const mainPath = getAutumn2020Var3DownloadListingPath(preferenceInfo, cppMainFileName, extraInfo);

  return (
    <DownloadButton path={mainPath} fileName={cppMainFileName} label={downloadMainCpp} />
  );
};

export default DownloadCppListingButton;