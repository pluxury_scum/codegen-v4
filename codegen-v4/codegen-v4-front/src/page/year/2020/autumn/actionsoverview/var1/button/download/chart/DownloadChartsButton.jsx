import {getAutumn2020Var1DownloadChartsPath} from "../../../../../../../../../config/path/back/autumn2020/task/var1/path";
import {downloadCharts} from "../../../../../../../../../config/label/overviewbutton/label";
import {chartsFileName} from "../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";

const DownloadChartsButton = props => {
  const inputFileName = props.inputFileName;
  const chartsPath = getAutumn2020Var1DownloadChartsPath(inputFileName);

  return (
    <DownloadButton path={chartsPath} fileName={chartsFileName} label={downloadCharts} />
  );
};

export default DownloadChartsButton;