import {downloadMainPython} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var3DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var3/path";
import {pythonMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadPythonListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const extraInfo = props.extraInfo;

  const mainPath = getAutumn2020Var3DownloadListingPath(preferenceInfo, pythonMainFileName, extraInfo);

  return (
    <DownloadButton path={mainPath} fileName={pythonMainFileName} label={downloadMainPython} />
  );
};

export default DownloadPythonListingButton;