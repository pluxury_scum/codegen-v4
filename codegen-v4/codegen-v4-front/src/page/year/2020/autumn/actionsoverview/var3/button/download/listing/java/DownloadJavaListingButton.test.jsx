import {render, unmountComponentAtNode} from "react-dom";
import DownloadJavaListingButton from "./DownloadJavaListingButton";
import {downloadGraphDrawerJava, downloadMainJava} from "../../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadButtons", () => {
  const preferenceInfo = {
    lang: "java",
    file: "file",
    oop: "withoop"
  };

  const extraInfo = {
    latitude: 52.5,
    longitude: 162,
    satelliteNumber: 7
  };

  render(
    <DownloadJavaListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainJava);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadGraphDrawerJava);
});