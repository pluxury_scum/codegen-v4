import {render, unmountComponentAtNode} from "react-dom";
import DownloadJavaListingButton from "./DownloadJavaListingButton";
import {downloadGraphDrawerJava, downloadMainJava} from "../../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadButtons", () => {
  const preferenceInfo = {
    lang: "java",
    file: "file",
    oop: "withoop"
  };

  render(
    <DownloadJavaListingButton
      preferenceInfo={preferenceInfo}
      inputFileName="POTS_6hours.dat"
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainJava);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadGraphDrawerJava);
});