import "../../../../../../index.css";
import DownloadListingButton from "./button/download/listing/DownloadListingButton";
import DownloadResourcesButton from "./button/download/resource/DownloadResourcesButton";
import DownloadChartsButton from "./button/download/chart/DownloadChartsButton";
import DownloadReferenceButton from "./button/download/reference/DownloadReferenceButton";
import {extractFile, extractLang, extractOop} from "../../../../../../service/queryParamsExtraction";
import GetExerciseInfoButton from "./button/exerciseinfo/GetExerciseInfoButton";

const Autumn2020Var1ActionsOverview = props => {
  const inputFileName = props.match.params.inputFileName;
  const routerSearch = props.location.search;

  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);

  const preferenceInfo = {
    lang, file, oop
  };

  return (
    <div>
      <div className="ttl">
        <p>Осень, 2020 год</p>
        <span>1 вариант, </span>
        <span>язык программирования: {lang.charAt(0).toUpperCase() + lang.slice(1)}, </span>
        <span>{file === "file" ? "с чтением файла, " : "без чтения файла, "}</span>
        <span>{oop === "withoop" ? "с ООП" : "без ООП"}</span>
      </div>
      <DownloadListingButton preferenceInfo={preferenceInfo} inputFileName={inputFileName} />
      <DownloadChartsButton inputFileName={inputFileName} />
      <DownloadResourcesButton />
      <DownloadReferenceButton />
      <GetExerciseInfoButton inputFileName={inputFileName} />
    </div>
  );
};

export default Autumn2020Var1ActionsOverview;