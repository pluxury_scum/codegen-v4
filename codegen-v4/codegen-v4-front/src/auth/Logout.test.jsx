import {render, unmountComponentAtNode} from "react-dom";
import Logout from "./Logout";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLogout", () => {
  localStorage.setItem("access_token", "123");
  localStorage.setItem("refresh_token", "456");

  render(<Logout history={[]} />, container);

  expect(localStorage.getItem("access_token")).toBe("null");
  expect(localStorage.getItem("refresh_token")).toBe("null");
});