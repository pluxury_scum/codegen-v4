import {Redirect, Route} from "react-router-dom";
import {checkIfUserHasToken, checkUserAccess} from "../service/security";
import LoginForm from "./LoginForm";

const AuthWrapper = props => {
  const path = props.path;
  const component = props.component;
  const rolesAllowed = props.rolesAllowed;

  const hasToken = checkIfUserHasToken();

  if (!hasToken && component !== LoginForm) {
    return <Redirect to={{pathname: "/login", state: {errorInfo: "Пожалуйста, авторизуйтесь"}}} />
  }

  const hasAccess = checkUserAccess(rolesAllowed);

  if (hasAccess) {
    return <Route path={path} component={component} exact />
  } else {
    return <></>
  }
};

export default AuthWrapper;