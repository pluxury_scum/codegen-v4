import {useState} from "react";
import {login} from "../service/security";
import translate from "translate";

const LoginForm = props => {
  const history = props.history;
  const accessContextState = props.location.state;
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [errorInfo, setErrorInfo] = useState(accessContextState === undefined ? "" : accessContextState.errorInfo);

  const submitForm = event => {
    event.preventDefault();

    login(userName, password)
      .then(response => {
        const accessToken = response.data["access_token"];
        const refreshToken = response.data["refresh_token"];

        if (accessToken && refreshToken) {
          localStorage.setItem("access_token", "Bearer " + accessToken);
          localStorage.setItem("refresh_token", "Bearer " + refreshToken);
          history.push("/");
        } else {
          translate(response.data["error_info"], "ru")
            .then(errInfo => setErrorInfo(errInfo))
        }
      });
  };

  return (
    <div className="login-page">
      <form onSubmit={submitForm}>
        <div className="ttl">Введите имя пользователя и пароль</div>
        <div>
          <label htmlFor="username">Имя пользователя</label>
          <input
            name="username"
            type="text"
            id="username"
            placeholder="Введите текст"
            required={true}
            onChange={event => setUserName(event.target.value)}
          />
        </div>
        <div>
          <label htmlFor="password">Пароль</label>
          <input
            name="password"
            type="password"
            id="password"
            placeholder="Введите текст"
            required={true}
            onChange={event => setPassword(event.target.value)}
          />
        </div>
        <button className="form-button" type="submit">Войти</button>
        <div>
          <b>{errorInfo}</b>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;