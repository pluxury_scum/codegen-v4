import {eurekaPort} from "../../../port/port";

export const eurekaDashboardPath = `http://localhost:${eurekaPort}`;