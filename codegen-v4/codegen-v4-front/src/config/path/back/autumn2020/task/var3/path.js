import {securityServicePort} from "../../../../../port/port";

export const getAutumn2020Var3ExerciseInfoPath = extraInfo => {
  const latitude = extraInfo.latitude;
  const longitude = extraInfo.longitude;
  const satelliteNumber = extraInfo.satelliteNumber;

  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var3/exercise-info?lat=${latitude}&lon=${longitude}&satnum=${satelliteNumber}`;
};

export const getAutumn2020Var3DownloadListingPath = (preferenceInfo, outputFileName, extraInfo) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  const latitude = extraInfo.latitude;
  const longitude = extraInfo.longitude;
  const satelliteNumber = extraInfo.satelliteNumber;

  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var3/download/listing/${lang}/${file}/${oop}/${outputFileName}?lat=${latitude}&lon=${longitude}&satnum=${satelliteNumber}`;
};

export const getAutumn2020Var3DownloadChartsPath = (extraInfo) => {
  const latitude = extraInfo.latitude;
  const longitude = extraInfo.longitude;
  const satelliteNumber = extraInfo.satelliteNumber;

  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var3/download/charts?lat=${latitude}&lon=${longitude}&satnum=${satelliteNumber}`;
};