import {securityServicePort} from "../../../../../port/port";

export const getAutumn2020Var1ExerciseInfoPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var1/exercise-info/${inputFileName}`;
};

export const getAutumn2020Var1DownloadListingPath = (preferenceInfo, inputFileName, outputFileName) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var1/download/listing/${lang}/${file}/${oop}/${inputFileName}/${outputFileName}`;
};

export const getAutumn2020Var1DownloadChartsPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var1/download/charts/${inputFileName}`;
};