import {securityServicePort} from "../../../../../port/port";

export const getAutumn2020Var2ExerciseInfoPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var2/exercise-info/${inputFileName}`;
};

export const getAutumn2020Var2DownloadListingPath = (preferenceInfo, inputFileName, outputFileName) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var2/download/listing/${lang}/${file}/${oop}/${inputFileName}/${outputFileName}`;
};

export const getAutumn2020Var2DownloadChartsPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/var2/download/charts/${inputFileName}`;
};