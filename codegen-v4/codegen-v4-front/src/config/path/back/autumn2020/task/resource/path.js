import {securityServicePort} from "../../../../../port/port";

export const autumn2020DownloadResourcesPath = `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/download/resources`;

export const autumn2020DownloadReferencePath = `http://localhost:${securityServicePort}/api/tasks/year/2020/autumn/download/reference`;