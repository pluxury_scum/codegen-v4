import {securityServicePort} from "../../../../port/port";

export const autumn2020ActuatorPath = `http://localhost:${securityServicePort}/api/year/2020/autumn/actuator/health`;

export const autumn2020SwaggerUiPath = `http://localhost:${securityServicePort}/api/year/2020/autumn/swagger-ui`;