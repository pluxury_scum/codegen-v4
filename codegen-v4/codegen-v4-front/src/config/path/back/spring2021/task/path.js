import {securityServicePort} from "../../../../port/port";

export const getSpring2021ExerciseInfoPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2021/spring/exercise-info/${inputFileName}`;
};

export const getSpring2021DownloadListingPath = (preferenceInfo, inputFileName, outputFileName) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return `http://localhost:${securityServicePort}/api/tasks/year/2021/spring/download/listing/${lang}/${file}/${oop}/${inputFileName}/${outputFileName}`;
};

export const getSpring2021DownloadChartsPath = inputFileName => {
  return `http://localhost:${securityServicePort}/api/tasks/year/2021/spring/download/charts/${inputFileName}`;
};

export const spring2021DownloadResourcesPath = `http://localhost:${securityServicePort}/api/tasks/year/2021/spring/download/resources`;
