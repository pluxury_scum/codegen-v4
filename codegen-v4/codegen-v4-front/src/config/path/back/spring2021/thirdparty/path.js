import {securityServicePort} from "../../../../port/port";

export const spring2021ActuatorPath = `http://localhost:${securityServicePort}/api/year/2021/spring/actuator/health`;

export const spring2021SwaggerUiPath = `http://localhost:${securityServicePort}/api/year/2021/spring/swagger-ui`;