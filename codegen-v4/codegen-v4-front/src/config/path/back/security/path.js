import {securityServicePort} from "../../../port/port";

export const loginPath = `http://localhost:${securityServicePort}/api/login`;

export const refreshTokenPath = `http://localhost:${securityServicePort}/api/token/refresh`;