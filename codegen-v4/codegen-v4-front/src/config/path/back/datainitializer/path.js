import {securityServicePort} from "../../../port/port";

export const initializeSecurityDataPath = `http://localhost:${securityServicePort}/api/initializer/security/initialize`;

export const initializeAutumn2020DataPath = `http://localhost:${securityServicePort}/api/initializer/autumn2020/initialize`;

export const initializeSpring2021DataPath = `http://localhost:${securityServicePort}/api/initializer/spring2021/initialize`;

export const dataInitializerActuatorPath = `http://localhost:${securityServicePort}/api/initializer/actuator/health`;