import {frontEndPort} from "../../../../port/port";

export const autumn2020Var2ExerciseInfoFormPath = "/year/2020/autumn/var2/input-file/select";

export const getAutumn2020Var2ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2020/autumn/var2/exercise-info/${inputFileName}`;
};

export const getAutumn2020Var2ActionOverviewPath = (preferenceInfo, inputFileName) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return `http://localhost:${frontEndPort}/year/2020/autumn/var2/actions-overview/${inputFileName}?lang=${lang}&file=${file}&oop=${oop}`;
};