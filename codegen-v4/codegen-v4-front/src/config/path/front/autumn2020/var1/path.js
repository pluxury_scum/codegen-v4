import {frontEndPort} from "../../../../port/port";

export const autumn2020Var1ExerciseInfoFormPath = "/year/2020/autumn/var1/input-file/select";

export const getAutumn2020Var1ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2020/autumn/var1/exercise-info/${inputFileName}`;
};

export const getAutumn2020Var1ActionOverviewPath = (preferenceInfo, inputFileName) => {
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return `http://localhost:${frontEndPort}/year/2020/autumn/var1/actions-overview/${inputFileName}?lang=${lang}&file=${file}&oop=${oop}`;
};