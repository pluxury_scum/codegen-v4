import {frontEndPort} from "../../../../port/port";

export const autumn2020Var3ExerciseInfoFormPath = "/year/2020/autumn/var3/extra-info";

export const getAutumn2020Var3ExerciseInfoPagePath = extraInfo => {
  const latitude = extraInfo.latitude;
  const longitude = extraInfo.longitude;
  const satelliteNumber = extraInfo.satelliteNumber;

  return `http://localhost:${frontEndPort}/year/2020/autumn/var3/exercise-info?lat=${latitude}&lon=${longitude}&satnum=${satelliteNumber}`;
};