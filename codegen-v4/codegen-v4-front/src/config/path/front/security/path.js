import {frontEndPort} from "../../../port/port";

export const sendLoginCredentialsPath = `http://localhost:${frontEndPort}/send-login-credentials`;