import {frontEndPort} from "../../../port/port";

export const autumn2020MenuPath = `http://localhost:${frontEndPort}/year/2020/autumn/menu`;

export const spring2021MenuPath = `http://localhost:${frontEndPort}/year/2021/spring/menu`;