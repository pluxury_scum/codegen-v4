export const downloadMainCpp = "Загрузить main.cpp";

export const downloadMainJava = "Загрузить Main.java";

export const downloadGraphDrawerJava = "Загрузить GraphDrawer.java";

export const downloadMainPython = "Загрузить main.py";

export const downloadCharts = "Загрузить charts.rar";

export const downloadResources = "Загрузить resources.rar";

export const downloadReference = "Загрузить reference.pdf";

export const getExerciseInfo = "Получить информацию по заданию";