export const fileWithOop = "На 5 с ООП";

export const fileNoOop = "На 5 без ООП";

export const manualWithOop = "На 3/4 с ООП";

export const manualNoOop = "На 3/4 без ООП";