export const extractLang = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lang");
};

export const extractFile = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("file") === "true" ? "file" : "manual";
};

export const extractOop = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("oop") === "true" ? "withoop" : "nooop";
};

export const extractLatitude = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lat");
};

export const extractLongitude = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lon");
};

export const extractSatelliteNumber = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("satnum");
};