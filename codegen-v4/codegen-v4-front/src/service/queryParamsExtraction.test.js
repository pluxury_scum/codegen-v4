import {extractFile, extractLang, extractLatitude, extractLongitude, extractOop, extractSatelliteNumber} from "./queryParamsExtraction"

it("shouldGetLang", () => {
  const lang = extractLang("?lang=cpp");

  expect(lang).toBe("cpp");
});

it("shouldGetFile", () => {
  const file = extractFile("?file=true");

  expect(file).toBe("file");
});

it("shouldGetManual", () => {
  const file = extractFile("?file=false");

  expect(file).toBe("manual");
});

it("shouldGetWithOop", () => {
  const oop = extractOop("?oop=true");

  expect(oop).toBe("withoop");
});

it("shouldGetNoOop", () => {
  const oop = extractOop("?oop=false");

  expect(oop).toBe("nooop");
});

it("shouldGetLat", () => {
  const latitude = extractLatitude("?lat=145");

  expect(latitude).toBe("145");
});

it("shouldGetLongitude", () => {
  const longitude = extractLongitude("?lon=60");

  expect(longitude).toBe("60");
});

it("shouldGetSatelliteNumber", () => {
  const satelliteNumber = extractSatelliteNumber("?satnum=7");

  expect(satelliteNumber).toBe("7");
});

it("shouldGetAllParams", () => {
  const routerSearch = "?lang=python&file=false&oop=true&lat=130&lon=50&satnum=5";

  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);
  const latitude = extractLatitude(routerSearch);
  const longitude = extractLongitude(routerSearch);
  const satelliteNumber = extractSatelliteNumber(routerSearch);

  expect(lang).toBe("python");
  expect(file).toBe("manual");
  expect(oop).toBe("withoop");
  expect(latitude).toBe("130");
  expect(longitude).toBe("50");
  expect(satelliteNumber).toBe("5");
});