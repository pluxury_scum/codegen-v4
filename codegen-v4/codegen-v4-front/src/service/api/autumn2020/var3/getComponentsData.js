import axios from "axios";
import {getAutumn2020Var3ExerciseInfoPath} from "../../../../config/path/back/autumn2020/task/var3/path";
import {getConfigWithAuthorizationHeader} from "../../../security";

export const getComponentsData = (lat, lon, satelliteNumber) => {
  const path = getAutumn2020Var3ExerciseInfoPath(lat, lon, satelliteNumber);
  const config = getConfigWithAuthorizationHeader();

  return axios.get(path, config)
    .then(response => response.data);
};