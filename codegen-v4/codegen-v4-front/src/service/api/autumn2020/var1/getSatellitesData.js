import axios from "axios";
import {getAutumn2020Var1ExerciseInfoPath} from "../../../../config/path/back/autumn2020/task/var1/path";
import {getConfigWithAuthorizationHeader} from "../../../security";

export const getSatellitesData = inputFileName => {
  const path = getAutumn2020Var1ExerciseInfoPath(inputFileName);
  const config = getConfigWithAuthorizationHeader();

  return axios.get(path, config).then(response => {
    const satellites = response.data.satellites;
    const satellite1 = satellites[0];
    const satellite2 = satellites[1];
    const satellite3 = satellites[2];

    return {satellite1, satellite2, satellite3}
  });
};
