CREATE TABLE IF NOT EXISTS student
(
    id                      BIGINT PRIMARY KEY,
    user_name               VARCHAR NOT NULL,
    password                VARCHAR NOT NULL,
    account_non_expired     BOOLEAN NOT NULL,
    account_non_locked      BOOLEAN NOT NULL,
    credentials_non_expired BOOLEAN NOT NULL,
    enabled                 BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS role
(
    id        BIGINT PRIMARY KEY,
    authority VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS student_role
(
    student_id BIGINT NOT NULL UNIQUE,
    role_id    BIGINT NOT NULL
);

INSERT INTO student (id, user_name, password, account_non_expired, account_non_locked, credentials_non_expired, enabled)
VALUES (1, 'admin', '$2a$10$4.UwnbtWVjgV83ft1avZo.HNdAKqUNt/csbfC7etBQJKKCxZCUww2', true, true, true, true),
       (2, 'a20201', '$2a$10$5ryAraZ1UfGg8cRrgw6ymOqBJfOOWUsH9fF99AXvt0alXAkHPsmVO', true, true, true, true),
       (3, 'a20202', '$2a$10$BsQTt4Cnq6cykt9eZNNR.eJVlXowIoX0wA1tNQu6in.WcD/867A4y', true, true, true, true),
       (4, 'a20203', '$2a$10$gPRjpXplUDiQcLbxBmTpje0az3SdNLiiL5nk6DIcBJciWB7xmsGcC', true, true, true, true),
       (5, 's2021', '$2a$10$dHFUOtWgxwlP2yqQuhqds.X8zGrjivxd55EnB0Cat41ZMtc1RhwDG', true, true, true, true)
ON CONFLICT DO NOTHING;

INSERT INTO role (id, authority)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_A20201'),
       (3, 'ROLE_A20202'),
       (4, 'ROLE_A20203'),
       (5, 'ROLE_S2021')
ON CONFLICT DO NOTHING;

INSERT INTO student_role (student_id, role_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5)
ON CONFLICT DO NOTHING;