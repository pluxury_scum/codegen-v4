﻿#include <iostream>
#include <vector>
#include <iomanip>

auto getTroposphericDelays(std::vector<double> mdArray, std::vector<double> tdArray, std::vector<double> mwArray, std::vector<double> twArray,
               int amountOfObservations) {
    std::vector<double> delays;
    for (int observation = 0; observation < amountOfObservations; observation++) {
        double md = mdArray.at(observation);
        double td = tdArray.at(observation);
        double mw = mwArray.at(observation);
        double tw = twArray.at(observation);
        double delay = md * td + mw * tw;
        delays.push_back(delay);
    }
    return delays;
}

auto getAnglesInHalfCircles(std::vector<double> elevationArray, int amountOfObservations) {
    double halfCircle = 180;
    std::vector<double> halfCircles;
    for (int observation = 0; observation < amountOfObservations; observation++) {
        double angleInDegrees = elevationArray.at(observation);
        double angleInHalfCircles = angleInDegrees / halfCircle;
        halfCircles.push_back(angleInHalfCircles);
    }
    return halfCircles;
}

void printInfo(int satellite1Number, int satellite2Number, int satellite3Number,
               std::vector<double> satellite1Delays, std::vector<double> satellite2Delays, std::vector<double> satellite3Delays,
               std::vector<double> satellite1ElevationAngles, std::vector<double> satellite2ElevationAngles, std::vector<double> satellite3ElevationAngles,
               int amountOfObservations) {
    std::cout << "Satellite #" << satellite1Number <<
                 "\t\t\t\tSatellite #" << satellite2Number <<
                 "\t\t\t\tSatellite #" << satellite3Number << std::endl;
    std::cout << std::fixed << std::setprecision(10);
    for (int observation = 0; observation < amountOfObservations; observation++) {
        double satellite1TroposphericDelay = satellite1Delays.at(observation);
        double satellite1ElevationAngle = satellite1ElevationAngles.at(observation);
        double satellite2TroposphericDelay = satellite2Delays.at(observation);
        double satellite2ElevationAngle = satellite2ElevationAngles.at(observation);
        double satellite3TroposphericDelay = satellite3Delays.at(observation);
        double satellite3ElevationAngle = satellite3ElevationAngles.at(observation);
        std::cout << satellite1TroposphericDelay << "\t" << satellite1ElevationAngle << "\t\t" <<
                     satellite2TroposphericDelay << "\t" << satellite2ElevationAngle << "\t\t" <<
                     satellite3TroposphericDelay << "\t" << satellite3ElevationAngle << std::endl;
    }
}

int main() {
    int amountOfObservations = 360;

    int satellite1Number = %s;
    int satellite2Number = %s;
    int satellite3Number = %s;

    std::vector<double> satellite1Md {
        %s
    };

    std::vector<double> satellite1Td {
        %s
    };

    std::vector<double> satellite1Mw {
        %s
    };

    std::vector<double> satellite1Tw {
        %s
    };

    std::vector<double> satellite2Md {
        %s
    };

    std::vector<double> satellite2Td {
        %s
    };

    std::vector<double> satellite2Mw {
        %s
    };

    std::vector<double> satellite2Tw {
        %s
    };

    std::vector<double> satellite3Md {
        %s
    };

    std::vector<double> satellite3Td {
        %s
    };

    std::vector<double> satellite3Mw {
        %s
    };

    std::vector<double> satellite3Tw {
        %s
    };

    std::vector<double> satellite1Elevation {
        %s
    };

    std::vector<double> satellite2Elevation {
        %s
    };

    std::vector<double> satellite3Elevation {
        %s
    };

    auto satellite1Delays = getTroposphericDelays(satellite1Md, satellite1Td, satellite1Mw, satellite1Tw, amountOfObservations);
    auto satellite2Delays = getTroposphericDelays(satellite2Md, satellite2Td, satellite2Mw, satellite2Tw, amountOfObservations);
    auto satellite3Delays = getTroposphericDelays(satellite3Md, satellite3Td, satellite3Mw, satellite3Tw, amountOfObservations);

    auto satellite1ElevationAngles = getAnglesInHalfCircles(satellite1Elevation, amountOfObservations);
    auto satellite2ElevationAngles = getAnglesInHalfCircles(satellite2Elevation, amountOfObservations);
    auto satellite3ElevationAngles = getAnglesInHalfCircles(satellite3Elevation, amountOfObservations);

    printInfo(satellite1Number, satellite2Number, satellite3Number,
              satellite1Delays, satellite2Delays, satellite3Delays,
              satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
              amountOfObservations);
}
