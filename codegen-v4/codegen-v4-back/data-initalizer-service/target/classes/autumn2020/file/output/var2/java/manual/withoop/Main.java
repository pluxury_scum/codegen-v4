import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 360;
        int amountOfHours = 3;

        int satellite1Number = %s;
        int satellite2Number = %s;
        int satellite3Number = %s;

        List<Double> satellite1ElevationAngles = asList(
            %s
        );

        List<Double> satellite2ElevationAngles = asList(
            %s
        );

        List<Double> satellite3ElevationAngles = asList(
            %s
        );

        Satellite satellite1 = SatelliteFactory.createSatellite(satellite1Number, satellite1ElevationAngles, amountOfObservations);
        Satellite satellite2 = SatelliteFactory.createSatellite(satellite2Number, satellite2ElevationAngles, amountOfObservations);
        Satellite satellite3 = SatelliteFactory.createSatellite(satellite3Number, satellite3ElevationAngles, amountOfObservations);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations, amountOfHours);
        consoleOutput.getConsoleOutput();

        GraphDrawer.draw();
    }
}

class ConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;
    private int amountOfObservations;
    private int amountOfHours;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations, int amountOfHours) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
        this.amountOfHours = amountOfHours;
    }

    public void getConsoleOutput() {
        TemplateConsoleOutput angularVelocitiesOutput = new AngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        angularVelocitiesOutput.print();

        TemplateConsoleOutput averageAngularVelocitiesOutput = new AverageAngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageAngularVelocitiesOutput.print();

        TemplateConsoleOutput linearVelocitiesOutput = new LinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        linearVelocitiesOutput.print();

        TemplateConsoleOutput averageLinearVelocitiesOutput = new AverageLinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageLinearVelocitiesOutput.print();
    }
}

abstract class TemplateConsoleOutput {
    private int amountOfObservations;

    public TemplateConsoleOutput(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    protected void print() {
        int satellite1Number = getSatellite1Number();
        int satellite2Number = getSatellite2Number();
        int satellite3Number = getSatellite3Number();
        String legend = getLegend();
        System.out.println(legend);
        System.out.println("Спутник #" + satellite1Number + "\t\tСпутник #" + satellite2Number + "\t\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = getVelocities1().get(observation);
            double satellite2Velocity = getVelocities2().get(observation);
            double satellite3Velocity = getVelocities3().get(observation);
            System.out.println(df.format(satellite1Velocity) + "\t" + df.format(satellite2Velocity) + "\t" + df.format(satellite3Velocity));
        }
        System.out.println("***********************************************");
    }

    abstract String getLegend();
    abstract int getSatellite1Number();
    abstract int getSatellite2Number();
    abstract int getSatellite3Number();
    abstract List<Double> getVelocities1();
    abstract List<Double> getVelocities2();
    abstract List<Double> getVelocities3();
}

class AngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;

    public AngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAngularVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAngularVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAngularVelocities();
        return velocities;
    }
}

class AverageAngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;

    public AverageAngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Средняя угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAverageAngularVelocities();
        return velocities;
    }
}

class LinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;

    public LinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getLinearVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getLinearVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getLinearVelocities();
        return velocities;
    }
}

class AverageLinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;

    public AverageLinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Средняя линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    protected List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAverageLinearVelocities();
        return velocities;
    }
}

class SatelliteFactory {
    public static Satellite createSatellite(int number, List<Double> elevationAnglesArray, int amountOfObservations) {
        ElevationAngles elevationAngles = new ElevationAngles(elevationAnglesArray);
        AngularVelocities angularVelocities = new AngularVelocities(elevationAngles, amountOfObservations);
        LinearVelocities linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite satellite = new Satellite(number, angularVelocities, linearVelocities);
        return satellite;
    }
}

class Satellite {
    private int number;
    private AngularVelocities angularVelocities;
    private LinearVelocities linearVelocities;

    public Satellite(int number, AngularVelocities angularVelocities, LinearVelocities linearVelocities) {
        this.number = number;
        this.angularVelocities = angularVelocities;
        this.linearVelocities = linearVelocities;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getAngularVelocities() {
        List<Double> velocities = angularVelocities.getAngularVelocities();
        return velocities;
    }

    public List<Double> getAverageAngularVelocities() {
        List<Double> velocities = angularVelocities.getAverageAngularVelocities();
        return velocities;
    }

    public List<Double> getLinearVelocities() {
        List<Double> velocities = linearVelocities.getLinearVelocities();
        return velocities;
    }

    public List<Double> getAverageLinearVelocities() {
        List<Double> velocities = linearVelocities.getAverageLinearVelocities();
        return velocities;
    }
}

class AngularVelocities {
    private int amountOfObservations;
    private ElevationAngles elevationAngles;

    public AngularVelocities(ElevationAngles elevationAngles, int amountOfObservations) {
        this.elevationAngles = elevationAngles;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getAverageAngularVelocities() {
        int observationsPerHour = 120;
        List<Double> averageVelocities = new ArrayList<>();
        List<Double> angularVelocities = getAngularVelocities();

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);
        return averageVelocities;
    }

    public List<Double> getAngularVelocities() {
        double oneHourInSeconds = 3600;
        double previousElevationAngle = elevationAngles.getAngleAt(0);

        List<Double> angularVelocities = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAngles.getAngleAt(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.add(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }
        return angularVelocities;
    }

    private double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        double velocitySum = 0;
        for (int observation = start; observation < end; observation++) {
            velocitySum += velocities.get(observation);
        }
        return velocitySum;
    }
}

class LinearVelocities {
    private int amountOfObservations;

    public LinearVelocities(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    List<Double> getAverageLinearVelocities() {
        int observationsPerHour = 120;
        List<Double> averageVelocities = new ArrayList<>();
        List<Double> linearVelocities = getLinearVelocities();

        double firstHourSum = getVelocitySumPerHour(linearVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(linearVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(linearVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public List<Double> getLinearVelocities() {
        double gravitational = 6.67 * Math.pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6_371_000;
        double height = 20_000;

        List<Double> linearVelocities = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double linearVelocity = Math.sqrt(gravitational * earthMass / (earthRadius + height));
            linearVelocities.add(linearVelocity);
        }
        return linearVelocities;
    }

    private double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        double hourSum = 0;
        for (int observation = start; observation < end; observation++) {
            hourSum += velocities.get(observation);
        }
        return hourSum;
    }
}

class ElevationAngles {
    private List<Double> elevationAngles;

    public ElevationAngles(List<Double> elevationAngles) {
        this.elevationAngles = elevationAngles;
    }

    public double getAngleAt(int observation) {
        return elevationAngles.get(observation);
    }
}