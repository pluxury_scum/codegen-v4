package com.gvozdev.datainitializerservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.datainitializerservice.util.path.Path.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PageControllerTests {
    private final MockMvc mockMvc;

    @Autowired
    PageControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void shouldCheckInitializeSecurityDataController() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void shouldCheckInitializeAutumn2020DataController() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void shouldCheckInitializeSpring2021DataController() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }
}
