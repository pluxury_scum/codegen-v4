package com.gvozdev.datainitializerservice.util.path;

public class Path {
    public static final String INITIALIZE_SECURITY = """
        /api/initializer/security/initialize""";

    public static final String INITIALIZE_AUTUMN2020 = """
        /api/initializer/autumn2020/initialize""";

    public static final String INITIALIZE_SPRING2021 = """
        /api/initializer/spring2021/initialize""";

    private Path() {
    }
}
