﻿#include <iostream>
#include <vector>
#include <iomanip>

class DelayComponent {
public:
    virtual std::vector<double> getValues() = 0;
};

class DryComponentFunction : public DelayComponent {
public:
    DryComponentFunction(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class VerticalDryComponent : public DelayComponent {
public:
    VerticalDryComponent(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class WetComponentFunction : public DelayComponent {
public:
    WetComponentFunction(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class VerticalWetComponent : public DelayComponent {
public:
    VerticalWetComponent(std::vector<double> values) {
        this->values = values;
    }

    virtual std::vector<double> getValues() override {
        return values;
    }

private:
    std::vector<double> values;
};

class TroposphericDelays {
public:
    TroposphericDelays(DelayComponent& dryComponentFunction, DelayComponent& verticalDryComponent,
                       DelayComponent& wetComponentFunction, DelayComponent& verticalWetComponent,
                       int amountOfObservations) {
        this->dryComponentFunction = &dryComponentFunction;
        this->verticalDryComponent = &verticalDryComponent;
        this->wetComponentFunction = &wetComponentFunction;
        this->verticalWetComponent = &verticalWetComponent;
        this->amountOfObservations = amountOfObservations;
    }

    std::vector<double> getDelays() {
        std::vector<double> dryComponentFunctionValues = dryComponentFunction->getValues();
        std::vector<double> verticalDryComponentValues = verticalDryComponent->getValues();
        std::vector<double> wetComponentFunctionValues = wetComponentFunction->getValues();
        std::vector<double> verticalWetComponentValues = verticalWetComponent->getValues();

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = dryComponentFunctionValues.at(observation);
            double td = verticalDryComponentValues.at(observation);
            double mw = wetComponentFunctionValues.at(observation);
            double tw = verticalWetComponentValues.at(observation);
            double delay = md * td + mw * tw;
            delays.push_back(delay);
        }
        return delays;
    }

private:
    DelayComponent* dryComponentFunction;
    DelayComponent* verticalDryComponent;
    DelayComponent* wetComponentFunction;
    DelayComponent* verticalWetComponent;
    int amountOfObservations;
    std::vector<double> delays;
};

class ElevationAngles {
public:
    ElevationAngles(std::vector<double> degrees) {
        this->degrees = degrees;
    }

    std::vector<double> getAnglesInHalfCircles() {
        double halfCircle = 180;
        std::vector<double> halfCircles;
        for (double angleInDegrees : degrees) {
            double angleInHalfCircles = angleInDegrees / halfCircle;
            halfCircles.push_back(angleInHalfCircles);
        }
        return halfCircles;
    }

private:
    std::vector<double> degrees;
};

class Satellite {
public:
    Satellite(int number, TroposphericDelays& troposphericDelays, ElevationAngles& elevationAngles) {
        this->number = number;
        this->troposphericDelays = &troposphericDelays;
        this->elevationAngles = &elevationAngles;
    }

    int getNumber() {
        return number;
    }

    std::vector<double> getTroposphericDelays() {
        std::vector<double> delays = troposphericDelays->getDelays();
        return delays;
    }

    std::vector<double> getElevationAngles() {
        std::vector<double> angles = elevationAngles->getAnglesInHalfCircles();
        return angles;
    }

private:
    int number;
    TroposphericDelays* troposphericDelays;
    ElevationAngles* elevationAngles;
};

class SatelliteFactory {
public:
    SatelliteFactory(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    Satellite* createSatellite(int satelliteNumber, std::vector<double> mdArray, std::vector<double> tdArray,
                               std::vector<double> mwArray, std::vector<double> twArray,
                               std::vector<double> elevationArray) {
        DelayComponent* dryComponentFunction = new DryComponentFunction(mdArray);
        DelayComponent* verticalDryComponent = new VerticalDryComponent(tdArray);
        DelayComponent* wetComponentFunction = new WetComponentFunction(mwArray);
        DelayComponent* verticalWetComponent = new VerticalWetComponent(twArray);
        TroposphericDelays* delays = new TroposphericDelays(*dryComponentFunction, *verticalDryComponent,
                                                            *wetComponentFunction, *verticalWetComponent,
                                                            amountOfObservations);
        ElevationAngles* angles = new ElevationAngles(elevationArray);
        Satellite* satellite = new Satellite(satelliteNumber, *delays, *angles);
        return satellite;
    }

private:
    int amountOfObservations;
};

class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
    }

    void printInfo() {
        int satellite1Number = satellite1->getNumber();
        int satellite2Number = satellite2->getNumber();
        int satellite3Number = satellite3->getNumber();
        std::vector<double> satellite1TroposphericDelays = satellite1->getTroposphericDelays();
        std::vector<double> satellite2TroposphericDelays = satellite2->getTroposphericDelays();
        std::vector<double> satellite3TroposphericDelays = satellite3->getTroposphericDelays();
        std::vector<double> satellite1ElevationAngles = satellite1->getElevationAngles();
        std::vector<double> satellite2ElevationAngles = satellite2->getElevationAngles();
        std::vector<double> satellite3ElevationAngles = satellite3->getElevationAngles();
        std::cout << "Satellite #" << satellite1Number <<
                     "\t\t\t\tSatellite #" << satellite2Number <<
                     "\t\t\t\tSatellite #" << satellite3Number << std::endl;
        std::cout << std::fixed << std::setprecision(10);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1TroposphericDelay = satellite1TroposphericDelays.at(observation);
            double satellite1ElevationAngle = satellite1ElevationAngles.at(observation);
            double satellite2TroposphericDelay = satellite2TroposphericDelays.at(observation);
            double satellite2ElevationAngle = satellite2ElevationAngles.at(observation);
            double satellite3TroposphericDelay = satellite3TroposphericDelays.at(observation);
            double satellite3ElevationAngle = satellite3ElevationAngles.at(observation);
            std::cout << satellite1TroposphericDelay << "\t" << satellite1ElevationAngle << "\t\t" <<
                         satellite2TroposphericDelay << "\t" << satellite2ElevationAngle << "\t\t" <<
                         satellite3TroposphericDelay << "\t" << satellite3ElevationAngle << std::endl;
        }
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
};

int main() {
    int amountOfObservations = 360;

    int satellite1Number = %s;
    int satellite2Number = %s;
    int satellite3Number = %s;

    std::vector<double> satellite1Md {
        %s
    };

    std::vector<double> satellite1Td {
        %s
    };

    std::vector<double> satellite1Mw {
        %s
    };

    std::vector<double> satellite1Tw {
        %s
    };

    std::vector<double> satellite2Md {
        %s
    };

    std::vector<double> satellite2Td {
        %s
    };

    std::vector<double> satellite2Mw {
        %s
    };

    std::vector<double> satellite2Tw {
        %s
    };

    std::vector<double> satellite3Md {
        %s
    };

    std::vector<double> satellite3Td {
        %s
    };

    std::vector<double> satellite3Mw {
        %s
    };

    std::vector<double> satellite3Tw {
        %s
    };

    std::vector<double> satellite1Elevation {
        %s
    };

    std::vector<double> satellite2Elevation {
        %s
    };

    std::vector<double> satellite3Elevation {
        %s
    };

    SatelliteFactory* satelliteFactory = new SatelliteFactory(amountOfObservations);

    Satellite* satellite1 = satelliteFactory->createSatellite(satellite1Number, satellite1Md, satellite1Td, satellite1Mw, satellite1Tw, satellite1Elevation);

    Satellite* satellite2 = satelliteFactory->createSatellite(satellite2Number, satellite2Md, satellite2Td, satellite2Mw, satellite2Tw, satellite2Elevation);

    Satellite* satellite3 = satelliteFactory->createSatellite(satellite3Number, satellite3Md, satellite3Td, satellite3Mw, satellite3Tw, satellite3Elevation);

    ConsoleOutput* consoleOutput = new ConsoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
    consoleOutput->printInfo();
}