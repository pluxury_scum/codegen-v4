﻿#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>

double __getK() {
	double f1 = 1575420000;
	double f2 = 1227600000;
	double k = pow(f1, 2) / pow(f2, 2);
	return k;
}

double getIonosphericDelayAt(std::vector<double> pseudoRanges1, std::vector<double> pseudoRanges2, int observation) {
	double speedOfLight = 2.99792458 * 1E8;
	double p1 = pseudoRanges1.at(observation);
	double p2 = pseudoRanges2.at(observation);
	double k = __getK();
	double delay = (p1 - p2) / (speedOfLight * (1 - k));
	double delayInMeters = delay * speedOfLight;
	return delayInMeters;
}

void printDelays(int satellite1Number, int satellite2Number, int satellite3Number, std::vector<double> satellite1P1,
                 std::vector<double> satellite1P2, std::vector<double> satellite2P1, std::vector<double> satellite2P2,
                 std::vector<double> satellite3P1, std::vector<double> satellite3P2, int amountOfObservations) {
    std::cout << "Satellite #" << satellite1Number << "\t\tSatellite #" << satellite2Number <<
        "\t\tSatellite #" << satellite3Number << std::endl;
    std::cout << std::fixed << std::setprecision(10);
    for (int observation = 0; observation < amountOfObservations; observation++) {
        double delay1 = getIonosphericDelayAt(satellite1P1, satellite1P2, observation);
        double delay2 = getIonosphericDelayAt(satellite2P1, satellite2P2, observation);
        double delay3 = getIonosphericDelayAt(satellite3P1, satellite3P2, observation);
        std::cout << delay1 << "\t" << delay2 << "\t" << delay3 << std::endl;
    }
}

int main() {
	int amountOfObservations = 360;

	int satellite1Number = %s;
    int satellite2Number = %s;
    int satellite3Number = %s;

    std::vector<double> satellite1P1 {
        %s
    };

    std::vector<double> satellite1P2 {
        %s
    };

    std::vector<double> satellite2P1 {
        %s
    };

    std::vector<double> satellite2P2 {
        %s
    };

    std::vector<double> satellite3P1 {
        %s
    };

    std::vector<double> satellite3P2 {
        %s
    };

    printDelays(satellite1Number, satellite2Number, satellite3Number,
                satellite1P1, satellite1P2, satellite2P1, satellite2P2, satellite3P1, satellite3P2,
                amountOfObservations);
}