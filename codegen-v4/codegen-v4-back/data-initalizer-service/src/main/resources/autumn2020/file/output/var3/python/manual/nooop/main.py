import math
import matplotlib.pyplot as plt
import numpy as np


def __getTecuToMetersCoefficient() -> float:
    l1: float = 1_575_420_000
    oneTecUnit: float = 1E16
    coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
    return coefficient


def __getDelayInTecu(weightMatrix: list, tecList: list) -> float:
    delay: float = 0
    for observation in range(4):
        weight: float = weightMatrix[observation]
        rawTec: float = tecList[observation]
        tecInOneTecUnit: float = rawTec * 0.1
        delay += (weight * tecInOneTecUnit)
    return delay


def getDelayInMeters(weightMatrix: list, tec: list) -> float:
    tecuToMetersCoefficient: float = __getTecuToMetersCoefficient()
    delayInTecu: float = __getDelayInTecu(weightMatrix, tec)
    delayInMeters: float = delayInTecu * tecuToMetersCoefficient
    return delayInMeters


def getKlobucharDelayInMeters(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                              alpha: list, beta: list) -> float:
    delayInSeconds: float = __getKlobucharDelayInSeconds(latpp, lonpp, elevationAngle, azimuth, gpsTime, alpha, beta)
    speedOfLight: float = 2.99792458 * 1E8
    delayInMeters: float = delayInSeconds * speedOfLight
    return delayInMeters


def __getEarthCenteredAngle(elevationAngle: float) -> float:
    earthCenteredAngle: float = 0.0137 / (elevationAngle + 0.11) - 0.022
    return earthCenteredAngle


def __getIppLatitude(latpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = latpp + earthCenteredAngle * math.cos(azimuth)
    if ippLatitude > 0.416:
        ippLatitude = 0.416
    elif ippLatitude < -0.416:
        ippLatitude = -0.416
    return ippLatitude


def __getIppLongtitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(azimuth) / (math.cos(ippLatitude)))
    return ippLongtitude


def __getIppGeomagneticLatitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
    return ippGeomagneticLatitude


def __getIppLocalTime(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float) -> float:
    secondsInOneDay: float = 86_400
    secondsInTwelveHours: float = 43_200
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippLocalTime: float = secondsInTwelveHours * ippLongtitude + gpsTime
    while ippLocalTime > secondsInOneDay:
        ippLocalTime -= secondsInOneDay
    while ippLocalTime < 0:
        ippLocalTime += secondsInOneDay
    return ippLocalTime


def __getIonosphericDelayAmplitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float,
                                   alpha: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    amplitude: float = 0
    for i in range(4):
        amplitude += (alpha[i] * pow(ippGeomagneticLatitude, i))
    if amplitude < 0:
        amplitude = 0
    return amplitude


def __getIonosphericDelayPeriod(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, beta: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    period: float = 0
    for i in range(4):
        period += (beta[i] * pow(ippGeomagneticLatitude, i))
    if period < 72000:
        period = 72000
    return period


def __getIonosphericDelayPhase(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                               beta: list) -> float:
    ippLocalTime: float = __getIppLocalTime(latpp, lonpp, elevationAngle, azimuth, gpsTime)
    ionosphericDelayPeriod: float = __getIonosphericDelayPeriod(latpp, lonpp, elevationAngle, azimuth, beta)
    ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
    return ionosphericDelayPhase


def __getSlantFactor(elevationAngle: float) -> float:
    slantFactor: float = 1 + 16 * math.pow((0.53 - elevationAngle), 3)
    return slantFactor


def __getKlobucharDelayInSeconds(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                                 alpha: list, beta: list) -> float:
    ionosphericDelayPhase: float = __getIonosphericDelayPhase(latpp, lonpp, elevationAngle, azimuth, gpsTime, beta)
    ionosphericDelayAmplitude: float = __getIonosphericDelayAmplitude(latpp, lonpp, elevationAngle, azimuth, alpha)
    slantFactor: float = __getSlantFactor(elevationAngle)
    ionosphericTimeDelay: float
    if abs(ionosphericDelayPhase) > 1.57:
        ionosphericTimeDelay = 5E-9 * slantFactor
    else:
        ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                    math.pow(ionosphericDelayPhase,
                                                                             4) / 24)) * slantFactor
    return ionosphericTimeDelay


def createDelays(tecA1: list, tecA2: list, tecA3: list, tecA4: list, weightMatrix: list,
                 amountOfObservations: int) -> list:
    delays: list = []
    for observation in range(amountOfObservations):
        a1: int = tecA1[observation]
        a2: int = tecA2[observation]
        a3: int = tecA3[observation]
        a4: int = tecA4[observation]

        tec: list = []
        tec.extend([a1, a2, a3, a4])

        delay: float = getDelayInMeters(weightMatrix, tec)
        delays.append(delay)
    return delays


def createKlobuchar(latpp: float, lonpp: float,
                    elevationAngle: float, azimuth: float,
                    alphaArray: list, betaArray: list, gpsTimeArray: list,
                    amountOfObservations: int) -> list:
    models: list = []
    for observation in range(amountOfObservations):
        time: float = gpsTimeArray[observation]
        model: float = getKlobucharDelayInMeters(latpp, lonpp, elevationAngle, azimuth, time, alphaArray, betaArray)
        models.append(model)
    return models


def printDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int) -> None:
    print("igrg\tigsg\tklobuchar")
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" + str(round(klobucharValue, 3)))


def showDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int) -> None:
    forecastValues: list = []
    preciseValues: list = []
    klobucharValues: list = []
    observations = np.arange(0, amountOfObservations)
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        forecastValues.append(forecastValue)
        preciseValues.append(preciseValue)
        klobucharValues.append(klobucharValue)
    plt.plot(observations * 2, forecastValues, 'o-', label="igrg")
    plt.plot(observations * 2, preciseValues, 'o-', label="igsg")
    plt.plot(observations * 2, klobucharValues, 'o-', label="Klobuchar")
    plt.locator_params(axis='x', nbins=24)
    plt.xlabel("Время, час")
    plt.ylabel("Ионосферная поправка, метр")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 12

    halfCircle: float = 180

    latpp: float = %s / halfCircle
    lonpp: float = %s / halfCircle

    lat1: float = %s / halfCircle
    lat2: float = %s / halfCircle
    lon1: float = %s / halfCircle
    lon2: float = %s / halfCircle

    xpp: float = (lonpp - lon1) / (lon2 - lon1)
    ypp: float = (latpp - lat1) / (lat2 - lat1)

    weightMatrix: list = [0] * 4
    weightMatrix[0] = xpp * ypp
    weightMatrix[1] = (1 - xpp) * ypp
    weightMatrix[2] = (1 - xpp) * (1 - ypp)
    weightMatrix[3] = xpp * (1 - ypp)
    print(weightMatrix)

    alphaArray: list = [%s]
    betaArray: list = [%s]
    gpsTimeArray: list = [
        %s
    ]

    forecastA1: list = [%s]
    forecastA2: list = [%s]
    forecastA3: list = [%s]
    forecastA4: list = [%s]

    preciseA1: list = [%s]
    preciseA2: list = [%s]
    preciseA3: list = [%s]
    preciseA4: list = [%s]

    forecastDelays: list = createDelays(forecastA1, forecastA2, forecastA3, forecastA4,
                                        weightMatrix, amountOfObservations)
    preciseDelays: list = createDelays(preciseA1, preciseA2, preciseA3, preciseA4, weightMatrix, amountOfObservations)

    elevationAngle: float = 90 / halfCircle
    azimuth: float = 0
    klobucharDelays: list = createKlobuchar(latpp, lonpp, elevationAngle, azimuth, alphaArray, betaArray, gpsTimeArray,
                                            amountOfObservations)

    printDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    showDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)


if __name__ == "__main__":
    main()
