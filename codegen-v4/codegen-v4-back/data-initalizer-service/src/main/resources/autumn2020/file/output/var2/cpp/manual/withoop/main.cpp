﻿#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <iomanip>

class ElevationAngles {
public:
    ElevationAngles(std::vector<double> elevationAngles, int amountOfObservations) {
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = elevationAngles.at(observation);
            this->elevationAngles.push_back(elevationAngle);
        }
    }

    double getAngleAt(int observation) {
        return elevationAngles.at(observation);
    }

private:
    std::vector<double> elevationAngles;
};

class AngularVelocities {
public:
    AngularVelocities(ElevationAngles& elevationAngles, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        this->elevationAngles = &elevationAngles;
    }

    std::vector<double> getAverageAngularVelocities() {
        std::vector<double> averageVelocities;
        std::vector<double> angularVelocities = getAngularVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.push_back(firstHourAverage);
        averageVelocities.push_back(secondHourAverage);
        averageVelocities.push_back(thirdHourAverage);
        return averageVelocities;
    }

    std::vector<double> getAngularVelocities() {
        double oneHourInSeconds = 3600;
        double previousElevationAngle = elevationAngles->getAngleAt(0);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAngles->getAngleAt(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.push_back(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }
        return angularVelocities;
    }

private:
    int amountOfObservations;
    ElevationAngles* elevationAngles;
    std::vector<double> angularVelocities;

    double getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
        double velocitySum = 0;
        for (int observation = start; observation < end; observation++) {
            velocitySum += velocities.at(observation);
        }
        return velocitySum;
    }
};

class LinearVelocities {
public:
    LinearVelocities(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    std::vector<double> getAverageLinearVelocities() {
        std::vector<double> averageVelocities;
        std::vector<double> linearVelocities = getLinearVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(linearVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(linearVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(linearVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.push_back(firstHourAverage);
        averageVelocities.push_back(secondHourAverage);
        averageVelocities.push_back(thirdHourAverage);
        return averageVelocities;
    }

    std::vector<double> getLinearVelocities() {
        double gravitational = 6.67 * pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6371000;
        double flightHeight = 20000;

        for (int interval = 0; interval < amountOfObservations; interval++) {
            double linearVelocity = sqrt(gravitational * earthMass / (earthRadius + flightHeight));
            linearVelocities.push_back(linearVelocity);
        }
        return linearVelocities;
    }

private:
    int amountOfObservations;
    std::vector<double> linearVelocities;

    double getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
        double hourSum = 0;
        for (int observation = start; observation < end; observation++) {
            hourSum += velocities.at(observation);
        }
        return hourSum;
    }
};

class Satellite {
public:
    Satellite(int number, AngularVelocities& angularVelocities, LinearVelocities& linearVelocities) {
        this->number = number;
        this->angularVelocities = &angularVelocities;
        this->linearVelocities = &linearVelocities;
    }

    int getNumber() {
        return number;
    }

    auto getAngularVelocities() {
        auto velocities = angularVelocities->getAngularVelocities();
        return velocities;
    }

    auto getAverageAngularVelocities() {
        auto velocities = angularVelocities->getAverageAngularVelocities();
        return velocities;
    }

    auto getLinearVelocities() {
        auto velocities = linearVelocities->getLinearVelocities();
        return velocities;
    }

    auto getAverageLinearVelocities() {
        auto velocities = linearVelocities->getAverageLinearVelocities();
        return velocities;
    }

private:
    int number;
    AngularVelocities* angularVelocities;
    LinearVelocities* linearVelocities;
};

class SatelliteFactory {
public:
    SatelliteFactory(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    Satellite* createSatellite(int number, std::vector<double> elevationAnglesArray) {
        ElevationAngles* elevationAngles = new ElevationAngles(elevationAnglesArray, amountOfObservations);
        AngularVelocities* angularVelocities = new AngularVelocities(*elevationAngles, amountOfObservations);
        LinearVelocities* linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite* satellite = new Satellite(number, *angularVelocities, *linearVelocities);
        return satellite;
    }

private:
    int amountOfObservations;
};

class TemplateConsoleOutput {
public:
    TemplateConsoleOutput(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    void print() {
        int satellite1Number = getSatellite1Number();
        int satellite2Number = getSatellite2Number();
        int satellite3Number = getSatellite3Number();
        std::cout << "Satellite #" << satellite1Number << "\t\tSatellite #" << satellite2Number <<
            "\t\tSatellite #" << satellite3Number << std::endl;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = getSatellite1Velocities().at(observation);
            double satellite2Velocity = getSatellite2Velocities().at(observation);
            double satellite3Velocity = getSatellite3Velocities().at(observation);
            std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
        }
        std::cout << "***********************************************" << std::endl;
    }

protected:
    virtual int getSatellite1Number() = 0;
    virtual int getSatellite2Number() = 0;
    virtual int getSatellite3Number() = 0;
    virtual std::vector<double> getSatellite1Velocities() = 0;
    virtual std::vector<double> getSatellite2Velocities() = 0;
    virtual std::vector<double> getSatellite3Velocities() = 0;

private:
    int amountOfObservations;
};

class AngularVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AngularVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getSatellite1Velocities() override {
        auto velocities = satellite1->getAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite2Velocities() override {
        auto velocities = satellite2->getAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite3Velocities() override {
        auto velocities = satellite3->getAngularVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class AverageAngularVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AverageAngularVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getSatellite1Velocities() override {
        auto velocities = satellite1->getAverageAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite2Velocities() override {
        auto velocities = satellite2->getAverageAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite3Velocities() override {
        auto velocities = satellite3->getAverageAngularVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class LinearVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    LinearVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getSatellite1Velocities() override {
        auto velocities = satellite1->getLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite2Velocities() override {
        auto velocities = satellite2->getLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite3Velocities() override {
        auto velocities = satellite3->getLinearVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class AverageLinearVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AverageLinearVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getSatellite1Velocities() override {
        auto velocities = satellite1->getAverageLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite2Velocities() override {
        auto velocities = satellite2->getAverageLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getSatellite3Velocities() override {
        auto velocities = satellite3->getAverageLinearVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations, int amountOfHours) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
        this->amountOfHours = amountOfHours;
    }

    void getConsoleOutput() {
        AngularVelocitiesConsoleOutput angularVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
        angularVelocitiesOutput.print();

        AverageAngularVelocitiesConsoleOutput averageAngularVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfHours);
        averageAngularVelocitiesOutput.print();

        LinearVelocitiesConsoleOutput linearVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
        linearVelocitiesOutput.print();

        AverageLinearVelocitiesConsoleOutput averageLinearVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfHours);
        averageLinearVelocitiesOutput.print();
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
    int amountOfHours;
};

int main() {
    int amountOfObservations = 360;
    int amountOfHours = 3;

    int satellite1Number = %s;
    int satellite2Number = %s;
    int satellite3Number = %s;

    std::vector<double> satellite1ElevationAngles{
        %s
    };

    std::vector<double> satellite2ElevationAngles{
        %s
    };

    std::vector<double> satellite3ElevationAngles{
        %s
    };

    SatelliteFactory* satelliteFactory = new SatelliteFactory(amountOfObservations);

    Satellite* satellite1 = satelliteFactory->createSatellite(satellite1Number, satellite1ElevationAngles);
    Satellite* satellite2 = satelliteFactory->createSatellite(satellite2Number, satellite2ElevationAngles);
    Satellite* satellite3 = satelliteFactory->createSatellite(satellite3Number, satellite3ElevationAngles);

    ConsoleOutput* consoleOutput = new ConsoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations, amountOfHours);
    consoleOutput->getConsoleOutput();
}