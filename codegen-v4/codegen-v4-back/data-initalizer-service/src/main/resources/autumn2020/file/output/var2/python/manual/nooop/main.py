import math
import matplotlib.pyplot as plt
import numpy as np


def __getAngularVelocities(elevationAngles: list, amountOfObservations: int) -> list:
    angularVelocities: list = []
    oneHourInSeconds: float = 3600
    previousElevationAngle: float = elevationAngles[0]
    for observation in range(amountOfObservations):
        currentElevationAngle: float = elevationAngles[observation]
        angularVelocity: float = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds
        angularVelocities.append(angularVelocity)
        previousElevationAngle = currentElevationAngle
    return angularVelocities


def __getAverageAngularVelocities(elevationAngles: list, amountOfObservations: int) -> list:
    averageVelocities: list = []
    angularVelocities: list = __getAngularVelocities(elevationAngles, amountOfObservations)
    observationsPerHour: int = 120

    firstHourSum: float = __getVelocitySumPerHour(angularVelocities, 0, 120)
    secondHourSum: float = __getVelocitySumPerHour(angularVelocities, 120, 240)
    thirdHourSum: float = __getVelocitySumPerHour(angularVelocities, 240, 360)

    firstHourAverage: float = firstHourSum / observationsPerHour
    secondHourAverage: float = secondHourSum / observationsPerHour
    thirdHourAverage: float = thirdHourSum / observationsPerHour
    averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
    return averageVelocities


def __getLinearVelocities(amountOfObservations: int) -> list:
    gravitational: float = 6.67 * pow(10, -11)
    earthMass: float = 5.972E24
    earthRadius: float = 6_371_000
    flightHeight: float = 20_000
    linearVelocities: list = []
    for observation in range(amountOfObservations):
        linearVelocities.append(math.sqrt(gravitational * earthMass / (earthRadius + flightHeight)))
    return linearVelocities


def __getAverageLinearVelocities(amountOfObservations: int) -> list:
    averageVelocities: list = []
    linearVelocities: list = __getLinearVelocities(amountOfObservations)
    observationsPerHour: int = 120

    firstHourSum: float = __getVelocitySumPerHour(linearVelocities, 0, 120)
    secondHourSum: float = __getVelocitySumPerHour(linearVelocities, 120, 240)
    thirdHourSum: float = __getVelocitySumPerHour(linearVelocities, 240, 360)

    firstHourAverage: float = firstHourSum / observationsPerHour
    secondHourAverage: float = secondHourSum / observationsPerHour
    thirdHourAverage: float = thirdHourSum / observationsPerHour
    averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
    return averageVelocities


def __getVelocitySumPerHour(velocities: list, start: int, end: int) -> float:
    velocitySum: float = 0
    for observation in range(start, end):
        velocitySum += velocities[observation]
    return velocitySum


def __printAngularVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                             satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                             satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    satellite1AngularVelocities: list = __getAngularVelocities(satellite1ElevationAngles, amountOfObservations)
    satellite2AngularVelocities: list = __getAngularVelocities(satellite2ElevationAngles, amountOfObservations)
    satellite3AngularVelocities: list = __getAngularVelocities(satellite3ElevationAngles, amountOfObservations)

    print("Угловая скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1Velocity: float = satellite1AngularVelocities[observation]
        satellite2Velocity: float = satellite2AngularVelocities[observation]
        satellite3Velocity: float = satellite3AngularVelocities[observation]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printAverageAngularVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                    satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                                    satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    satellite1AverageAngularVelocities: list = __getAverageAngularVelocities(satellite1ElevationAngles,
                                                                             amountOfObservations)
    satellite2AverageAngularVelocities: list = __getAverageAngularVelocities(satellite2ElevationAngles,
                                                                             amountOfObservations)
    satellite3AverageAngularVelocities: list = __getAverageAngularVelocities(satellite3ElevationAngles,
                                                                             amountOfObservations)

    print("Средняя угловая скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for hour in range(3):
        satellite1Velocity: float = satellite1AverageAngularVelocities[hour]
        satellite2Velocity: float = satellite2AverageAngularVelocities[hour]
        satellite3Velocity: float = satellite3AverageAngularVelocities[hour]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printLinearVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                            amountOfObservations: int) -> None:
    satellite1LinearVelocities: list = __getLinearVelocities(amountOfObservations)
    satellite2LinearVelocities: list = __getLinearVelocities(amountOfObservations)
    satellite3LinearVelocities: list = __getLinearVelocities(amountOfObservations)

    print("Линейная скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1Velocity: float = satellite1LinearVelocities[observation]
        satellite2Velocity: float = satellite2LinearVelocities[observation]
        satellite3Velocity: float = satellite3LinearVelocities[observation]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printAverageLinearVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                   amountOfObservations: int) -> None:
    satellite1AverageLinearVelocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite2AverageLinearVelocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite3AverageLinearVelocities: list = __getAverageLinearVelocities(amountOfObservations)

    print("Средняя линейная скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for hour in range(3):
        satellite1Velocity: float = satellite1AverageLinearVelocities[hour]
        satellite2Velocity: float = satellite2AverageLinearVelocities[hour]
        satellite3Velocity: float = satellite3AverageLinearVelocities[hour]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def printVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                    satellite1ElevationAngles: list, satellite2ElevationAngles: list, satellite3ElevationAngles: list,
                    amountOfObservations: int) -> None:
    __printAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles,
                             satellite2ElevationAngles, satellite3ElevationAngles, amountOfObservations)
    __printAverageAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles,
                                    satellite2ElevationAngles, satellite3ElevationAngles, amountOfObservations)
    __printLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)
    __printAverageLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)


def __drawAngularVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                 satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                                 satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    satellite1Velocities: list = []
    satellite2Velocities: list = []
    satellite3Velocities: list = []
    observations = np.arange(1, amountOfObservations + 1)
    for observation in range(amountOfObservations):
        satellite1Velocity: float = __getAngularVelocities(satellite1ElevationAngles, amountOfObservations)[observation]
        satellite2Velocity: float = __getAngularVelocities(satellite2ElevationAngles, amountOfObservations)[observation]
        satellite3Velocity: float = __getAngularVelocities(satellite3ElevationAngles, amountOfObservations)[observation]
        satellite1Velocities.append(satellite1Velocity)
        satellite2Velocities.append(satellite2Velocity)
        satellite3Velocities.append(satellite3Velocity)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Угловая скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawAverageAngularVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                        satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                                        satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    satellite1Velocities: list = __getAverageAngularVelocities(satellite1ElevationAngles, amountOfObservations)
    satellite2Velocities: list = __getAverageAngularVelocities(satellite2ElevationAngles, amountOfObservations)
    satellite3Velocities: list = __getAverageAngularVelocities(satellite3ElevationAngles, amountOfObservations)
    observations = np.arange(1, 4)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, часы")
    plt.ylabel("Средняя угловая скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawLinearVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                amountOfObservations: int) -> None:
    satellite1Velocities: list = []
    satellite2Velocities: list = []
    satellite3Velocities: list = []
    observations = np.arange(1, amountOfObservations + 1)
    for observation in range(amountOfObservations):
        satellite1Velocity: float = __getLinearVelocities(amountOfObservations)[observation]
        satellite2Velocity: float = __getLinearVelocities(amountOfObservations)[observation]
        satellite3Velocity: float = __getLinearVelocities(amountOfObservations)[observation]
        satellite1Velocities.append(satellite1Velocity)
        satellite2Velocities.append(satellite2Velocity)
        satellite3Velocities.append(satellite3Velocity)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Линейная скорость, м/с")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawAverageLinearVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                       amountOfObservations: int) -> None:
    satellite1Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite2Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite3Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    observations = np.arange(1, 4)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, часы")
    plt.ylabel("Средняя линейная скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def drawVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int, satellite1ElevationAngles: list,
                   satellite2ElevationAngles: list, satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    __drawAngularVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles,
                                 satellite2ElevationAngles, satellite3ElevationAngles, amountOfObservations)
    __drawAverageAngularVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, satellite1ElevationAngles,
                                        satellite2ElevationAngles, satellite3ElevationAngles, amountOfObservations)
    __drawLinearVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)
    __drawAverageLinearVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)


def main():
    amountOfObservations: int = 360

    satellite1Number: int = %s
    satellite2Number: int = %s
    satellite3Number: int = %s

    satellite1ElevationAngles: list = [
        %s
    ]

    satellite2ElevationAngles: list = [
        %s
    ]

    satellite3ElevationAngles: list = [
        %s
    ]

    printVelocities(satellite1Number, satellite2Number, satellite3Number,
                    satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
                    amountOfObservations)

    drawVelocities(satellite1Number, satellite2Number, satellite3Number,
                   satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
                   amountOfObservations)


if __name__ == '__main__':
    main()
