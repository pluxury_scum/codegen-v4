import matplotlib.pyplot as plt
import numpy as np

def __getIonosphericDelayAt(pseudoRanges1: list, pseudoRanges2: list, interval: int) -> float:
    speedOfLight: float = 2.99792458 * 1E8
    p1: float = pseudoRanges1[interval]
    p2: float = pseudoRanges2[interval]
    k: float = __getK()
    delay: float = (p1 - p2) / (speedOfLight * (1 - k))
    delayInMeters: float = delay * speedOfLight
    return delayInMeters

def __getK() -> float:
    f1: float = 1_575_420_000
    f2: float = 1_227_600_000
    k: float = pow(f1, 2) / pow(f2, 2)
    return k

def printDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int, satellite1P1: list,
                satellite1P2: list, satellite2P1: list, satellite2P2: list, satellite3P1: list, satellite3P2: list,
                amountOfObservations: int) -> None:
    print("Ионосферная задержка\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        delay1: float = __getIonosphericDelayAt(satellite1P1, satellite1P2, observation)
        delay2: float = __getIonosphericDelayAt(satellite2P1, satellite2P2, observation)
        delay3: float = __getIonosphericDelayAt(satellite3P1, satellite3P2, observation)
        print(str(round(delay1, 10)) + "\t" + str(round(delay2, 10)) + "\t" + str(round(delay3, 10)))

def showDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int, satellite1P1: list,
               satellite1P2: list, satellite2P1: list, satellite2P2: list, satellite3P1: list, satellite3P2: list,
               amountOfObservations: int) -> None:
    satellite1Delays: list = []
    satellite2Delays: list = []
    satellite3Delays: list = []
    observations = np.arange(0, amountOfObservations)
    for observation in range(amountOfObservations):
        delay1: float = __getIonosphericDelayAt(satellite1P1, satellite1P2, observation)
        delay2: float = __getIonosphericDelayAt(satellite2P1, satellite2P2, observation)
        delay3: float = __getIonosphericDelayAt(satellite3P1, satellite3P2, observation)
        satellite1Delays.append(delay1)
        satellite2Delays.append(delay2)
        satellite3Delays.append(delay3)
    plt.plot(observations, satellite1Delays, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Delays, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Delays, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Ионосферная задержка, метры")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()

def main():
    amountOfObservations: int = 360

    satellite1Number: int = %s
    satellite2Number: int = %s
    satellite3Number: int = %s

    satellite1P1: list = [
        %s
    ]

    satellite1P2: list = [
        %s
    ]

    satellite2P1: list = [
        %s
    ]

    satellite2P2: list = [
        %s
    ]

    satellite3P1: list = [
        %s
    ]

    satellite3P2: list = [
        %s
    ]

    printDelays(satellite1Number, satellite2Number, satellite3Number,
                satellite1P1, satellite1P2, satellite2P1, satellite2P2, satellite3P1, satellite3P2,
                amountOfObservations)

    showDelays(satellite1Number, satellite2Number, satellite3Number,
               satellite1P1, satellite1P2, satellite2P1, satellite2P2, satellite3P1, satellite3P2,
               amountOfObservations)

if __name__ == '__main__':
    main()
