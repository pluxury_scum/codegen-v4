import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.List;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int amountOfObservations = 12;

        int forecastListFirstLine = 304;
        int preciseListFirstLine = 394;

        int satelliteNumber = %s;

        double latpp = %s;
        double lonpp = %s;
        UserGeo userGeo = new UserGeo(latpp, lonpp);

        double lat1 = %s;
        double lat2 = %s;
        int lon1 = %s;
        int lon2 = %s;
        IgpGeo igpGeo = new IgpGeo(lon1, lon2, lat1, lat2);

        AxisGeo axisGeo = new AxisGeo(userGeo, igpGeo);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        String fileNameEphemeris = "./src/resources/brdc0010.18n";
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(amountOfObservations, fileNameEphemeris);
        IonCoefficients alpha = new IonCoefficients(ephemerisFileReader.getAlpha());
        IonCoefficients beta = new IonCoefficients(ephemerisFileReader.getBeta());
        GpsTime gpsTime = new GpsTime(ephemerisFileReader.getGpsTime(satelliteNumber));

        String fileNameForecast = "./src/resources/igrg0010.18i";
        IonoFileReader ionoFileReaderForecast = new IonoFileReader(fileNameForecast);

        List<List<List<Integer>>> forecastA1 = ionoFileReaderForecast.getTecArray(lat2, forecastListFirstLine);
        List<List<List<Integer>>> forecastA2 = ionoFileReaderForecast.getTecArray(lat2, forecastListFirstLine);
        List<List<List<Integer>>> forecastA3 = ionoFileReaderForecast.getTecArray(lat1, forecastListFirstLine);
        List<List<List<Integer>>> forecastA4 = ionoFileReaderForecast.getTecArray(lat1, forecastListFirstLine);

        String fileNameReal = "./src/resources/igsg0010.18i";
        IonoFileReader ionoFileReaderReal = new IonoFileReader(fileNameReal);
        List<List<List<Integer>>> preciseA1 = ionoFileReaderReal.getTecArray(lat2, preciseListFirstLine);
        List<List<List<Integer>>> preciseA2 = ionoFileReaderReal.getTecArray(lat2, preciseListFirstLine);
        List<List<List<Integer>>> preciseA3 = ionoFileReaderReal.getTecArray(lat1, preciseListFirstLine);
        List<List<List<Integer>>> preciseA4 = ionoFileReaderReal.getTecArray(lat1, preciseListFirstLine);

        IonosphericDelaysFactory ionosphericDelaysFactory = new IonosphericDelaysFactory(weightMatrix, lon2, lon1, lon1, lon2, amountOfObservations);
        KlobucharDelaysFactory klobucharDelaysFactory = new KlobucharDelaysFactory(latpp, lonpp, gpsTime, alpha, beta, amountOfObservations);

        List<IonosphericDelay> forecastDelays = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4);
        List<IonosphericDelay> preciseDelays = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4);
        List<KlobucharModel> klobucharDelays = klobucharDelaysFactory.createKlobuchar();

        init(primaryStage, forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
    }

    @SuppressWarnings("unchecked")
    private void init(Stage primaryStage, List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModel> klobucharDelays,
        int amountOfObservations) {
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время, час");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Ионосферная поправка, метр");

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> forecastData = new XYChart.Series<>();
        forecastData.setName("igrg");
        XYChart.Series<Number, Number> preciseData = new XYChart.Series<>();
        preciseData.setName("igsg");
        XYChart.Series<Number, Number> klobucharData = new XYChart.Series<>();
        klobucharData.setName("Klobuchar");

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastDelay = forecastDelays.get(observation).getDelayInMeters();
            forecastData.getData().add(new XYChart.Data<>(observation, forecastDelay));

            double preciseDelay = preciseDelays.get(observation).getDelayInMeters();
            preciseData.getData().add(new XYChart.Data<>(observation, preciseDelay));

            double klobucharDelay = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            klobucharData.getData().add(new XYChart.Data<>(observation, klobucharDelay));
        }

        lineChart.getData().addAll(forecastData, preciseData, klobucharData);
        root.getChildren().add(lineChart);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
