import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 12;

        double latpp = %s;
        double lonpp = %s;
        UserGeo userGeo = new UserGeo(latpp, lonpp);

        double lat1 = %s;
        double lat2 = %s;
        int lon1 = %s;
        int lon2 = %s;
        IgpGeo igpGeo = new IgpGeo(lon1, lon2, lat1, lat2);

        AxisGeo axisGeo = new AxisGeo(userGeo, igpGeo);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Double> alphaArray = asList( %s );
        List<Double> betaArray = asList( %s );
        List<Double> gpsTimeArray = asList(
            %s
        );

        IonCoefficients alpha = new IonCoefficients(alphaArray);
        IonCoefficients beta = new IonCoefficients(betaArray);
        GpsTime gpsTime = new GpsTime(gpsTimeArray);

        List<Integer> forecastA1 = asList( %s );
        List<Integer> forecastA2 = asList( %s );
        List<Integer> forecastA3 = asList( %s );
        List<Integer> forecastA4 = asList( %s );

        List<Integer> preciseA1 = asList( %s );
        List<Integer> preciseA2 = asList( %s );
        List<Integer> preciseA3 = asList( %s );
        List<Integer> preciseA4 = asList( %s );

        List<IonosphericDelay> forecastDelays = IonosphericDelaysFactory.createDelays(weightMatrix, forecastA1, forecastA2, forecastA3, forecastA4,
            amountOfObservations);
        List<IonosphericDelay> preciseDelays = IonosphericDelaysFactory.createDelays(weightMatrix, preciseA1, preciseA2, preciseA3, preciseA4,
            amountOfObservations);
        List<KlobucharModel> klobucharDelays = KlobucharDelaysFactory.createKlobuchar(latpp, lonpp, gpsTime, alpha, beta, amountOfObservations);

        ConsoleOutput consoleOutput = new ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
        consoleOutput.printDelays();

        GraphDrawer.draw();
    }
}

class UserGeo {
    private double latpp;
    private double lonpp;

    public UserGeo(double lat, double lon) {
        double halfCircle = 180;
        this.latpp = lat / halfCircle;
        this.lonpp = lon / halfCircle;
    }

    public double getLatpp() {
        return latpp;
    }

    public double getLonpp() {
        return lonpp;
    }
}

class IgpGeo {
    private double lon1;
    private double lon2;
    private double lat1;
    private double lat2;

    public IgpGeo(double lon1, double lon2, double lat1, double lat2) {
        double halfCircle = 180;
        this.lon1 = lon1 / halfCircle;
        this.lon2 = lon2 / halfCircle;
        this.lat1 = lat1 / halfCircle;
        this.lat2 = lat2 / halfCircle;
    }

    public double getLon1() {
        return lon1;
    }

    public double getLon2() {
        return lon2;
    }

    public double getLat1() {
        return lat1;
    }

    public double getLat2() {
        return lat2;
    }
}

class AxisGeo {
    private double xpp;
    private double ypp;

    public AxisGeo(UserGeo userGeo, IgpGeo igpGeo) {
        double lonpp = userGeo.getLonpp();
        double latpp = userGeo.getLatpp();

        double lon1 = igpGeo.getLon1();
        double lon2 = igpGeo.getLon2();

        double lat1 = igpGeo.getLat1();
        double lat2 = igpGeo.getLat2();

        this.xpp = (lonpp - lon1) / (lon2 - lon1);
        this.ypp = (latpp - lat1) / (lat2 - lat1);
    }

    public double getXpp() {
        return xpp;
    }

    public double getYpp() {
        return ypp;
    }
}

class WeightMatrix {
    private List<Double> weights;

    public WeightMatrix(AxisGeo axisGeo) {
        this.weights = new ArrayList<>(4);
        double xpp = axisGeo.getXpp();
        double ypp = axisGeo.getYpp();
        weights.add(xpp * ypp);
        weights.add((1 - xpp) * ypp);
        weights.add((1 - xpp) * (1 - ypp));
        weights.add(xpp * (1 - ypp));
    }

    public double getWeightAt(int pos) {
        return weights.get(pos);
    }
}

class ConsoleOutput {
    private List<IonosphericDelay> forecastDelays;
    private List<IonosphericDelay> preciseDelays;
    private List<KlobucharModel> klobucharDelays;
    private int amountOfObservations;

    public ConsoleOutput(List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModel> klobucharDelays,
        int amountOfObservations) {
        this.forecastDelays = forecastDelays;
        this.preciseDelays = preciseDelays;
        this.klobucharDelays = klobucharDelays;
        this.amountOfObservations = amountOfObservations;
    }

    public void printDelays() {
        System.out.println("igrg\tigsg\tklobuchar");
        DecimalFormat df = new DecimalFormat("#.###");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastValue = forecastDelays.get(observation).getDelayInMeters();
            double preciseValue = preciseDelays.get(observation).getDelayInMeters();
            double klobucharValue = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            System.out.println(df.format(forecastValue) + "\t" + df.format(preciseValue) + "\t" + klobucharValue);
        }
    }
}

class IonosphericDelaysFactory {
    public static List<IonosphericDelay> createDelays(WeightMatrix weightMatrix, List<Integer> tecA1, List<Integer> tecA2,
        List<Integer> tecA3, List<Integer> tecA4, int amountOfObservations) {
        List<IonosphericDelay> delays = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            int a1 = tecA1.get(observation);
            int a2 = tecA2.get(observation);
            int a3 = tecA3.get(observation);
            int a4 = tecA4.get(observation);

            List<Integer> tecArray = asList(a1, a2, a3, a4);

            Tec tempTec = new Tec(tecArray);
            IonosphericDelay tempDelay = new IonosphericDelay(weightMatrix, tempTec);
            delays.add(tempDelay);
        }
        return delays;
    }
}

class KlobucharDelaysFactory {
    public static List<KlobucharModel> createKlobuchar(
        double latpp, double lonpp, GpsTime gpsTime, IonCoefficients alpha, IonCoefficients beta, int amountOfObservations
    ) {
        List<KlobucharModel> models = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime.getGpsTimeAt(observation);
            KlobucharModel klobucharTemp = new KlobucharModel(latpp, lonpp, time, alpha, beta);
            models.add(klobucharTemp);
        }
        return models;
    }
}

class IonCoefficients {
    private List<Double> coefficients;

    public IonCoefficients(List<Double> coefficients) {
        this.coefficients = coefficients;
    }

    public double getCoefficientAt(int pos) {
        return coefficients.get(pos);
    }
}

class Tec {
    private List<Integer> tec;

    public Tec(List<Integer> tec) {
        this.tec = tec;
    }

    public double getTecAt(int pos) {
        return tec.get(pos);
    }
}

class GpsTime {
    private List<Double> gpsTime;

    public GpsTime(List<Double> gpsTime) {
        this.gpsTime = gpsTime;
    }

    public double getGpsTimeAt(int pos) {
        return gpsTime.get(pos);
    }
}

class IonosphericDelay {
    private WeightMatrix weightMatrix;
    private Tec tec;

    public IonosphericDelay(WeightMatrix weightMatrix, Tec tec) {
        this.weightMatrix = weightMatrix;
        this.tec = tec;
    }

    public double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();
        double delayInMeters = delayInTecu * tecuToMetersCoefficient;
        return delayInMeters;
    }

    private double getTecuToMetersCoefficient() {
        double l1 = 1_575_420_000;
        double oneTecUnit = 1E16;
        double coefficient = 40.3 / Math.pow(l1, 2) * oneTecUnit;
        return coefficient;
    }

    private double getDelayInTecu() {
        double delay = 0;
        for (int observation = 0; observation < 4; observation++) {
            double weight = weightMatrix.getWeightAt(observation);
            double rawTec = tec.getTecAt(observation);
            double tecInOneTecUnit = rawTec * 0.1;
            delay += (weight * tecInOneTecUnit);
        }
        return delay;
    }
}

class KlobucharModel {
    private double latpp;
    private double lonpp;
    private double elevationAngle;
    private double azimuth;
    private double gpsTime;
    private IonCoefficients alpha;
    private IonCoefficients beta;

    public KlobucharModel(double latpp, double lonpp, double gpsTime, IonCoefficients alpha, IonCoefficients beta) {
        double halfCircle = 180;
        this.latpp = latpp;
        this.lonpp = lonpp;
        this.gpsTime = gpsTime;
        this.elevationAngle = 90 / halfCircle;
        this.azimuth = 0;
        this.alpha = alpha;
        this.beta = beta;
    }

    public double getKlobucharDelayInMeters() {
        double delayInSeconds = getKlobucharDelayInSeconds();
        double speedOfLight = 2.99792458 * 1E8;
        double delayInMeters = delayInSeconds * speedOfLight;
        return delayInMeters;
    }

    private double getEarthCenteredAngle() {
        double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
        return earthCenteredAngle;
    }

    private double getIppLatitude() {
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * Math.cos(azimuth);
        if (ippLatitude > 0.416)
            ippLatitude = 0.416;
        else if (ippLatitude < -0.416)
            ippLatitude = -0.416;
        return ippLatitude;
    }

    private double getIppLongtitude() {
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();
        double ippLongtitude = lonpp + (earthCenteredAngle * Math.sin(azimuth) / (Math.cos(ippLatitude)));
        return ippLongtitude;
    }

    private double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();
        double ippGeomagneticLatitude = ippLatitude + 0.064 * Math.cos(ippLongtitude - 1.617);
        return ippGeomagneticLatitude;
    }

    private double getIppLocalTime() {
        double secondsInOneDay = 86_400;
        double secondsInTwelveHours = 43_200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
        while (ippLocalTime > secondsInOneDay)
            ippLocalTime -= secondsInOneDay;
        while (ippLocalTime < 0)
            ippLocalTime += secondsInOneDay;
        return ippLocalTime;
    }

    private double getIonosphericDelayAmplitude() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double amplitude = 0;
        for (int i = 0; i < 4; i++) {
            amplitude += (alpha.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (amplitude < 0)
            amplitude = 0;
        return amplitude;
    }

    private double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double period = 0;
        for (int i = 0; i < 4; i++) {
            period += (beta.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (period < 72_000)
            period = 72_000;
        return period;
    }

    private double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        double ionosphericDelayPhase = 2 * Math.PI * (ippLocalTime - 50_400) / ionosphericDelayPeriod;
        return ionosphericDelayPhase;
    }

    private double getSlantFactor() {
        double slantFactor = 1 + 16 * Math.pow((0.53 - elevationAngle), 3);
        return slantFactor;
    }

    private double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();
        double ionosphericTimeDelay;
        if (Math.abs(ionosphericDelayPhase) > 1.57)
            ionosphericTimeDelay = 5E-9 * slantFactor;
        else
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - Math.pow(ionosphericDelayPhase, 2) / 2 + Math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        return ionosphericTimeDelay;
    }
}