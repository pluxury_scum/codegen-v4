package com.gvozdev.datainitializerservice.repository.autumn2020;

import com.gvozdev.datainitializerservice.entity.autumn2020.Autumn2020OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface Autumn2020OutputFileRepository extends JpaRepository<Autumn2020OutputFile, Long> {

    @Query(
        value = """
            SELECT * FROM autumn_2020_output_file f\040
            WHERE f.var = ?1\040
            AND f.lang = ?2\040
            AND f.file = ?3\040
            AND f.oop = ?4\040
            AND f.name = ?5\040
            """, nativeQuery = true
    )
    Autumn2020OutputFile findFileByParameters(String var, String lang, String file, String oop, String name);
}
