package com.gvozdev.datainitializerservice.initializer.autumn2020;

import com.gvozdev.datainitializerservice.DataInitializerServiceApplication;
import com.gvozdev.datainitializerservice.entity.autumn2020.Autumn2020OutputFile;
import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.gvozdev.datainitializerservice.repository.autumn2020.Autumn2020OutputFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Date;

import static com.gvozdev.datainitializerservice.initializer.path.ResourcePaths.AUTUMN2020_OUTPUT_FILE;
import static java.text.MessageFormat.format;
import static java.util.Objects.requireNonNull;

@Slf4j
@Component
public class Autumn2020OutputFilesDatabaseInitializer implements Initializer {
    private final Autumn2020OutputFileRepository outputFileRepository;

    public Autumn2020OutputFilesDatabaseInitializer(Autumn2020OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    @Transactional
    public void initialize() {
        saveOutputFile(1L, "var1", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(2L, "var1", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(3L, "var1", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(4L, "var1", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(5L, "var1", "java", "file", "withoop", "Main.java");
        saveOutputFile(6L, "var1", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(7L, "var1", "java", "manual", "withoop", "Main.java");
        saveOutputFile(8L, "var1", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(9L, "var1", "python", "file", "nooop", "main.py");
        saveOutputFile(10L, "var1", "python", "file", "withoop", "main.py");
        saveOutputFile(11L, "var1", "python", "manual", "nooop", "main.py");
        saveOutputFile(12L, "var1", "python", "manual", "withoop", "main.py");
        saveOutputFile(13L, "var2", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(14L, "var2", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(15L, "var2", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(16L, "var2", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(17L, "var2", "java", "file", "withoop", "Main.java");
        saveOutputFile(18L, "var2", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(19L, "var2", "java", "manual", "withoop", "Main.java");
        saveOutputFile(20L, "var2", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(21L, "var2", "python", "file", "nooop", "main.py");
        saveOutputFile(22L, "var2", "python", "file", "withoop", "main.py");
        saveOutputFile(23L, "var2", "python", "manual", "nooop", "main.py");
        saveOutputFile(24L, "var2", "python", "manual", "withoop", "main.py");
        saveOutputFile(25L, "var3", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(26L, "var3", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(27L, "var3", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(28L, "var3", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(29L, "var3", "java", "file", "withoop", "Main.java");
        saveOutputFile(30L, "var3", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(31L, "var3", "java", "manual", "withoop", "Main.java");
        saveOutputFile(32L, "var3", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(33L, "var3", "python", "file", "nooop", "main.py");
        saveOutputFile(34L, "var3", "python", "file", "withoop", "main.py");
        saveOutputFile(35L, "var3", "python", "manual", "nooop", "main.py");
        saveOutputFile(36L, "var3", "python", "manual", "withoop", "main.py");

        log.info("Все выходные файлы для УИРС осени 2020 успешно загружены");
    }

    private void saveOutputFile(long id, String var, String lang, String file, String oop, String name) {
        try {
            String path = format(
                AUTUMN2020_OUTPUT_FILE,
                var, lang, file, oop, name
            );

            URI uri = requireNonNull(DataInitializerServiceApplication.class.getClassLoader().getResource(path)).toURI();
            File codeListing = new File(uri);
            byte[] fileBytes = Files.readAllBytes(codeListing.toPath());
            Autumn2020OutputFile outputFile = new Autumn2020OutputFile(id, var, lang, file, oop, name, fileBytes, new Date());

            outputFileRepository.save(outputFile);
        } catch (IOException | URISyntaxException exception) {
            exception.printStackTrace();
        }
    }
}
