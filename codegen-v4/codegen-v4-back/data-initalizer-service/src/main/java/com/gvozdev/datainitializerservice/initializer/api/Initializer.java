package com.gvozdev.datainitializerservice.initializer.api;

public interface Initializer {
    void initialize();
}
