package com.gvozdev.datainitializerservice.repository.spring2021;

import com.gvozdev.datainitializerservice.entity.spring2021.Spring2021OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface Spring2021OutputFileRepository extends JpaRepository<Spring2021OutputFile, Long> {

    @Query(
        value = """
            SELECT * FROM spring_2021_output_file f\040
            WHERE f.lang = ?1\040
            AND f.file = ?2\040
            AND f.oop = ?3\040
            AND f.name = ?4\040
            """, nativeQuery = true
    )
    Spring2021OutputFile findFileByParameters(String lang, String file, String oop, String name);
}
