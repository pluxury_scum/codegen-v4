package com.gvozdev.datainitializerservice.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DataSourceConfig {

    @Bean("FilesDataSource")
    @Primary
    @ConfigurationProperties("spring.files.datasource.main")
    public HikariDataSource filesDataSource() {
        return DataSourceBuilder
            .create()
            .type(HikariDataSource.class)
            .build();
    }

    @Bean("SecurityDataSource")
    @ConfigurationProperties("spring.security.datasource.main")
    public HikariDataSource securityDataSource() {
        return DataSourceBuilder
            .create()
            .type(HikariDataSource.class)
            .build();
    }
}