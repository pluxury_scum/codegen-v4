package com.gvozdev.datainitializerservice.initializer.path;

public class ResourcePaths {
    public static final String SECURITY_SCHEMA = "security/schema.sql";

    public static final String AUTUMN2020_SCHEMA = "autumn2020/schema.sql";

    public static final String SPRING2021_SCHEMA = "spring2021/schema.sql";

    public static final String AUTUMN2020_INPUT_FILE = "autumn2020/file/input/{0}";

    public static final String AUTUMN2020_OUTPUT_FILE = "autumn2020/file/output/{0}/{1}/{2}/{3}/{4}";

    public static final String SPRING2021_INPUT_FILE = "spring2021/file/input/{0}";

    public static final String SPRING2021_OUTPUT_FILE = "spring2021/file/output/{0}/{1}/{2}/{3}";

    private ResourcePaths() {
    }
}
