package com.gvozdev.datainitializerservice.initializer.spring2021;

import com.gvozdev.datainitializerservice.DataInitializerServiceApplication;
import com.gvozdev.datainitializerservice.entity.spring2021.Spring2021OutputFile;
import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.gvozdev.datainitializerservice.repository.spring2021.Spring2021OutputFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Date;

import static com.gvozdev.datainitializerservice.initializer.path.ResourcePaths.SPRING2021_OUTPUT_FILE;
import static java.text.MessageFormat.format;
import static java.util.Objects.requireNonNull;

@Slf4j
@Component
public class Spring2021OutputFilesDatabaseInitializer implements Initializer {
    private final Spring2021OutputFileRepository outputFileRepository;

    public Spring2021OutputFilesDatabaseInitializer(Spring2021OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    @Transactional
    public void initialize() {
        saveOutputFile(1L, "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(2L, "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(3L, "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(4L, "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(5L, "java", "file", "withoop", "Main.java");
        saveOutputFile(6L, "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(7L, "java", "manual", "withoop", "Main.java");
        saveOutputFile(8L, "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(9L, "python", "file", "nooop", "main.py");
        saveOutputFile(10L, "python", "file", "withoop", "main.py");
        saveOutputFile(11L, "python", "manual", "nooop", "main.py");
        saveOutputFile(12L, "python", "manual", "withoop", "main.py");

        log.info("Все выходные файлы для УИРС весны 2021 успешно загружены");
    }

    private void saveOutputFile(long id, String lang, String file, String oop, String name) {
        try {
            String path = format(
                SPRING2021_OUTPUT_FILE,
                lang, file, oop, name
            );

            URI uri = requireNonNull(DataInitializerServiceApplication.class.getClassLoader().getResource(path)).toURI();
            File codeListing = new File(uri);
            byte[] fileBytes = Files.readAllBytes(codeListing.toPath());
            Spring2021OutputFile inputFile = new Spring2021OutputFile(id, lang, file, oop, name, fileBytes, new Date());

            outputFileRepository.save(inputFile);
        } catch (IOException | URISyntaxException exception) {
            exception.printStackTrace();
        }
    }
}
