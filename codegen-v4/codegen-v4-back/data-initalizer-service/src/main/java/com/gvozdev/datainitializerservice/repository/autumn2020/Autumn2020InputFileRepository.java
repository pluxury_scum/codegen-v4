package com.gvozdev.datainitializerservice.repository.autumn2020;

import com.gvozdev.datainitializerservice.entity.autumn2020.Autumn2020InputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Autumn2020InputFileRepository extends JpaRepository<Autumn2020InputFile, Long> {
    Autumn2020InputFile findByName(String name);
}
