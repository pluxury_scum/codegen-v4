package com.gvozdev.datainitializerservice.controller;

import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.gvozdev.datainitializerservice.initializer.autumn2020.Autumn2020InputFilesDatabaseInitializer;
import com.gvozdev.datainitializerservice.initializer.autumn2020.Autumn2020OutputFilesDatabaseInitializer;
import com.gvozdev.datainitializerservice.initializer.spring2021.Spring2021InputFilesDatabaseInitializer;
import com.gvozdev.datainitializerservice.initializer.spring2021.Spring2021OutputFilesDatabaseInitializer;
import com.gvozdev.datainitializerservice.initializer.sql.SqlScriptInitializer;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.gvozdev.datainitializerservice.initializer.path.ResourcePaths.*;
import static org.springframework.http.HttpStatus.OK;

@Controller
@RequestMapping("/api/initializer")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private final ApplicationContext applicationContext;

    public PageController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @GetMapping("/security/initialize")
    public ResponseEntity<String> initializeSecurityData() {
        HikariDataSource dataSource = applicationContext.getBean("SecurityDataSource", HikariDataSource.class);

        Initializer sqlScriptInitializer = new SqlScriptInitializer(dataSource, SECURITY_SCHEMA);
        sqlScriptInitializer.initialize();

        return new ResponseEntity<>("Данные Security сервиса успешно загружены", OK);
    }

    @GetMapping("/autumn2020/initialize")
    public ResponseEntity<String> initializeAutumn2020Data() {
        HikariDataSource dataSource = applicationContext.getBean("FilesDataSource", HikariDataSource.class);

        Initializer sqlScriptInitializer = new SqlScriptInitializer(dataSource, AUTUMN2020_SCHEMA);
        sqlScriptInitializer.initialize();

        Initializer inputFilesInitializer = applicationContext.getBean(Autumn2020InputFilesDatabaseInitializer.class);
        inputFilesInitializer.initialize();

        Initializer outputFilesInitializer = applicationContext.getBean(Autumn2020OutputFilesDatabaseInitializer.class);
        outputFilesInitializer.initialize();

        return new ResponseEntity<>("Данные сервиса Autumn2020 успешно загружены", OK);
    }

    @GetMapping("/spring2021/initialize")
    public ResponseEntity<String> initializeSpring2021Data() {
        HikariDataSource dataSource = applicationContext.getBean("FilesDataSource", HikariDataSource.class);

        Initializer sqlScriptInitializer = new SqlScriptInitializer(dataSource, SPRING2021_SCHEMA);
        sqlScriptInitializer.initialize();

        Initializer inputFilesInitializer = applicationContext.getBean(Spring2021InputFilesDatabaseInitializer.class);
        inputFilesInitializer.initialize();

        Initializer outputFilesInitializer = applicationContext.getBean(Spring2021OutputFilesDatabaseInitializer.class);
        outputFilesInitializer.initialize();

        return new ResponseEntity<>("Данные сервиса Spring2021 успешно загружены!", OK);
    }
}
