package com.gvozdev.datainitializerservice.initializer.sql;

import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

@Slf4j
public record SqlScriptInitializer(HikariDataSource dataSource, String resourcePath) implements Initializer {

    @Override
    public void initialize() {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(
            false,
            false,
            "UTF-8",
            new ClassPathResource(resourcePath)
        );

        resourceDatabasePopulator.execute(dataSource);

        log.info("SQL скрипты по пути {} успешно выполнены", resourcePath);
    }
}