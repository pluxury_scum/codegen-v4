package com.gvozdev.datainitializerservice.entity.autumn2020;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "autumn_2020_output_file")
public class Autumn2020OutputFile implements Serializable {

    @Serial
    private static final long serialVersionUID = 991952050411729827L;

    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "var", nullable = false)
    private String var;

    @Column(name = "lang", nullable = false)
    private String lang;

    @Column(name = "file", nullable = false)
    private String file;

    @Column(name = "oop", nullable = false)
    private String oop;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "file_bytes", nullable = false)
    private byte[] fileBytes;

    @Column(name = "creation_date")
    private Date creationDate;

    public Autumn2020OutputFile() {
    }

    public Autumn2020OutputFile(long id, String var, String lang, String file, String oop, String name, byte[] fileBytes, Date creationDate) {
        this.id = id;
        this.var = var;
        this.lang = lang;
        this.file = file;
        this.oop = oop;
        this.name = name;
        this.fileBytes = fileBytes;
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getOop() {
        return oop;
    }

    public void setOop(String oop) {
        this.oop = oop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
