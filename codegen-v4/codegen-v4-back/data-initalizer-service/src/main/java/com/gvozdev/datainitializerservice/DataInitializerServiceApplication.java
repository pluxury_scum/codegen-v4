package com.gvozdev.datainitializerservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@EnableEurekaClient
@SpringBootApplication
public class DataInitializerServiceApplication {
    public static void main(String[] args) {
        run(DataInitializerServiceApplication.class, args);
    }
}
