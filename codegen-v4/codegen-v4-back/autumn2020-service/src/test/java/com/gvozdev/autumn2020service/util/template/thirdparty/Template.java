package com.gvozdev.autumn2020service.util.template.thirdparty;

public class Template {
    public static final String ACTUATOR = """
        /api/year/2020/autumn/actuator""";

    public static final String SWAGGER_UI = """
        /api/year/2020/autumn/swagger-ui""";

    private Template() {
    }
}
