package com.gvozdev.autumn2020service.service.resolver.var3;

import com.gvozdev.autumn2020service.domain.var3.Components;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.CPP;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.CPP_MAIN;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ResolverTests {
    private static final String LATITUDE = "55.0";
    private static final String LONGITUDE = "162";
    private static final String SATELLITE_NUMBER = "7";

    private final Resolver resolver;

    @Autowired
    ResolverTests(Resolver resolver) {
        this.resolver = resolver;
    }

    @Test
    void shouldCheckExerciseInfoDataCanBeResolved() {
        Components exerciseInfo = resolver.getExerciseInfo(LATITUDE, LONGITUDE, SATELLITE_NUMBER);

        assertNotNull(exerciseInfo);
    }

    @Test
    void shouldCheckDownloadListingDataCanBeResolved() {
        byte[] listing = resolver.getListing(
            CPP, FILE, WITHOOP, CPP_MAIN,
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        assertNotNull(listing);
    }

    @Test
    void shouldCheckDownloadChartsDataCanBeResolved() {
        byte[] charts = resolver.getCharts(LATITUDE, LONGITUDE, SATELLITE_NUMBER);

        assertNotNull(charts);
    }
}
