package com.gvozdev.autumn2020service.util.converter;

import com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.autumn2020service.util.queryparam.inputfilename.UrlInputFileNameQueryParamConverter;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlInputFileNameQueryParamConverterTests {
    private final UrlInputFileNameQueryParamConverter converter = new UrlInputFileNameQueryParamConverter();

    @Test
    void shouldCheckBogi6ValueConversion() {
        InputFileName bogi6 = converter.convert("BOGI_1-6.dat");

        assertEquals(BOGI_6, bogi6);
    }

    @Test
    void shouldCheckBrdcValueConversion() {
        InputFileName brdc = converter.convert("brdc0010.18n");

        assertEquals(BRDC, brdc);
    }

    @Test
    void shouldCheckBshm10ValueConversion() {
        InputFileName bshm10 = converter.convert("bshm_1-10.dat");

        assertEquals(BSHM_10, bshm10);
    }

    @Test
    void shouldCheckHueg6ValueConversion() {
        InputFileName hueg6 = converter.convert("HUEG_1-6.dat");

        assertEquals(HUEG_6, hueg6);
    }

    @Test
    void shouldCheckHueg10ValueConversion() {
        InputFileName hueg10 = converter.convert("hueg_1-10.dat");

        assertEquals(HUEG_10, hueg10);
    }

    @Test
    void shouldCheckIgrgValueConversion() {
        InputFileName igrg = converter.convert("igrg0010.18i");

        assertEquals(IGRG, igrg);
    }

    @Test
    void shouldCheckIgsgValueConversion() {
        InputFileName igsg = converter.convert("igsg0010.18i");

        assertEquals(IGSG, igsg);
    }

    @Test
    void shouldCheckLeij10ValueConversion() {
        InputFileName leij10 = converter.convert("leij_1-10.dat");

        assertEquals(LEIJ_10, leij10);
    }

    @Test
    void shouldCheckOnsa6ValueConversion() {
        InputFileName onsa6 = converter.convert("ONSA_1-6.dat");

        assertEquals(ONSA_6, onsa6);
    }

    @Test
    void shouldCheckOnsa10ValueConversion() {
        InputFileName onsa10 = converter.convert("onsa_1-10.dat");

        assertEquals(ONSA_10, onsa10);
    }

    @Test
    void shouldCheckPots6ValueConversion() {
        InputFileName pots6 = converter.convert("POTS_6hours.dat");

        assertEquals(POTS_6, pots6);
    }

    @Test
    void shouldCheckSpt10ValueConversion() {
        InputFileName spt10 = converter.convert("spt0_1-10.dat");

        assertEquals(SPT_10, spt10);
    }

    @Test
    void shouldCheckSvtl10ValueConversion() {
        InputFileName svtl10 = converter.convert("svtl_1-10.dat");

        assertEquals(SVTL_10, svtl10);
    }

    @Test
    void shouldCheckTit10ValueConversion() {
        InputFileName tit10 = converter.convert("tit2_1-10.dat");

        assertEquals(TIT_10, tit10);
    }

    @Test
    void shouldCheckTit6ValueConversion() {
        InputFileName tit6 = converter.convert("TITZ_6hours.dat");

        assertEquals(TIT_6, tit6);
    }

    @Test
    void shouldCheckVis10ValueConversion() {
        InputFileName vis10 = converter.convert("vis0_1-10.dat");

        assertEquals(VIS_10, vis10);
    }

    @Test
    void shouldCheckWarn10ValueConversion() {
        InputFileName warn10 = converter.convert("warn_1-10.dat");

        assertEquals(WARN_10, warn10);
    }

    @Test
    void shouldCheckWarn6ValueConversion() {
        InputFileName warn6 = converter.convert("WARN_6hours.dat");

        assertEquals(WARN_6, warn6);
    }

    @Test
    void shouldCheckZeck10ValueConversion() {
        InputFileName zeck10 = converter.convert("zeck_1-10.dat");

        assertEquals(ZECK_10, zeck10);
    }

    @Test
    void shouldCheckResourcesValueConversion() {
        InputFileName resources = converter.convert("resources.rar");

        assertEquals(RESOURCES, resources);
    }

    @Test
    void shouldCheckReferenceValueConversion() {
        InputFileName reference = converter.convert("reference.pdf");

        assertEquals(REFERENCE, reference);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            NoSuchElementException.class,
            () -> converter.convert("wrong_input_file_value")
        );
    }
}
