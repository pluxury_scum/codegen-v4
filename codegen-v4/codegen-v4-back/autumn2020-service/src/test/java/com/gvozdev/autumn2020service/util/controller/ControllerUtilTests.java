package com.gvozdev.autumn2020service.util.controller;

import com.gvozdev.autumn2020service.util.var3.controller.ControllerUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ControllerUtilTests {
    private final ControllerUtil controllerUtil;

    @Autowired
    ControllerUtilTests(ControllerUtil controllerUtil) {
        this.controllerUtil = controllerUtil;
    }

    @Test
    void shouldCheckAreInputParametersInvalidMethod() {
        boolean isLatitudeWithoutDotInvalid = controllerUtil.areInputParametersInvalid("50", "162", "7");
        boolean isLatitudeWithUnparsableNumberInvalid = controllerUtil.areInputParametersInvalid("latitude", "162", "7");
        boolean isLatitudeWithFractionInvalid = controllerUtil.areInputParametersInvalid("22.5", "162", "7");
        boolean isLatitudeWithoutFractionInvalid = controllerUtil.areInputParametersInvalid("50.0", "162", "7");
        boolean isLongitudeWithDotInvalid = controllerUtil.areInputParametersInvalid("50.0", "55.0", "7");
        boolean isLongitudeWithUnparsableNumberInvalid = controllerUtil.areInputParametersInvalid("50.0", "longitude", "7");
        boolean isLongitudeWithoutDotInvalid = controllerUtil.areInputParametersInvalid("50.0", "55", "7");
        boolean isUnparsableSatelliteNumberInvalid = controllerUtil.areInputParametersInvalid("50.0", "55", "satelliteNumber");
        boolean isSatelliteNumber41Invalid = controllerUtil.areInputParametersInvalid("50.0", "55", "41");
        boolean isSatelliteNumber0Invalid = controllerUtil.areInputParametersInvalid("50.0", "55", "0");
        boolean isSatelliteNumberMinus5Invalid = controllerUtil.areInputParametersInvalid("50.0", "55", "-5");
        boolean isSatelliteNumber7Invalid = controllerUtil.areInputParametersInvalid("50.0", "55", "7");
        boolean isSatelliteNumber15Invalid = controllerUtil.areInputParametersInvalid("50.0", "55", "15");

        assertTrue(isLatitudeWithoutDotInvalid);
        assertTrue(isLatitudeWithUnparsableNumberInvalid);
        assertFalse(isLatitudeWithFractionInvalid);
        assertFalse(isLatitudeWithoutFractionInvalid);
        assertTrue(isLongitudeWithDotInvalid);
        assertTrue(isLongitudeWithUnparsableNumberInvalid);
        assertFalse(isLongitudeWithoutDotInvalid);
        assertTrue(isUnparsableSatelliteNumberInvalid);
        assertTrue(isSatelliteNumber41Invalid);
        assertTrue(isSatelliteNumber0Invalid);
        assertTrue(isSatelliteNumberMinus5Invalid);
        assertFalse(isSatelliteNumber7Invalid);
        assertFalse(isSatelliteNumber15Invalid);
    }

    @Test
    void shouldCheckGetSmallerLatitudeForInterpolationMethod() {
        String smallerLatitudeFrom3 = controllerUtil.getSmallerLatitudeForInterpolation("3");
        String smallerLatitudeFrom61 = controllerUtil.getSmallerLatitudeForInterpolation("61");
        String smallerLatitudeFrom103 = controllerUtil.getSmallerLatitudeForInterpolation("103");
        String smallerLatitudeFromMinus7 = controllerUtil.getSmallerLatitudeForInterpolation("-7");
        String smallerLatitudeFromMinus17 = controllerUtil.getSmallerLatitudeForInterpolation("-17");
        String smallerLatitudeFromMinus128 = controllerUtil.getSmallerLatitudeForInterpolation("-128");

        assertEquals("2.5", smallerLatitudeFrom3);
        assertEquals("60.0", smallerLatitudeFrom61);
        assertEquals("102.5", smallerLatitudeFrom103);
        assertEquals("-7.5", smallerLatitudeFromMinus7);
        assertEquals("-17.5", smallerLatitudeFromMinus17);
        assertEquals("-130.0", smallerLatitudeFromMinus128);
    }

    @Test
    void shouldCheckGetBiggerLatitudeForInterpolationMethod() {
        String biggerLatitudeFrom3 = controllerUtil.getBiggerLatitudeForInterpolation("3");
        String biggerLatitudeFrom61 = controllerUtil.getBiggerLatitudeForInterpolation("61");
        String biggerLatitudeFrom103 = controllerUtil.getBiggerLatitudeForInterpolation("103");
        String biggerLatitudeFromMinus7 = controllerUtil.getBiggerLatitudeForInterpolation("-7");
        String biggerLatitudeFromMinus17 = controllerUtil.getBiggerLatitudeForInterpolation("-17");
        String biggerLatitudeFromMinus128 = controllerUtil.getBiggerLatitudeForInterpolation("-128");

        assertEquals("5.0", biggerLatitudeFrom3);
        assertEquals("62.5", biggerLatitudeFrom61);
        assertEquals("105.0", biggerLatitudeFrom103);
        assertEquals("-5.0", biggerLatitudeFromMinus7);
        assertEquals("-15.0", biggerLatitudeFromMinus17);
        assertEquals("-127.5", biggerLatitudeFromMinus128);
    }

    @Test
    void shouldCheckGetSmallerLongitudeForInterpolationMethod() {
        String smallerLongitudeFrom3 = controllerUtil.getSmallerLongitudeForInterpolation("3");
        String smallerLongitudeFrom61 = controllerUtil.getSmallerLongitudeForInterpolation("61");
        String smallerLongitudeFrom103 = controllerUtil.getSmallerLongitudeForInterpolation("103");
        String smallerLongitudeFromMinus7 = controllerUtil.getSmallerLongitudeForInterpolation("-7");
        String smallerLongitudeFromMinus17 = controllerUtil.getSmallerLongitudeForInterpolation("-17");
        String smallerLongitudeFromMinus128 = controllerUtil.getSmallerLongitudeForInterpolation("-128");

        assertEquals("0", smallerLongitudeFrom3);
        assertEquals("60", smallerLongitudeFrom61);
        assertEquals("100", smallerLongitudeFrom103);
        assertEquals("-10", smallerLongitudeFromMinus7);
        assertEquals("-20", smallerLongitudeFromMinus17);
        assertEquals("-130", smallerLongitudeFromMinus128);
    }

    @Test
    void shouldCheckGetBiggerLongitudeForInterpolationMethod() {
        String biggerLongitudeFrom3 = controllerUtil.getBiggerLongitudeForInterpolation("3");
        String biggerLongitudeFrom61 = controllerUtil.getBiggerLongitudeForInterpolation("61");
        String biggerLongitudeFrom103 = controllerUtil.getBiggerLongitudeForInterpolation("103");
        String biggerLongitudeFromMinus7 = controllerUtil.getBiggerLongitudeForInterpolation("-7");
        String biggerLongitudeFromMinus17 = controllerUtil.getBiggerLongitudeForInterpolation("-17");
        String biggerLongitudeFromMinus128 = controllerUtil.getBiggerLongitudeForInterpolation("-128");

        assertEquals("5", biggerLongitudeFrom3);
        assertEquals("65", biggerLongitudeFrom61);
        assertEquals("105", biggerLongitudeFrom103);
        assertEquals("-5", biggerLongitudeFromMinus7);
        assertEquals("-15", biggerLongitudeFromMinus17);
        assertEquals("-125", biggerLongitudeFromMinus128);
    }
}
