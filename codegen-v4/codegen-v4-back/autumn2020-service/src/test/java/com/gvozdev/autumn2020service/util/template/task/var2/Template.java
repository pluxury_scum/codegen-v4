package com.gvozdev.autumn2020service.util.template.task.var2;

public class Template {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2020/autumn/var2/exercise-info/{0}""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2020/autumn/var2/download/listing/{0}/{1}/{2}/{3}/{4}""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2020/autumn/var2/download/charts/{0}""";

    private Template() {
    }
}
