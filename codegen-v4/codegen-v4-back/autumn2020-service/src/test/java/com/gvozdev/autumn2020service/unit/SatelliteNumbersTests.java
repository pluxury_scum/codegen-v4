package com.gvozdev.autumn2020service.unit;

import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.service.jpa.impl.InputFileServiceImpl;
import com.gvozdev.autumn2020service.util.index.ComponentIndexes;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReader;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.autumn2020service.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.BSHM_10;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SatelliteNumbersTests {
    private final InputFileServiceImpl inputFileService;
    private final ComponentIndexes componentIndexes;
    private final FileReaderUtil fileReaderUtil;

    @Autowired
    SatelliteNumbersTests(
        InputFileServiceImpl inputFileService, ComponentIndexes componentIndexes, FileReaderUtil fileReaderUtil
    ) {
        this.inputFileService = inputFileService;
        this.componentIndexes = componentIndexes;
        this.fileReaderUtil = fileReaderUtil;
    }

    @Test
    void shouldCheckSatelliteNumbers() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);

        List<Integer> expectedSatelliteNumbers = asList(2, 5, 6, 12, 15, 16, 18, 20, 21, 24, 25, 26, 29, 31);
        List<Integer> actualSatelliteNumbers = fileReader.getSatelliteNumbers();

        assertEquals(expectedSatelliteNumbers, actualSatelliteNumbers);
    }
}