package com.gvozdev.autumn2020service.unit;

import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.service.jpa.impl.InputFileServiceImpl;
import com.gvozdev.autumn2020service.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.autumn2020service.util.var3.filereader.EphemerisFileReaderUtil;
import com.gvozdev.autumn2020service.util.var3.filereader.IonoFileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.autumn2020service.util.constant.Constants.START_OF_OBSERVATIONS_LINE_NUMBER;
import static com.gvozdev.autumn2020service.util.constant.Constants.VAR3_AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.BRDC;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class IonCoefficientsTests {
    private final InputFileServiceImpl inputFileService;
    private final EphemerisFileReaderUtil ephemerisFileReaderUtil;
    private final IonoFileReaderUtil ionoFileReaderUtil;

    @Autowired
    IonCoefficientsTests(
        InputFileServiceImpl inputFileService, EphemerisFileReaderUtil ephemerisFileReaderUtil, IonoFileReaderUtil ionoFileReaderUtil
    ) {
        this.inputFileService = inputFileService;
        this.ephemerisFileReaderUtil = ephemerisFileReaderUtil;
        this.ionoFileReaderUtil = ionoFileReaderUtil;
    }

    @Test
    void shouldCheckAlphaCoefficients() {
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            fileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        List<Double> expectedAlphaCoefficients = asList(7.451E-9, -1.49E-8, -5.96E-8, 1.192E-7);
        List<Double> actualAlphaCoefficients = ephemerisFileReader.getAlphaCoefficients();

        assertEquals(expectedAlphaCoefficients, actualAlphaCoefficients);
    }

    @Test
    void shouldCheckBetaCoefficients() {
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            fileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        List<Double> expectedBetaCoefficients = asList(92160.0, -114700.0, -131100.0, 720900.0);
        List<Double> actualBetaCoefficients = ephemerisFileReader.getBetaCoefficients();

        assertEquals(expectedBetaCoefficients, actualBetaCoefficients);
    }
}
