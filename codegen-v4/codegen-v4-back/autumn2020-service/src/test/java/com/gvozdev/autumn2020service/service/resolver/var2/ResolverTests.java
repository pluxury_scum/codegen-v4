package com.gvozdev.autumn2020service.service.resolver.var2;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.POTS_6;
import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.CPP;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.CPP_MAIN;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ResolverTests {
    private final Resolver resolver;

    @Autowired
    ResolverTests(Resolver resolver) {
        this.resolver = resolver;
    }

    @Test
    void shouldCheckExerciseInfoDataCanBeResolved() {
        ExerciseInfoWrapper exerciseInfo = resolver.getExerciseInfo(POTS_6);

        assertNotNull(exerciseInfo);
    }

    @Test
    void shouldCheckDownloadListingDataCanBeResolved() {
        byte[] listing = resolver.getListing(CPP, FILE, WITHOOP, POTS_6, CPP_MAIN);

        assertNotNull(listing);
    }

    @Test
    void shouldCheckDownloadChartsDataCanBeResolved() {
        byte[] charts = resolver.getCharts(POTS_6);

        assertNotNull(charts);
    }
}
