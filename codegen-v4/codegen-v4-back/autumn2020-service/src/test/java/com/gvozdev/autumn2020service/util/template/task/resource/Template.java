package com.gvozdev.autumn2020service.util.template.task.resource;

public class Template {
    public static final String DOWNLOAD_RESOURCES = """
        /api/tasks/year/2020/autumn/download/resources""";

    public static final String DOWNLOAD_REFERENCE = """
        /api/tasks/year/2020/autumn/download/reference""";

    private Template() {
    }
}
