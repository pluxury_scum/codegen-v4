package com.gvozdev.autumn2020service.file.output.var1.java.file.withoop;

import com.gvozdev.autumn2020service.entity.OutputFile;
import com.gvozdev.autumn2020service.service.jpa.api.OutputFileService;
import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.oop.Oop;
import com.gvozdev.autumn2020service.util.queryparam.var.Var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.JAVA;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.JAVA_GRAPH_DRAWER;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.JAVA_MAIN;
import static com.gvozdev.autumn2020service.util.queryparam.var.Var.VAR1;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FilesExistenceTests {
    private static final Var VAR = VAR1;
    private static final Lang LANG = JAVA;
    private static final File FILE = File.FILE;
    private static final Oop OOP = WITHOOP;

    private final OutputFileService outputFileService;

    @Autowired
    FilesExistenceTests(OutputFileService outputFileService) {
        this.outputFileService = outputFileService;
    }

    @Test
    void shouldCheckMainExists() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(),
            JAVA_MAIN.getFileName()
        );

        assertNotNull(outputFile);
    }

    @Test
    void shouldCheckGraphDrawerExists() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(),
            JAVA_GRAPH_DRAWER.getFileName()
        );

        assertNotNull(outputFile);
    }
}