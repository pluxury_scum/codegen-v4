package com.gvozdev.autumn2020service.service.resolver.resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ResolverTests {
    private final Resolver resolver;

    @Autowired
    ResolverTests(Resolver resolver) {
        this.resolver = resolver;
    }

    @Test
    void shouldCheckResourcesDataCanBeResolved() {
        byte[] resources = resolver.getResources();

        assertNotNull(resources);
    }

    @Test
    void shouldCheckReferenceDataCanBeResolved() {
        byte[] reference = resolver.getReference();

        assertNotNull(reference);
    }
}
