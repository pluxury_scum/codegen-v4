package com.gvozdev.autumn2020service.controller.task.var3;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.MANUAL;
import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.*;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.*;
import static com.gvozdev.autumn2020service.util.template.task.var3.Template.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.text.MessageFormat.format;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PageControllerTests {
    private final MockMvc mockMvc;

    @Autowired
    PageControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void shouldCheckExerciseInfo() throws Exception {
        mockMvc.perform(
                get(EXERCISE_INFO)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
                    .characterEncoding(UTF_8)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$.interpolationCoordinates", hasKey("smallerLatitude")))
            .andExpect(jsonPath("$.interpolationCoordinates", hasKey("biggerLatitude")))
            .andExpect(jsonPath("$.interpolationCoordinates", hasKey("smallerLongitude")))
            .andExpect(jsonPath("$.interpolationCoordinates", hasKey("biggerLongitude")))
            .andExpect(jsonPath("$.gpsTime.gpsTime", hasSize(12)))
            .andExpect(jsonPath("$.alpha.ionCoefficients", hasSize(4)))
            .andExpect(jsonPath("$.beta.ionCoefficients", hasSize(4)))
            .andExpect(jsonPath("$.forecastValues", hasSize(4)))
            .andExpect(jsonPath("$.preciseValues", hasSize(4)));
    }

    @Test
    void shouldCheckExerciseInfoWrongLatitudeValue() throws Exception {
        mockMvc.perform(
            get(EXERCISE_INFO)
                .param("lat", "wrong_latitude_value")
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckExerciseInfoWrongLongitudeValue() throws Exception {
        mockMvc.perform(
            get(EXERCISE_INFO)
                .param("lat", LATITUDE)
                .param("lon", "wrong_longitude_value")
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckExerciseInfoWrongSatelliteNumberValue() throws Exception {
        mockMvc.perform(
            get(EXERCISE_INFO)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", "wrong_satellite_number")
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, WITHOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, NOOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, WITHOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, NOOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, NOOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, "wrong_file_value", WITHOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, "wrong_file_value", NOOOP, CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppFile() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, "wrong_oop_value", CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppManual() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, "wrong_oop_value", CPP_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, NOOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, NOOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, "wrong_file_value", WITHOOP, JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, "wrong_file_value", WITHOOP, JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, "wrong_oop_value", JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, "wrong_oop_value", JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, "wrong_oop_value", JAVA_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, "wrong_oop_value", JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingPythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, NOOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, NOOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
                get(url)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, NOOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, "wrong_file_value", WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, "wrong_file_value", NOOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonFile() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, "wrong_oop_value", PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonManual() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, "wrong_oop_value", PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, NOOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, WITHOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, NOOOP, "wrong_output_file_name"
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLatitudeValue() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", "wrong_latitude_value")
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLongitudeValue() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", "wrong_longitude_value")
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongSatelliteNumberValue() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(
            get(url)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", "wrong_satellite_number_value")
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadCharts() throws Exception {
        mockMvc.perform(
                get(DOWNLOAD_CHARTS)
                    .param("lat", LATITUDE)
                    .param("lon", LONGITUDE)
                    .param("satnum", SATELLITE_NUMBER)
            ).andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    void shouldCheckDownloadChartsWrongLatitudeValue() throws Exception {
        mockMvc.perform(
            get(DOWNLOAD_CHARTS)
                .param("lat", "wrong_latitude_value")
                .param("lon", LONGITUDE)
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadChartsWrongLongitudeValue() throws Exception {
        mockMvc.perform(
            get(DOWNLOAD_CHARTS)
                .param("lat", LATITUDE)
                .param("lon", "wrong_longitude_value")
                .param("satnum", SATELLITE_NUMBER)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadChartsWrongSatelliteNumberValue() throws Exception {
        mockMvc.perform(
            get(DOWNLOAD_CHARTS)
                .param("lat", LATITUDE)
                .param("lon", LONGITUDE)
                .param("satnum", "wrong_satellite_number")
        ).andExpect(status().isBadRequest());
    }
}
