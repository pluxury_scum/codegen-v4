package com.gvozdev.autumn2020service.controller.task.resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.autumn2020service.util.template.task.resource.Template.DOWNLOAD_REFERENCE;
import static com.gvozdev.autumn2020service.util.template.task.resource.Template.DOWNLOAD_RESOURCES;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PageControllerTests {
    private final MockMvc mockMvc;

    @Autowired
    PageControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void shouldCheckDownloadResourcesController() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    void shouldCheckDownloadReferenceController() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }
}
