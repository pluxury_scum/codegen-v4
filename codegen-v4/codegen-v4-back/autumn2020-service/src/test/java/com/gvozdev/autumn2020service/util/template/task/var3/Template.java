package com.gvozdev.autumn2020service.util.template.task.var3;

public class Template {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2020/autumn/var3/exercise-info""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2020/autumn/var3/download/listing/{0}/{1}/{2}/{3}""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2020/autumn/var3/download/charts""";

    public static final String LATITUDE = "55.0";

    public static final String LONGITUDE = "162";

    public static final String SATELLITE_NUMBER = "7";

    private Template() {
    }
}
