package com.gvozdev.autumn2020service.util.common;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CommonUtilTests {
    private final CommonUtil commonUtil;

    @Autowired
    CommonUtilTests(CommonUtil commonUtil) {
        this.commonUtil = commonUtil;
    }

    @Test
    void shouldCheckToCharMethod() {
        char result = commonUtil.toChar(58);

        assertEquals('j', result);
    }

    @Test
    void shouldCheckIsDoubleDigitMethod() {
        boolean is4DoubleDigit = commonUtil.isDoubleDigit(4);
        boolean is56DoubleDigit = commonUtil.isDoubleDigit(56);
        boolean is789DoubleDigit = commonUtil.isDoubleDigit(789);

        assertFalse(is4DoubleDigit);
        assertTrue(is56DoubleDigit);
        assertFalse(is789DoubleDigit);
    }

    @Test
    void shouldCheckGetFirstDigitMethod() {
        int firstDigitOf1 = commonUtil.getFirstDigit(1);
        int firstDigitOf23 = commonUtil.getFirstDigit(23);
        int firstDigitOf456 = commonUtil.getFirstDigit(456);

        assertEquals(1, firstDigitOf1);
        assertEquals(2, firstDigitOf23);
        assertEquals(4, firstDigitOf456);
    }

    @Test
    void shouldCheckGetSecondDigitMethod() {
        int secondDigitOf34 = commonUtil.getSecondDigit(34);
        int secondDigitOf567 = commonUtil.getSecondDigit(567);

        assertEquals(4, secondDigitOf34);
        assertEquals(6, secondDigitOf567);
        assertThrows(StringIndexOutOfBoundsException.class, () -> commonUtil.getSecondDigit(2));
    }

    @Test
    void shouldCheckRoundUpToNumberMethod() {
        double number1RoundedUp = commonUtil.roundUpToNumber(1, 2.5);
        double number23Point4RoundedUp = commonUtil.roundUpToNumber(23.4, 5.0);
        double number567Point89RoundedUp = commonUtil.roundUpToNumber(567.89, 10.0);

        assertEquals(2.5, number1RoundedUp);
        assertEquals(25.0, number23Point4RoundedUp);
        assertEquals(570.0, number567Point89RoundedUp);
    }

    @Test
    void shouldCheckRoundDownToNumberMethod() {
        double number1RoundedDown = commonUtil.roundDownToNumber(1, 2.5);
        double number23Point4RoundedDown = commonUtil.roundDownToNumber(23.4, 5.0);
        double number567Point89RoundedDown = commonUtil.roundDownToNumber(567.89, 10.0);

        assertEquals(0.0, number1RoundedDown);
        assertEquals(20.0, number23Point4RoundedDown);
        assertEquals(560.0, number567Point89RoundedDown);
    }

    @Test
    void shouldCheckIsEvenMethod() {
        boolean is2Even = commonUtil.isEven(2);
        boolean is51Even = commonUtil.isEven(35);
        boolean is678Even = commonUtil.isEven(678);

        assertTrue(is2Even);
        assertFalse(is51Even);
        assertTrue(is678Even);
    }
}
