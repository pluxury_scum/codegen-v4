package com.gvozdev.autumn2020service.util.converter;

import com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.UrlOutputFileNameQueryParamConverter;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlOutputFileNameQueryParamConverterTests {
    private final UrlOutputFileNameQueryParamConverter converter = new UrlOutputFileNameQueryParamConverter();

    @Test
    void shouldCheckCppMainValueConversion() {
        OutputFileName cppMain = converter.convert("main.cpp");

        assertEquals(CPP_MAIN, cppMain);
    }

    @Test
    void shouldCheckJavaMainValueConversion() {
        OutputFileName javaMain = converter.convert("Main.java");

        assertEquals(JAVA_MAIN, javaMain);
    }

    @Test
    void shouldCheckJavaGraphDrawerValueConversion() {
        OutputFileName javaGraphDrawer = converter.convert("GraphDrawer.java");

        assertEquals(JAVA_GRAPH_DRAWER, javaGraphDrawer);
    }

    @Test
    void shouldCheckPythonMainValueConversion() {
        OutputFileName pythonMain = converter.convert("main.py");

        assertEquals(PYTHON_MAIN, pythonMain);
    }

    @Test
    void shouldCheckChartsValueConversion() {
        OutputFileName charts = converter.convert("charts.rar");

        assertEquals(CHARTS, charts);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            NoSuchElementException.class,
            () -> converter.convert("wrong_output_file_value")
        );
    }
}
