package com.gvozdev.autumn2020service.file.input;

import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.service.jpa.impl.InputFileServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FilesExistenceTests {
    private final InputFileServiceImpl inputFileService;

    @Autowired
    FilesExistenceTests(InputFileServiceImpl inputFileService) {
        this.inputFileService = inputFileService;
    }

    @Test
    void shouldCheckBogi6Exists() {
        InputFile inputFile = inputFileService.findByName(BOGI_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckBrdc0010Exists() {
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckBshm10Exists() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckHueg6Exists() {
        InputFile inputFile = inputFileService.findByName(HUEG_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckHueg10Exists() {
        InputFile inputFile = inputFileService.findByName(HUEG_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckIgrg0010Exists() {
        InputFile inputFile = inputFileService.findByName(IGRG.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckIgsg0010Exists() {
        InputFile inputFile = inputFileService.findByName(IGSG.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckLeij10Exists() {
        InputFile inputFile = inputFileService.findByName(LEIJ_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckOnsa6Exists() {
        InputFile inputFile = inputFileService.findByName(ONSA_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckOnsa10Exists() {
        InputFile inputFile = inputFileService.findByName(ONSA_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckPots6Exists() {
        InputFile inputFile = inputFileService.findByName(POTS_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckSpt10Exists() {
        InputFile inputFile = inputFileService.findByName(SPT_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckSvtl10Exists() {
        InputFile inputFile = inputFileService.findByName(SVTL_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckTit10Exists() {
        InputFile inputFile = inputFileService.findByName(TIT_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckTit6Exists() {
        InputFile inputFile = inputFileService.findByName(TIT_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckVis10Exists() {
        InputFile inputFile = inputFileService.findByName(VIS_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckWarn6Exists() {
        InputFile inputFile = inputFileService.findByName(WARN_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckWarn10Exists() {
        InputFile inputFile = inputFileService.findByName(WARN_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckZeck10Exists() {
        InputFile inputFile = inputFileService.findByName(ZECK_10.getFileName());

        assertNotNull(inputFile);
    }
}
