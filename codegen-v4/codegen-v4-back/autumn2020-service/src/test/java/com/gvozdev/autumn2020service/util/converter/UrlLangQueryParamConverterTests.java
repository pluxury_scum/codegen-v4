package com.gvozdev.autumn2020service.util.converter;

import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.lang.UrlLangQueryParamConverter;
import org.junit.jupiter.api.Test;

import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlLangQueryParamConverterTests {
    private final UrlLangQueryParamConverter converter = new UrlLangQueryParamConverter();

    @Test
    void shouldCheckCppValueConversion() {
        Lang cpp = converter.convert("cpp");

        assertEquals(CPP, cpp);
    }

    @Test
    void shouldCheckJavaValueConversion() {
        Lang java = converter.convert("java");

        assertEquals(JAVA, java);
    }

    @Test
    void shouldCheckPythonValueConversion() {
        Lang python = converter.convert("python");

        assertEquals(PYTHON, python);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> converter.convert("wrong_lang_value")
        );
    }
}
