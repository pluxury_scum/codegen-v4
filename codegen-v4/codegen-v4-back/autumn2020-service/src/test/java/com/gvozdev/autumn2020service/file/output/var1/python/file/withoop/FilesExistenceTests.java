package com.gvozdev.autumn2020service.file.output.var1.python.file.withoop;

import com.gvozdev.autumn2020service.entity.OutputFile;
import com.gvozdev.autumn2020service.service.jpa.api.OutputFileService;
import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.oop.Oop;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.autumn2020service.util.queryparam.var.Var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.PYTHON;
import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.PYTHON_MAIN;
import static com.gvozdev.autumn2020service.util.queryparam.var.Var.VAR1;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FilesExistenceTests {
    private static final Var VAR = VAR1;
    private static final Lang LANG = PYTHON;
    private static final File FILE = File.FILE;
    private static final Oop OOP = WITHOOP;
    private static final OutputFileName OUTPUT_FILE_NAME = PYTHON_MAIN;

    private final OutputFileService outputFileService;

    @Autowired
    FilesExistenceTests(OutputFileService outputFileService) {
        this.outputFileService = outputFileService;
    }

    @Test
    void shouldCheckMainExists() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(),
            OUTPUT_FILE_NAME.getFileName()
        );

        assertNotNull(outputFile);
    }
}