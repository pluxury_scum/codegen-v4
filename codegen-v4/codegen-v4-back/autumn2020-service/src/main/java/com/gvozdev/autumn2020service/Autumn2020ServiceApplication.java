package com.gvozdev.autumn2020service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@EnableEurekaClient
@SpringBootApplication
public class Autumn2020ServiceApplication {
    public static void main(String[] args) {
        run(Autumn2020ServiceApplication.class, args);
    }
}
