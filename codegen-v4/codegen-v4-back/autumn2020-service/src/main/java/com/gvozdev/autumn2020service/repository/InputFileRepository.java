package com.gvozdev.autumn2020service.repository;

import com.gvozdev.autumn2020service.entity.InputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputFileRepository extends JpaRepository<InputFile, Long> {
    InputFile findByName(String name);
}
