package com.gvozdev.autumn2020service.util.constant;

public class Constants {
	public static final int AMOUNT_OF_OBSERVATIONS = 360;

	public static final int VAR3_AMOUNT_OF_OBSERVATIONS = 12;

	public static final int START_OF_OBSERVATIONS_LINE_NUMBER = 8;

	public static final int FIRST_LONGITUDE = -180;

	public static final int DELTA_LONGITUDE = 5;

	public static final int TEC_VALUES_PER_LINE = 16;

	public static final int FORECAST_TEC_LIST_FIRST_LINE = 304;

	public static final int PRECISE_TEC_LIST_FIRST_LINE = 394;

	private Constants() {
	}
}