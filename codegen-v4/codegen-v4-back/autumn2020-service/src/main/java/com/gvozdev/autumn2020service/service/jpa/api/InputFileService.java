package com.gvozdev.autumn2020service.service.jpa.api;

import com.gvozdev.autumn2020service.entity.InputFile;

public interface InputFileService {
    InputFile findByName(String name);

    void save(InputFile inputFile);
}
