package com.gvozdev.autumn2020service.service.resolver.var2;

import com.gvozdev.autumn2020service.domain.var2.Satellite;

import java.util.List;

public record ExerciseInfoWrapper(List<Satellite> satellites) {
}
