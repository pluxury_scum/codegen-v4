package com.gvozdev.autumn2020service.util.var2.chart;

import com.gvozdev.autumn2020service.domain.var2.LinearVelocities;
import com.gvozdev.autumn2020service.domain.var2.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static java.awt.Color.WHITE;
import static java.util.stream.IntStream.range;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class LinearVelocitiesChartDrawer extends TemplateChartDrawer {
    public LinearVelocitiesChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    XYSeriesCollection getMeasurementsSeriesCollection() {
        LinearVelocities linearVelocitiesForSatellite1 = new LinearVelocities(amountOfObservations);
        List<Double> satellite1Velocities = linearVelocitiesForSatellite1.getLinearVelocities();

        LinearVelocities linearVelocitiesForSatellite2 = new LinearVelocities(amountOfObservations);
        List<Double> satellite2Velocities = linearVelocitiesForSatellite2.getLinearVelocities();

        LinearVelocities linearVelocitiesForSatellite3 = new LinearVelocities(amountOfObservations);
        List<Double> satellite3Velocities = linearVelocitiesForSatellite3.getLinearVelocities();

        XYSeries satellite1VelocitiesSeries = new XYSeries("Спутник #" + satellite1.number());
        XYSeries satellite2VelocitiesSeries = new XYSeries("Спутник #" + satellite2.number());
        XYSeries satellite3VelocitiesSeries = new XYSeries("Спутник #" + satellite3.number());

        range(0, amountOfObservations).forEach(observation -> {
            double linearVelocityForSatellite1 = satellite1Velocities.get(observation);
            satellite1VelocitiesSeries.add(observation, linearVelocityForSatellite1);

            double linearVelocityForSatellite2 = satellite2Velocities.get(observation);
            satellite2VelocitiesSeries.add(observation, linearVelocityForSatellite2);

            double linearVelocityForSatellite3 = satellite3Velocities.get(observation);
            satellite3VelocitiesSeries.add(observation, linearVelocityForSatellite3);
        });

        XYSeriesCollection linearVelocitiesSeriesCollection = new XYSeriesCollection();
        linearVelocitiesSeriesCollection.addSeries(satellite1VelocitiesSeries);
        linearVelocitiesSeriesCollection.addSeries(satellite2VelocitiesSeries);
        linearVelocitiesSeriesCollection.addSeries(satellite3VelocitiesSeries);

        return linearVelocitiesSeriesCollection;
    }

    @Override
    JFreeChart createChart(XYSeriesCollection observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Линейная скорость, м/с",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    @Override
    void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(LEGEND_FONT);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.05, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }
}
