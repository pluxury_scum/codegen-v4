package com.gvozdev.autumn2020service.domain.var1;

import java.util.List;

public record Satellite(int number, List<Double> p1, List<Double> p2) {
}
