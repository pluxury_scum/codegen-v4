package com.gvozdev.autumn2020service.config;

import com.gvozdev.autumn2020service.util.index.ComponentIndexes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ComponentIndexesConfig {

    @Bean
    public ComponentIndexes componentIndexes() {
        int p1Index = 1;
        int p2Index = 2;
        int elevationIndex = 14;

        return new ComponentIndexes(p1Index, p2Index, elevationIndex);
    }
}
