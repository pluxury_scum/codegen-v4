package com.gvozdev.autumn2020service.domain.var3;

public class AxisGeo {
    private final double xpp;
    private final double ypp;

    public AxisGeo(UserGeo userGeo, IgpGeo igpGeo) {
        double lonpp = userGeo.getLongitude();
        double latpp = userGeo.getLatitude();

        double lon1 = igpGeo.getSmallerLongitude();
        double lon2 = igpGeo.getBiggerLongitude();

        double lat1 = igpGeo.getSmallerLatitude();
        double lat2 = igpGeo.getBiggerLatitude();

        xpp = (lonpp - lon1) / (lon2 - lon1);
        ypp = (latpp - lat1) / (lat2 - lat1);
    }

    public double getXpp() {
        return xpp;
    }

    public double getYpp() {
        return ypp;
    }
}