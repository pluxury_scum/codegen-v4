package com.gvozdev.autumn2020service.controller.task.var3;

import com.gvozdev.autumn2020service.domain.var3.Components;
import com.gvozdev.autumn2020service.service.resolver.var3.Resolver;
import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.oop.Oop;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.autumn2020service.util.var3.controller.ControllerUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName.CHARTS;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("Var3Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var3")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private final Resolver resolver;
    private final ControllerUtil controllerUtil;

    public PageController(Resolver resolver, ControllerUtil controllerUtil) {
        this.resolver = resolver;
        this.controllerUtil = controllerUtil;
    }

    @GetMapping("/exercise-info")
    public ResponseEntity<Components> getExerciseInfo(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        Components components = resolver.getExerciseInfo(latitude, longitude, satelliteNumber);

        return new ResponseEntity<>(components, OK);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{outputFileName}")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") Lang lang,
        @PathVariable("file") File file,
        @PathVariable("oop") Oop oop,
        @PathVariable("outputFileName") OutputFileName outputFileName,
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        byte[] listing = resolver.getListing(lang, file, oop, outputFileName, latitude, longitude, satelliteNumber);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(outputFileName.getFileName()).build());

        return new ResponseEntity<>(listing, responseHeaders, OK);
    }

    @GetMapping("/download/charts")
    public ResponseEntity<byte[]> downloadCharts(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        byte[] charts = resolver.getCharts(latitude, longitude, satelliteNumber);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(CHARTS.getFileName()).build());

        return new ResponseEntity<>(charts, responseHeaders, OK);
    }
}