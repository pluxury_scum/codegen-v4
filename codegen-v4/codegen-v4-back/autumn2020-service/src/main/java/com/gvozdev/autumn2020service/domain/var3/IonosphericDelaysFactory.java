package com.gvozdev.autumn2020service.domain.var3;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public record IonosphericDelaysFactory(WeightMatrix weightMatrix, int amountOfObservations) {
    public List<IonosphericDelay> getIonosphericDelays(List<Tec> tecList) {
        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                int a1Tec = tecList.get(0).tec().get(observation);
                int a2Tec = tecList.get(1).tec().get(observation);
                int a3Tec = tecList.get(2).tec().get(observation);
                int a4Tec = tecList.get(3).tec().get(observation);

                return asList(a1Tec, a2Tec, a3Tec, a4Tec);
            })
            .map(Tec::new)
            .map(tec -> new IonosphericDelay(weightMatrix, tec))
            .collect(toList());
    }
}