package com.gvozdev.autumn2020service.service.jpa.api;

import com.gvozdev.autumn2020service.entity.OutputFile;

public interface OutputFileService {
    OutputFile findByParameters(String var, String lang, String file, String oop, String name);

    void save(OutputFile outputFile);
}
