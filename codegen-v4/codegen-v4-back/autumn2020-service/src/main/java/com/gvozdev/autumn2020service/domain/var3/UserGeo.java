package com.gvozdev.autumn2020service.domain.var3;

import static java.lang.Double.parseDouble;

public class UserGeo {
    private final double latitude;
    private final double longitude;

    public UserGeo(String latitude, String longitude) {
        double halfCircle = 180;

        double latpp = parseDouble(latitude);
        double lonpp = parseDouble(longitude);

        this.latitude = latpp / halfCircle;
        this.longitude = lonpp / halfCircle;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}