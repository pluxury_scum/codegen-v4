package com.gvozdev.autumn2020service.domain.var2;

import java.util.List;

public record Satellite(int number, List<Double> elevation) {
}

