package com.gvozdev.autumn2020service.util.var1.chart;

import com.gvozdev.autumn2020service.domain.var1.IonosphericDelays;
import com.gvozdev.autumn2020service.domain.var1.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import static java.awt.Color.*;
import static java.awt.Font.BOLD;
import static javax.imageio.ImageIO.write;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public record PseudoRangesChartDrawer(
    Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations
) {
    public byte[] getChartInBytes() {
        XYSeriesCollection ionosphericDelaysSeriesCollection = getIonosphericDelaysSeriesCollection();

        JFreeChart ionosphericDelaysChart = createChart(ionosphericDelaysSeriesCollection);

        configureChartLines(ionosphericDelaysChart);
        configureLegend(ionosphericDelaysChart);
        configureAxes(ionosphericDelaysChart);
        configureAxesRanges(ionosphericDelaysChart);

        BufferedImage bufferedImage = ionosphericDelaysChart.createBufferedImage(2000, 1000);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            write(bufferedImage, "png", byteArrayOutputStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream.toByteArray();
    }

    private XYSeriesCollection getIonosphericDelaysSeriesCollection() {
        IonosphericDelays ionosphericDelaysForSatellite1 = new IonosphericDelays(satellite1, amountOfObservations);
        List<Double> satellite1Delays = ionosphericDelaysForSatellite1.getIonosphericDelays();

        IonosphericDelays ionosphericDelaysForSatellite2 = new IonosphericDelays(satellite2, amountOfObservations);
        List<Double> satellite2Delays = ionosphericDelaysForSatellite2.getIonosphericDelays();

        IonosphericDelays ionosphericDelaysForSatellite3 = new IonosphericDelays(satellite3, amountOfObservations);
        List<Double> satellite3Delays = ionosphericDelaysForSatellite3.getIonosphericDelays();

        XYSeries satellite1DelaysSeries = new XYSeries("Спутник #" + satellite1.number());
        XYSeries satellite2DelaysSeries = new XYSeries("Спутник #" + satellite2.number());
        XYSeries satellite3DelaysSeries = new XYSeries("Спутник #" + satellite3.number());

        IntStream.range(0, amountOfObservations).forEach(observation -> {
            double ionosphericDelayForSatellite1 = satellite1Delays.get(observation);
            satellite1DelaysSeries.add(observation, ionosphericDelayForSatellite1);

            double ionosphericDelayForSatellite2 = satellite2Delays.get(observation);
            satellite2DelaysSeries.add(observation, ionosphericDelayForSatellite2);

            double ionosphericDelayForSatellite3 = satellite3Delays.get(observation);
            satellite3DelaysSeries.add(observation, ionosphericDelayForSatellite3);
        });

        XYSeriesCollection ionosphericDelaysSeriesCollection = new XYSeriesCollection();
        ionosphericDelaysSeriesCollection.addSeries(satellite1DelaysSeries);
        ionosphericDelaysSeriesCollection.addSeries(satellite2DelaysSeries);
        ionosphericDelaysSeriesCollection.addSeries(satellite3DelaysSeries);

        return ionosphericDelaysSeriesCollection;
    }

    private static JFreeChart createChart(XYDataset ionosphericDelaysSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Наклонная задержка сигнала, метры",
            ionosphericDelaysSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    private static void configureChartLines(JFreeChart ionosphericDelaysChart) {
        int satellite1Index = 0;
        int satellite2Index = 1;
        int satellite3Index = 2;

        XYPlot plot = ionosphericDelaysChart.getXYPlot();
        XYItemRenderer plotRenderer = plot.getRenderer();

        plotRenderer.setSeriesPaint(satellite1Index, BLUE);
        plotRenderer.setSeriesStroke(satellite1Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite2Index, YELLOW);
        plotRenderer.setSeriesStroke(satellite2Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite3Index, RED);
        plotRenderer.setSeriesStroke(satellite3Index, new BasicStroke(2f));
    }

    private static void configureLegend(JFreeChart ionosphericDelaysChart) {
        Font legendFont = new Font("Serif", BOLD, 25);

        XYPlot plot = ionosphericDelaysChart.getXYPlot();

        LegendTitle legend = ionosphericDelaysChart.getLegend();
        legend.setItemFont(legendFont);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.05, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        ionosphericDelaysChart.removeLegend();
    }

    private static void configureAxes(JFreeChart ionosphericDelaysChart) {
        XYPlot plot = ionosphericDelaysChart.getXYPlot();

        Font axisLabelFont = new Font("Serif", BOLD, 20);
        Font axisTickFont = new Font("Serif", Font.PLAIN, 15);

        plot.getDomainAxis().setLabelFont(axisLabelFont);
        plot.getDomainAxis().setTickLabelFont(axisTickFont);
        plot.getRangeAxis().setLabelFont(axisLabelFont);
        plot.getRangeAxis().setTickLabelFont(axisTickFont);
    }

    private static void configureAxesRanges(JFreeChart ionosphericDelaysChart) {
        XYPlot plot = ionosphericDelaysChart.getXYPlot();

        NumberAxis range = (NumberAxis) plot.getDomainAxis();
        range.setTickUnit(new NumberTickUnit(50));
    }
}
