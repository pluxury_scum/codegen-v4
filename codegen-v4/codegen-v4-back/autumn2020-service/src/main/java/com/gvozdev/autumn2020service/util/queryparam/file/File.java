package com.gvozdev.autumn2020service.util.queryparam.file;

public enum File {
    FILE, MANUAL;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
