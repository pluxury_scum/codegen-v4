package com.gvozdev.autumn2020service.util.queryparam.var;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.autumn2020service.util.queryparam.var.Var.valueOf;

@Component
public class UrlVarQueryParamConverter implements Converter<String, Var> {

    @Override
    public Var convert(String source) {
        return valueOf(source.toUpperCase());
    }
}