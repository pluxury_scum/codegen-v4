package com.gvozdev.autumn2020service.util.queryparam.inputfilename;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.getFileNameFromString;

@Component
public class UrlInputFileNameQueryParamConverter implements Converter<String, InputFileName> {

    @Override
    public InputFileName convert(String source) {
        return getFileNameFromString(source);
    }
}
