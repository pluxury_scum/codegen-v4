package com.gvozdev.autumn2020service.domain.var3;

import static java.lang.Double.parseDouble;

public class IgpGeo {
    private final double smallerLatitude;
    private final double biggerLatitude;
    private final double smallerLongitude;
    private final double biggerLongitude;

    public IgpGeo(String smallerLatitude, String biggerLatitude, String smallerLongitude, String biggerLongitude) {
        int halfCircleInDegrees = 180;

        double lat1 = parseDouble(smallerLatitude);
        double lat2 = parseDouble(biggerLatitude);

        double lon1 = parseDouble(smallerLongitude);
        double lon2 = parseDouble(biggerLongitude);

        this.smallerLatitude = lat1 / halfCircleInDegrees;
        this.biggerLatitude = lat2 / halfCircleInDegrees;
        this.smallerLongitude = lon1 / halfCircleInDegrees;
        this.biggerLongitude = lon2 / halfCircleInDegrees;
    }

    public double getSmallerLatitude() {
        return smallerLatitude;
    }

    public double getBiggerLatitude() {
        return biggerLatitude;
    }

    public double getSmallerLongitude() {
        return smallerLongitude;
    }

    public double getBiggerLongitude() {
        return biggerLongitude;
    }
}