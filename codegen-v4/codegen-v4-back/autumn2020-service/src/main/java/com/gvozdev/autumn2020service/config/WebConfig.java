package com.gvozdev.autumn2020service.config;

import com.gvozdev.autumn2020service.util.queryparam.file.UrlFileQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.inputfilename.UrlInputFileNameQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.lang.UrlLangQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.oop.UrlOopQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.UrlOutputFileNameQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.var.UrlVarQueryParamConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new UrlVarQueryParamConverter());
        registry.addConverter(new UrlLangQueryParamConverter());
        registry.addConverter(new UrlFileQueryParamConverter());
        registry.addConverter(new UrlOopQueryParamConverter());
        registry.addConverter(new UrlInputFileNameQueryParamConverter());
        registry.addConverter(new UrlOutputFileNameQueryParamConverter());
    }
}