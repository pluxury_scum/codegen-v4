package com.gvozdev.autumn2020service.util.queryparam.oop;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.autumn2020service.util.queryparam.oop.Oop.valueOf;

@Component
public class UrlOopQueryParamConverter implements Converter<String, Oop> {

    @Override
    public Oop convert(String source) {
        return valueOf(source.toUpperCase());
    }
}