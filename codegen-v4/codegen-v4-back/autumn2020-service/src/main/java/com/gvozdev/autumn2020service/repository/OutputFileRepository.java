package com.gvozdev.autumn2020service.repository;

import com.gvozdev.autumn2020service.entity.OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OutputFileRepository extends JpaRepository<OutputFile, Long> {

    @Query(
        value = """
                SELECT * FROM autumn_2020_output_file f\040
                WHERE f.var = ?1\040
                AND f.lang = ?2\040
                AND f.file = ?3\040
                AND f.oop = ?4\040
                AND f.name = ?5
            """, nativeQuery = true
    )
    OutputFile findFileByParameters(String var, String lang, String file, String oop, String name);
}
