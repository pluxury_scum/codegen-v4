package com.gvozdev.autumn2020service.domain.var3;

import java.util.List;

import static java.lang.StrictMath.pow;
import static java.util.stream.IntStream.range;

public record IonosphericDelay(WeightMatrix weightMatrix, Tec tec) {
    public double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();

        return delayInTecu * tecuToMetersCoefficient;
    }

    private static double getTecuToMetersCoefficient() {
        double l1 = 1_575_420_000;
        double oneTecUnit = 1E16;

        return 40.3 / pow(l1, 2) * oneTecUnit;
    }

    private double getDelayInTecu() {
        List<Double> weights = weightMatrix.getWeights();
        List<Integer> tecArray = tec.tec();

        return range(0, 4)
            .mapToDouble(observation -> {
                double weight = weights.get(observation);
                double rawTec = tecArray.get(observation);
                double tecInOneTecUnit = rawTec * 0.1;

                return weight * tecInOneTecUnit;
            })
            .sum();
    }
}