package com.gvozdev.autumn2020service.util.queryparam.file;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.autumn2020service.util.queryparam.file.File.valueOf;

@Component
public class UrlFileQueryParamConverter implements Converter<String, File> {

    @Override
    public File convert(String source) {
        return valueOf(source.toUpperCase());
    }
}
