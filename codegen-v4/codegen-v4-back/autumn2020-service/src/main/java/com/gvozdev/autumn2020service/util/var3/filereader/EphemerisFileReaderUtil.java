package com.gvozdev.autumn2020service.util.var3.filereader;

import com.gvozdev.autumn2020service.util.common.CommonUtil;
import com.gvozdev.autumn2020service.util.number.OneDigitNumber;
import com.gvozdev.autumn2020service.util.number.TwoDigitNumber;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.lang.Character.getNumericValue;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.joining;

@Component
public record EphemerisFileReaderUtil(CommonUtil commonUtil) {
    public boolean isOneDigitSatelliteNumberValid(List<List<List<Byte>>> allLines, int observation) {
        try {
            boolean firstDigitExists = allLines.get(observation).get(0).get(0) != null;
            boolean satelliteNumberSizeEqualsOne = allLines.get(observation).get(0).size() == 1;

            return firstDigitExists && satelliteNumberSizeEqualsOne;
        } catch (RuntimeException ignored) {
            return false;
        }
    }

    public boolean isTwoDigitSatelliteNumberValid(List<List<List<Byte>>> allLines, int observation) {
        try {
            boolean firstDigitExists = allLines.get(observation).get(0).get(0) != null;
            boolean secondDigitExists = allLines.get(observation).get(0).get(1) != null;
            boolean satelliteNumberSizeEqualsTwo = allLines.get(observation).get(0).size() == 2;

            return firstDigitExists && secondDigitExists && satelliteNumberSizeEqualsTwo;
        } catch (RuntimeException ignored) {
            return false;
        }
    }

    public boolean areSatelliteNumbersEqual(OneDigitNumber requiredSatelliteNumber, int observation, List<List<List<Byte>>> allLines) {
        char receivedFirstDigit = (char) (byte) allLines.get(observation).get(0).get(0);

        OneDigitNumber receivedSatelliteNumber = new OneDigitNumber(receivedFirstDigit);

        return requiredSatelliteNumber.equals(receivedSatelliteNumber);
    }

    public boolean areSatelliteNumbersEqual(TwoDigitNumber requiredSatelliteNumber, int observation, List<List<List<Byte>>> allLines) {
        char receivedFirstDigit = (char) (byte) allLines.get(observation).get(0).get(0);
        char receivedSecondDigit = (char) (byte) allLines.get(observation).get(0).get(1);

        TwoDigitNumber receivedSatelliteNumber = new TwoDigitNumber(receivedFirstDigit, receivedSecondDigit);

        return requiredSatelliteNumber.equals(receivedSatelliteNumber);
    }

    public boolean isHourValueEven(List<List<List<Byte>>> allLines, int observation) {
        List<Byte> hoursNumberInfo = allLines.get(observation).get(4);
        int hourValue = getHourValue(hoursNumberInfo);

        return commonUtil.isEven(hourValue);
    }

    public int getHourValue(List<Byte> hoursNumberInfo) {
        int digitsInHour = hoursNumberInfo.size();
        int hourFirstDigit = getNumericValue(hoursNumberInfo.get(0));

        if (digitsInHour == 1) {
            return hourFirstDigit;
        } else {
            return getDoubleDigitHourValue(hoursNumberInfo, hourFirstDigit);
        }
    }

    public double getNumericGpsTime(List<List<List<Byte>>> allLines, int observation) {
        int observationOffset = 7;

        String numeric = allLines.get(observation + observationOffset).get(0).stream()
            .map(aByte -> {
                char symbol = (char) aByte.byteValue();

                if (symbol == 'D') {
                    return 'E';
                }

                return symbol;
            })
            .map(Object::toString)
            .collect(joining());

        return parseDouble(numeric);
    }

    public double getNumericIonCoefficient(List<List<Byte>> line, int number) {
        String numeric = line.get(number).stream()
            .map(aByte -> {
                char symbol = (char) aByte.byteValue();

                if (symbol == 'D') {
                    return 'E';
                }

                return symbol;
            })
            .map(Object::toString)
            .collect(joining());

        return parseDouble(numeric);
    }

    private static int getDoubleDigitHourValue(List<Byte> hoursNumberInfo, int hourFirstDigit) {
        int hourSecondDigitIndex = 1;

        int hourSecondDigit = getNumericValue(hoursNumberInfo.get(hourSecondDigitIndex));
        String hourFull = hourFirstDigit + Integer.toString(hourSecondDigit);

        return parseInt(hourFull);
    }
}
