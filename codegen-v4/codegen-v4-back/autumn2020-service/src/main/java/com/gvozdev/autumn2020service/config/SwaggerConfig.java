package com.gvozdev.autumn2020service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.ant;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createDocket() {
        return new Docket(SWAGGER_2)
            .apiInfo(getApiInfo())
            .select()
            .paths(ant("/api/**"))
            .apis(basePackage("com.gvozdev.autumn2020service"))
            .build();
    }

    private static ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
            .title("CodeGen v4 API")
            .description("Autumn 2020 service")
            .contact(getContact())
            .version("1.0")
            .build();
    }

    private static Contact getContact() {
        String name = "Alexander Gvozdev";
        String gitlabProfileLink = "https://gitlab.com/9035719268/codegen-v4";
        String email = "9035719268a@gmail.com";

        return new Contact(name, gitlabProfileLink, email);
    }
}
