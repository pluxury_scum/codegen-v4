package com.gvozdev.autumn2020service.service.resolver.var1;

import com.gvozdev.autumn2020service.domain.var1.Satellite;

import java.util.List;

public record ExerciseInfoWrapper(List<Satellite> satellites) {
}
