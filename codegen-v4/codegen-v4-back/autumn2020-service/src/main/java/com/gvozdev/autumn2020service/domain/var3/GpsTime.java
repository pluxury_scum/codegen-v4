package com.gvozdev.autumn2020service.domain.var3;

import java.util.List;

public record GpsTime(List<Double> gpsTime) {
}
