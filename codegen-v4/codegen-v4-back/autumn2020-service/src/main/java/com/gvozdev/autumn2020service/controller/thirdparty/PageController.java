package com.gvozdev.autumn2020service.controller.thirdparty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("Autumn2020ThirdPartyController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/year/2020/autumn")
public class PageController {

    @GetMapping("/swagger-ui")
    public String getSwagger() {
        return "redirect:/swagger-ui.html";
    }
}
