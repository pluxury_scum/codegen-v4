package com.gvozdev.autumn2020service.domain.var2;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.IntStream.range;

public record AngularVelocities(Satellite satellite, int amountOfObservations) {

    public List<Double> getAverageAngularVelocities() {
        int observationsPerHour = 120;
        List<Double> angularVelocities = getAngularVelocities();
        List<Double> averageVelocities = new ArrayList<>();

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public List<Double> getAngularVelocities() {
        double oneHourInSeconds = 3600;

        List<Double> elevationAnglesArray = satellite.elevation();
        double previousElevationAngle = elevationAnglesArray.get(0);
        List<Double> angularVelocities = new ArrayList<>(amountOfObservations);

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAnglesArray.get(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.add(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }

        return angularVelocities;
    }

    private static double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        return range(start, end)
            .mapToDouble(velocities::get)
            .sum();
    }
}