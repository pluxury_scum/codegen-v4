package com.gvozdev.autumn2020service.service.resolver.var1;

import com.gvozdev.autumn2020service.domain.var1.Satellite;
import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.entity.OutputFile;
import com.gvozdev.autumn2020service.service.jpa.api.InputFileService;
import com.gvozdev.autumn2020service.service.jpa.api.OutputFileService;
import com.gvozdev.autumn2020service.util.common.CommonUtil;
import com.gvozdev.autumn2020service.util.index.ComponentIndexes;
import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.oop.Oop;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.autumn2020service.util.queryparam.var.Var;
import com.gvozdev.autumn2020service.util.var1.chart.PseudoRangesChartDrawer;
import com.gvozdev.autumn2020service.util.var1.controller.ControllerUtil;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReader;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReaderUtil;
import com.gvozdev.autumn2020service.util.var1.fileservice.FileService;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.autumn2020service.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.MANUAL;
import static com.gvozdev.autumn2020service.util.queryparam.var.Var.VAR1;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component("Var1Resolver")
public record Resolver(
    InputFileService inputFileService, OutputFileService outputFileService, ComponentIndexes componentIndexes,
    ControllerUtil controllerUtil, FileReaderUtil fileReaderUtil, CommonUtil commonUtil
) {
    private static final Var VAR = VAR1;

    public ExerciseInfoWrapper getExerciseInfo(InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        return new ExerciseInfoWrapper(satellites);
    }

    public byte[] getListing(Lang lang, File file, Oop oop, InputFileName inputFileName, OutputFileName outputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName()
        );

        byte[] inputFileBytes = inputFile.getFileBytes();
        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing = "";

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(inputFileBytes, outputFileBytes);
        } else if (file.equals(MANUAL)) {
            listing = controllerUtil.getFilledListingForManual(inputFileBytes, outputFileBytes, lang);
        }

        return listing.getBytes(UTF_8);
    }

    public byte[] getCharts(InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        Satellite satellite1 = satellites.get(0);
        Satellite satellite2 = satellites.get(1);
        Satellite satellite3 = satellites.get(2);

        PseudoRangesChartDrawer pseudoRangesChartDrawer = new PseudoRangesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] pseudoRangesChartInBytes = pseudoRangesChartDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(pseudoRangesChartInBytes);

        return byteArrayOutputStream.toByteArray();
    }
}
