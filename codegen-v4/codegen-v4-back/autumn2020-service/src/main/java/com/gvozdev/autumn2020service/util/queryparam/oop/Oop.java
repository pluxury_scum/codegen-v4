package com.gvozdev.autumn2020service.util.queryparam.oop;

public enum Oop {
    WITHOOP, NOOOP;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
