package com.gvozdev.autumn2020service.service.resolver.var3;

import com.gvozdev.autumn2020service.domain.var3.Components;
import com.gvozdev.autumn2020service.domain.var3.IonosphericDelay;
import com.gvozdev.autumn2020service.domain.var3.KlobucharModelDelay;
import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.entity.OutputFile;
import com.gvozdev.autumn2020service.service.jpa.api.InputFileService;
import com.gvozdev.autumn2020service.service.jpa.api.OutputFileService;
import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.queryparam.oop.Oop;
import com.gvozdev.autumn2020service.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.autumn2020service.util.queryparam.var.Var;
import com.gvozdev.autumn2020service.util.var3.chart.DelaysChartDrawer;
import com.gvozdev.autumn2020service.util.var3.controller.ControllerUtil;
import com.gvozdev.autumn2020service.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.autumn2020service.util.var3.filereader.EphemerisFileReaderUtil;
import com.gvozdev.autumn2020service.util.var3.filereader.IonoFileReader;
import com.gvozdev.autumn2020service.util.var3.filereader.IonoFileReaderUtil;
import com.gvozdev.autumn2020service.util.var3.fileservice.FileService;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.autumn2020service.util.constant.Constants.*;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.MANUAL;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.*;
import static com.gvozdev.autumn2020service.util.queryparam.var.Var.VAR3;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component("Var3Resolver")
public record Resolver(
    InputFileService inputFileService, OutputFileService outputFileService, ControllerUtil controllerUtil,
    EphemerisFileReaderUtil ephemerisFileReaderUtil, IonoFileReaderUtil ionoFileReaderUtil
) {
    private static final Var VAR = VAR3;

    public Components getExerciseInfo(String latitude, String longitude, String satelliteNumber) {
        InputFile ephemerisFile = inputFileService.findByName(BRDC.getFileName());
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            ephemerisFileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        InputFile forecastFile = inputFileService.findByName(IGRG.getFileName());
        byte[] forecastFileBytes = forecastFile.getFileBytes();
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes, ionoFileReaderUtil);

        InputFile preciseFile = inputFileService.findByName(IGSG.getFileName());
        byte[] preciseFileBytes = preciseFile.getFileBytes();
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes, ionoFileReaderUtil);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader, preciseIonoFileReader, satelliteNumber, FORECAST_TEC_LIST_FIRST_LINE, PRECISE_TEC_LIST_FIRST_LINE
        );

        String smallerLatitude = controllerUtil.getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = controllerUtil.getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = controllerUtil.getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = controllerUtil.getBiggerLongitudeForInterpolation(longitude);

        return fileService.getComponents(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);
    }

    public byte[] getListing(Lang lang, File file, Oop oop, OutputFileName outputFileName, String latitude, String longitude, String satelliteNumber) {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName()
        );

        InputFile ephemerisFile = inputFileService.findByName(BRDC.getFileName());
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();

        InputFile forecastFile = inputFileService.findByName(IGRG.getFileName());
        byte[] forecastFileBytes = forecastFile.getFileBytes();

        InputFile preciseFile = inputFileService.findByName(IGSG.getFileName());
        byte[] preciseFileBytes = preciseFile.getFileBytes();

        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing = "";

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(outputFileBytes, satelliteNumber, latitude, longitude);
        } else if (file.equals(MANUAL)) {
            listing =
                controllerUtil.getFilledListingForManual(ephemerisFileBytes, forecastFileBytes, preciseFileBytes, outputFileBytes, satelliteNumber, latitude,
                    longitude);
        }

        return listing.getBytes(UTF_8);
    }

    public byte[] getCharts(String latitude, String longitude, String satelliteNumber) {
        InputFile ephemerisFile = inputFileService.findByName(BRDC.getFileName());
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            ephemerisFileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil,
            START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        InputFile forecastFile = inputFileService.findByName(IGRG.getFileName());
        byte[] forecastFileBytes = forecastFile.getFileBytes();
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes, ionoFileReaderUtil);

        InputFile preciseFile = inputFileService.findByName(IGSG.getFileName());
        byte[] preciseFileBytes = preciseFile.getFileBytes();
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes, ionoFileReaderUtil);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader, preciseIonoFileReader, satelliteNumber,
            FORECAST_TEC_LIST_FIRST_LINE, PRECISE_TEC_LIST_FIRST_LINE
        );

        String smallerLatitude = controllerUtil.getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = controllerUtil.getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = controllerUtil.getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = controllerUtil.getBiggerLongitudeForInterpolation(longitude);

        Components components = fileService.getComponents(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);

        List<IonosphericDelay> forecastDelays = controllerUtil.getForecastDelays(
            components, latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude
        );

        List<IonosphericDelay> preciseDelays = controllerUtil.getPreciseDelays(
            components, latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude
        );

        List<KlobucharModelDelay> klobucharModelDelays = controllerUtil.getKlobucharModels(components, latitude, longitude);

        DelaysChartDrawer delaysChartDrawer = new DelaysChartDrawer(forecastDelays, preciseDelays, klobucharModelDelays, VAR3_AMOUNT_OF_OBSERVATIONS);
        byte[] delaysChartInBytes = delaysChartDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(delaysChartInBytes);

        return byteArrayOutputStream.toByteArray();
    }
}
