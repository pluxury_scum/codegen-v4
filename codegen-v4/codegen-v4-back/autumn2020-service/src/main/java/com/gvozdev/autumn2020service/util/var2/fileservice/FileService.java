package com.gvozdev.autumn2020service.util.var2.fileservice;

import com.gvozdev.autumn2020service.domain.var2.Satellite;
import com.gvozdev.autumn2020service.util.common.CommonUtil;
import com.gvozdev.autumn2020service.util.number.OneDigitNumber;
import com.gvozdev.autumn2020service.util.number.TwoDigitNumber;
import com.gvozdev.autumn2020service.util.var2.filereader.FileReader;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public record FileService(FileReader fileReader, CommonUtil commonUtil) {
    public List<Satellite> getSatellites() {
        return range(0, 3)
            .mapToObj(this::createSatellite)
            .collect(toList());
    }

    private Satellite createSatellite(int satelliteIndex) {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satelliteNumber = satelliteNumbers.get(satelliteIndex + 1);

        List<List<Double>> elevationAngles = extractElevationAngles(satelliteNumbers);
        List<Double> elevation = elevationAngles.get(satelliteIndex);

        return new Satellite(satelliteNumber, elevation);
    }

    private List<Integer> extractSatelliteNumbers() {
        return fileReader.getSatelliteNumbers();
    }

    private List<List<Double>> extractElevationAngles(List<Integer> satelliteNumbers) {
        return satelliteNumbers.stream()
            .map(this::extractElevationAnglesForSatellite)
            .collect(toList());
    }

    private List<Double> extractElevationAnglesForSatellite(int satelliteNumber) {
        char satelliteNumberFirstDigit = commonUtil.toChar(commonUtil.getFirstDigit(satelliteNumber));

        if (commonUtil.isDoubleDigit(satelliteNumber)) {
            char satelliteNumberSecondDigit = commonUtil.toChar(commonUtil.getSecondDigit(satelliteNumber));

            TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(satelliteNumberFirstDigit, satelliteNumberSecondDigit);

            return new ArrayList<>(fileReader.getElevationAngles(requiredSatelliteNumber));
        } else {
            OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(satelliteNumberFirstDigit);

            return new ArrayList<>(fileReader.getElevationAngles(requiredSatelliteNumber));
        }
    }
}
