package com.gvozdev.autumn2020service.util.var2.chart;

import com.gvozdev.autumn2020service.domain.var2.AngularVelocities;
import com.gvozdev.autumn2020service.domain.var2.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static java.awt.Color.WHITE;
import static java.util.stream.IntStream.range;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class AngularVelocitiesChartDrawer extends TemplateChartDrawer {
    public AngularVelocitiesChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    public XYSeriesCollection getMeasurementsSeriesCollection() {
        AngularVelocities angularVelocitiesForSatellite1 = new AngularVelocities(satellite1, amountOfObservations);
        List<Double> satellite1Velocities = angularVelocitiesForSatellite1.getAngularVelocities();

        AngularVelocities angularVelocitiesForSatellite2 = new AngularVelocities(satellite2, amountOfObservations);
        List<Double> satellite2Velocities = angularVelocitiesForSatellite2.getAngularVelocities();

        AngularVelocities angularVelocitiesForSatellite3 = new AngularVelocities(satellite3, amountOfObservations);
        List<Double> satellite3Velocities = angularVelocitiesForSatellite3.getAngularVelocities();

        XYSeries satellite1VelocitiesSeries = new XYSeries("Спутник #" + satellite1.number());
        XYSeries satellite2VelocitiesSeries = new XYSeries("Спутник #" + satellite2.number());
        XYSeries satellite3VelocitiesSeries = new XYSeries("Спутник #" + satellite3.number());

        range(0, amountOfObservations).forEach(observation -> {
            double angularVelocityForSatellite1 = satellite1Velocities.get(observation);
            satellite1VelocitiesSeries.add(observation, angularVelocityForSatellite1);

            double angularVelocityForSatellite2 = satellite2Velocities.get(observation);
            satellite2VelocitiesSeries.add(observation, angularVelocityForSatellite2);

            double angularVelocityForSatellite3 = satellite3Velocities.get(observation);
            satellite3VelocitiesSeries.add(observation, angularVelocityForSatellite3);
        });

        XYSeriesCollection angularVelocitiesSeriesCollection = new XYSeriesCollection();
        angularVelocitiesSeriesCollection.addSeries(satellite1VelocitiesSeries);
        angularVelocitiesSeriesCollection.addSeries(satellite2VelocitiesSeries);
        angularVelocitiesSeriesCollection.addSeries(satellite3VelocitiesSeries);

        return angularVelocitiesSeriesCollection;
    }

    @Override
    public JFreeChart createChart(XYSeriesCollection observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Угловая скорость, рад/c",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    @Override
    public void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(LEGEND_FONT);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.95, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }
}
