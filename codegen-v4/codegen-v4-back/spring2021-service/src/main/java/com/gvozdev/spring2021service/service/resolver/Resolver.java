package com.gvozdev.spring2021service.service.resolver;

import com.gvozdev.spring2021service.domain.Satellite;
import com.gvozdev.spring2021service.entity.InputFile;
import com.gvozdev.spring2021service.entity.OutputFile;
import com.gvozdev.spring2021service.service.jpa.api.InputFileService;
import com.gvozdev.spring2021service.service.jpa.api.OutputFileService;
import com.gvozdev.spring2021service.util.chart.ElevationAnglesChartDrawer;
import com.gvozdev.spring2021service.util.chart.TemplateChartDrawer;
import com.gvozdev.spring2021service.util.chart.TroposphericDelaysChartDrawer;
import com.gvozdev.spring2021service.util.common.CommonUtil;
import com.gvozdev.spring2021service.util.componentindex.ComponentIndexes;
import com.gvozdev.spring2021service.util.controller.ControllerUtil;
import com.gvozdev.spring2021service.util.filereader.FileReader;
import com.gvozdev.spring2021service.util.filereader.FileReaderUtil;
import com.gvozdev.spring2021service.util.fileservice.FileService;
import com.gvozdev.spring2021service.util.queryparam.file.File;
import com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.spring2021service.util.queryparam.lang.Lang;
import com.gvozdev.spring2021service.util.queryparam.oop.Oop;
import com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.spring2021service.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.spring2021service.util.queryparam.file.File.FILE;
import static com.gvozdev.spring2021service.util.queryparam.file.File.MANUAL;
import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.RESOURCES;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component("Spring2021Resolver")
public record Resolver(
    InputFileService inputFileService, OutputFileService outputFileService, ComponentIndexes componentIndexes,
    ControllerUtil controllerUtil, FileReaderUtil fileReaderUtil, CommonUtil commonUtil
) {
    public ExerciseInfoWrapper getExerciseInfo(InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        return new ExerciseInfoWrapper(satellites);
    }

    public byte[] getListing(Lang lang, File file, Oop oop, InputFileName inputFileName, OutputFileName outputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());
        OutputFile outputFile = outputFileService.findByParameters(lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();
        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing = "";

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(inputFileBytes, outputFileBytes);
        } else if (file.equals(MANUAL)) {
            listing = controllerUtil.getFilledListingForManual(inputFileBytes, outputFileBytes, lang);
        }

        return listing.getBytes(UTF_8);
    }

    public byte[] getCharts(InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        Satellite satellite1 = satellites.get(0);
        Satellite satellite2 = satellites.get(1);
        Satellite satellite3 = satellites.get(2);

        TemplateChartDrawer troposphericDelaysChartDrawer = new TroposphericDelaysChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] troposphericDelaysChartInBytes = troposphericDelaysChartDrawer.getChartInBytes();

        TemplateChartDrawer elevationAnglesDrawer = new ElevationAnglesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] elevationAnglesChartInBytes = elevationAnglesDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(troposphericDelaysChartInBytes, elevationAnglesChartInBytes);

        return byteArrayOutputStream.toByteArray();
    }

    public byte[] getResources() {
        String inputFileName = RESOURCES.getFileName();
        InputFile inputFile = inputFileService.findByName(inputFileName);

        return inputFile.getFileBytes();
    }
}
