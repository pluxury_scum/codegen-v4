package com.gvozdev.spring2021service.domain;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public record TroposphericDelays(Satellite satellite, int amountOfObservations) {
    public List<Double> getTroposphericDelays() {
        List<Double> mdArray = satellite.md();
        List<Double> tdArray = satellite.td();
        List<Double> mwArray = satellite.mw();
        List<Double> twArray = satellite.tw();

        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                double md = mdArray.get(observation);
                double td = tdArray.get(observation);
                double mw = mwArray.get(observation);
                double tw = twArray.get(observation);

                return md * td + mw * tw;
            })
            .collect(toList());
    }
}
