package com.gvozdev.spring2021service.config;

import com.gvozdev.spring2021service.util.queryparam.file.UrlFileQueryParamConverter;
import com.gvozdev.spring2021service.util.queryparam.inputfilename.UrlInputFileNameQueryParamConverter;
import com.gvozdev.spring2021service.util.queryparam.lang.UrlLangQueryParamConverter;
import com.gvozdev.spring2021service.util.queryparam.oop.UrlOopQueryParamConverter;
import com.gvozdev.spring2021service.util.queryparam.outputfilename.UrlOutputFileNameQueryParamConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new UrlLangQueryParamConverter());
        registry.addConverter(new UrlFileQueryParamConverter());
        registry.addConverter(new UrlOopQueryParamConverter());
        registry.addConverter(new UrlInputFileNameQueryParamConverter());
        registry.addConverter(new UrlOutputFileNameQueryParamConverter());
    }
}