package com.gvozdev.spring2021service.util.queryparam.lang;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.spring2021service.util.queryparam.lang.Lang.valueOf;

@Component
public class UrlLangQueryParamConverter implements Converter<String, Lang> {

    @Override
    public Lang convert(String source) {
        return valueOf(source.toUpperCase());
    }
}