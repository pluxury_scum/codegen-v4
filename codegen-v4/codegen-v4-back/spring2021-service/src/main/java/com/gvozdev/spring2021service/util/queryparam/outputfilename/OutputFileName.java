package com.gvozdev.spring2021service.util.queryparam.outputfilename;

import java.util.stream.Stream;

public enum OutputFileName {
    CPP_MAIN("main.cpp"),
    JAVA_MAIN("Main.java"),
    JAVA_GRAPH_DRAWER("GraphDrawer.java"),
    PYTHON_MAIN("main.py"),
    CHARTS("charts.rar");

    private final String fileName;

    OutputFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public static OutputFileName getFileNameFromString(String otherFileName) {
        return Stream.of(values())
            .filter(outputFileName -> outputFileName.getFileName().equalsIgnoreCase(otherFileName))
            .findFirst()
            .orElseThrow();
    }
}
