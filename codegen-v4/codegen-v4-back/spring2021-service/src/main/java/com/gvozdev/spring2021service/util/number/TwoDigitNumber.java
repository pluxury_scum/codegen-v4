package com.gvozdev.spring2021service.util.number;

import static java.util.Objects.hash;

public record TwoDigitNumber(char firstDigit, char secondDigit) {
    private static final int SIZE = 2;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        TwoDigitNumber otherNumber = (TwoDigitNumber) other;

        return firstDigit == otherNumber.firstDigit() && secondDigit == otherNumber.secondDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, secondDigit, SIZE);
    }
}
