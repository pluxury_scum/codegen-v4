package com.gvozdev.spring2021service.util.queryparam.oop;

public enum Oop {
    WITHOOP, NOOOP;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
