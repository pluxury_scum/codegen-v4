package com.gvozdev.spring2021service.service.resolver;

import com.gvozdev.spring2021service.domain.Satellite;

import java.util.List;

public record ExerciseInfoWrapper(List<Satellite> satellites) {
}
