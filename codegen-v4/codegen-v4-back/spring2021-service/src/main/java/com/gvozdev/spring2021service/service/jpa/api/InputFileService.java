package com.gvozdev.spring2021service.service.jpa.api;

import com.gvozdev.spring2021service.entity.InputFile;

public interface InputFileService {
    InputFile findByName(String name);

    void save(InputFile inputFile);
}
