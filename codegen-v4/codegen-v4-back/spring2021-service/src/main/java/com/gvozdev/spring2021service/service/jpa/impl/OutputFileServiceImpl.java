package com.gvozdev.spring2021service.service.jpa.impl;

import com.gvozdev.spring2021service.entity.OutputFile;
import com.gvozdev.spring2021service.repository.OutputFileRepository;
import com.gvozdev.spring2021service.service.jpa.api.OutputFileService;
import org.springframework.stereotype.Service;

@Service
public class OutputFileServiceImpl implements OutputFileService {
    private final OutputFileRepository outputFileRepository;

    public OutputFileServiceImpl(OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    public OutputFile findByParameters(String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(lang, file, oop, name);
    }

    @Override
    public void save(OutputFile outputFile) {
        outputFileRepository.save(outputFile);
    }
}
