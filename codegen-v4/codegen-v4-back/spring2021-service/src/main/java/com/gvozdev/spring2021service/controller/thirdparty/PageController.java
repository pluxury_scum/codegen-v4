package com.gvozdev.spring2021service.controller.thirdparty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("Spring2021ThirdPartyController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/year/2021/spring")
public class PageController {

    @GetMapping("/swagger-ui")
    public String getSwagger() {
        return "redirect:/swagger-ui.html";
    }
}
