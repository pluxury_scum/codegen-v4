package com.gvozdev.spring2021service.domain;

import java.util.List;

public record Satellite(
    int number,
    List<Double> md, List<Double> td, List<Double> mw, List<Double> tw, List<Double> elevation
) {
}
