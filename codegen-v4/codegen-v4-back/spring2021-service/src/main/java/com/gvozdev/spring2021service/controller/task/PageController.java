package com.gvozdev.spring2021service.controller.task;

import com.gvozdev.spring2021service.service.resolver.ExerciseInfoWrapper;
import com.gvozdev.spring2021service.service.resolver.Resolver;
import com.gvozdev.spring2021service.util.queryparam.file.File;
import com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.spring2021service.util.queryparam.lang.Lang;
import com.gvozdev.spring2021service.util.queryparam.oop.Oop;
import com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.RESOURCES;
import static com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName.CHARTS;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("TaskController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2021/spring/")
public class PageController {
    private final Resolver resolver;

    public PageController(Resolver resolver) {
        this.resolver = resolver;
    }

    @GetMapping("/exercise-info/{inputFileName}")
    public ResponseEntity<ExerciseInfoWrapper> getExerciseInfo(@PathVariable("inputFileName") InputFileName inputFileName) {
        ExerciseInfoWrapper exerciseInfo = resolver.getExerciseInfo(inputFileName);

        return new ResponseEntity<>(exerciseInfo, OK);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") Lang lang,
        @PathVariable("file") File file,
        @PathVariable("oop") Oop oop,
        @PathVariable("inputFileName") InputFileName inputFileName,
        @PathVariable("outputFileName") OutputFileName outputFileName
    ) {
        byte[] listing = resolver.getListing(lang, file, oop, inputFileName, outputFileName);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(outputFileName.getFileName()).build());

        return new ResponseEntity<>(listing, responseHeaders, OK);
    }

    @GetMapping("/download/charts/{inputFileName}")
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") InputFileName inputFileName) {
        byte[] charts = resolver.getCharts(inputFileName);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(CHARTS.getFileName()).build());

        return new ResponseEntity<>(charts, responseHeaders, OK);
    }

    @GetMapping("/download/resources")
    public ResponseEntity<byte[]> downloadResources() {
        byte[] resources = resolver.getResources();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(RESOURCES.getFileName()).build());

        return new ResponseEntity<>(resources, responseHeaders, OK);
    }
}
