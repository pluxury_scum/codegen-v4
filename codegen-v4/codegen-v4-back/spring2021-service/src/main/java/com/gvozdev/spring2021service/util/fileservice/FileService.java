package com.gvozdev.spring2021service.util.fileservice;

import com.gvozdev.spring2021service.domain.Satellite;
import com.gvozdev.spring2021service.util.common.CommonUtil;
import com.gvozdev.spring2021service.util.filereader.FileReader;
import com.gvozdev.spring2021service.util.number.OneDigitNumber;
import com.gvozdev.spring2021service.util.number.TwoDigitNumber;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public record FileService(FileReader fileReader, CommonUtil commonUtil) {
    public List<Satellite> getSatellites() {
        return range(0, 3)
            .mapToObj(this::createSatellite)
            .collect(toList());
    }

    private Satellite createSatellite(int satelliteIndex) {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satelliteNumber = satelliteNumbers.get(satelliteIndex + 1);

        List<List<List<Double>>> troposphericDelayComponents = extractTroposphericDelayComponents(satelliteNumbers);
        List<List<Double>> components = troposphericDelayComponents.get(satelliteIndex);
        List<Double> md = components.get(0);
        List<Double> td = components.get(1);
        List<Double> mw = components.get(2);
        List<Double> tw = components.get(3);

        List<List<Double>> elevationAngles = extractElevationAngles(satelliteNumbers);
        List<Double> elevation = elevationAngles.get(satelliteIndex);

        return new Satellite(satelliteNumber, md, td, mw, tw, elevation);
    }

    private List<Integer> extractSatelliteNumbers() {
        return fileReader.getSatelliteNumbers();
    }

    private List<List<List<Double>>> extractTroposphericDelayComponents(List<Integer> satelliteNumbers) {
        return satelliteNumbers.stream()
            .map(this::getTroposphericDelayComponentsForSatellite)
            .collect(toList());
    }

    private List<List<Double>> getTroposphericDelayComponentsForSatellite(int satelliteNumber) {
        char satelliteNumberFirstDigit = commonUtil.toChar(commonUtil.getFirstDigit(satelliteNumber));

        if (commonUtil.isDoubleDigit(satelliteNumber)) {
            char satelliteNumberSecondDigit = commonUtil.toChar(commonUtil.getSecondDigit(satelliteNumber));

            TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(satelliteNumberFirstDigit, satelliteNumberSecondDigit);

            return fileReader.getTroposphericDelayComponents(requiredSatelliteNumber);
        } else {
            OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(satelliteNumberFirstDigit);

            return fileReader.getTroposphericDelayComponents(requiredSatelliteNumber);
        }
    }

    private List<List<Double>> extractElevationAngles(List<Integer> satelliteNumbers) {
        return satelliteNumbers.stream()
            .map(this::extractElevationAnglesForSatellite)
            .collect(toList());
    }

    private List<Double> extractElevationAnglesForSatellite(int satelliteNumber) {
        char satelliteNumberFirstDigit = commonUtil.toChar(commonUtil.getFirstDigit(satelliteNumber));

        if (commonUtil.isDoubleDigit(satelliteNumber)) {
            char satelliteNumberSecondDigit = commonUtil.toChar(commonUtil.getSecondDigit(satelliteNumber));

            TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(satelliteNumberFirstDigit, satelliteNumberSecondDigit);

            return new ArrayList<>(fileReader.getElevationAngles(requiredSatelliteNumber));
        } else {
            OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(satelliteNumberFirstDigit);

            return new ArrayList<>(fileReader.getElevationAngles(requiredSatelliteNumber));
        }
    }
}
