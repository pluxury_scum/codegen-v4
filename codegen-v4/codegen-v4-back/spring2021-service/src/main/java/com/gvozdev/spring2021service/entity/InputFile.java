package com.gvozdev.spring2021service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serial;
import java.io.Serializable;

@Entity
@Table(name = "spring_2021_input_file")
public class InputFile implements Serializable {

    @Serial
    private static final long serialVersionUID = -103361391623832891L;

    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "file_bytes", nullable = false)
    private byte[] fileBytes;

    public InputFile() {
    }

    public InputFile(long id, String name, byte[] fileBytes) {
        this.id = id;
        this.name = name;
        this.fileBytes = fileBytes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }
}
