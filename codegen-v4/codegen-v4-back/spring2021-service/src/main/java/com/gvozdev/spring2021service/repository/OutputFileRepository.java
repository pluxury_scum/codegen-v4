package com.gvozdev.spring2021service.repository;

import com.gvozdev.spring2021service.entity.OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OutputFileRepository extends JpaRepository<OutputFile, Long> {

    @Query(
        value = """
                SELECT * FROM spring_2021_output_file f\040
                WHERE f.lang = ?1\040
                AND f.file = ?2\040
                AND f.oop = ?3\040
                AND f.name = ?4
            """, nativeQuery = true
    )
    OutputFile findFileByParameters(String lang, String file, String oop, String name);
}
