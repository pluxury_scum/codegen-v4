package com.gvozdev.spring2021service.util.number;

import static java.util.Objects.hash;

public record OneDigitNumber(char firstDigit) {
    private static final int SIZE = 1;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        OneDigitNumber otherNumber = (OneDigitNumber) other;

        return firstDigit == otherNumber.firstDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, SIZE);
    }
}
