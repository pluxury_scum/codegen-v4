package com.gvozdev.spring2021service.domain;

import java.util.List;

import static java.util.stream.Collectors.toList;

public record ElevationAngles(Satellite satellite) {
    public List<Double> getAnglesInHalfCircles() {
        int halfCircleInDegrees = 180;
        List<Double> elevationAnglesInDegrees = satellite.elevation();

        return elevationAnglesInDegrees.stream()
            .map(angleInDegrees -> angleInDegrees / halfCircleInDegrees)
            .collect(toList());
    }
}