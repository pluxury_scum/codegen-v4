package com.gvozdev.spring2021service.unit;

import com.gvozdev.spring2021service.entity.InputFile;
import com.gvozdev.spring2021service.service.jpa.api.InputFileService;
import com.gvozdev.spring2021service.util.componentindex.ComponentIndexes;
import com.gvozdev.spring2021service.util.filereader.FileReader;
import com.gvozdev.spring2021service.util.filereader.FileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.spring2021service.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.ARTI_6;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SatelliteNumbersTests {
    private final InputFileService inputFileService;
    private final ComponentIndexes componentIndexes;
    private final FileReaderUtil fileReaderUtil;

    @Autowired
    SatelliteNumbersTests(
        InputFileService inputFileService, ComponentIndexes componentIndexes, FileReaderUtil fileReaderUtil
    ) {
        this.inputFileService = inputFileService;
        this.componentIndexes = componentIndexes;
        this.fileReaderUtil = fileReaderUtil;
    }

    @Test
    void shouldCheckSatelliteNumbers() {
        InputFile inputFile = inputFileService.findByName(ARTI_6.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);

        List<Integer> expectedSatelliteNumbers = asList(3, 6, 9, 16, 22, 23, 26, 31);
        List<Integer> actualSatelliteNumbers = fileReader.getSatelliteNumbers();

        assertEquals(expectedSatelliteNumbers, actualSatelliteNumbers);
    }
}
