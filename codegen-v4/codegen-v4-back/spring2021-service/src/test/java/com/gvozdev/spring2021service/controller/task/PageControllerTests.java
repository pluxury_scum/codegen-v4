package com.gvozdev.spring2021service.controller.task;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.spring2021service.util.queryparam.file.File.FILE;
import static com.gvozdev.spring2021service.util.queryparam.file.File.MANUAL;
import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.ARTI_6;
import static com.gvozdev.spring2021service.util.queryparam.lang.Lang.*;
import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName.*;
import static com.gvozdev.spring2021service.util.template.task.Template.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.text.MessageFormat.format;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PageControllerTests {
    private final MockMvc mockMvc;

    @Autowired
    PageControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void shouldCheckExerciseInfo() throws Exception {
        String url = format(EXERCISE_INFO, ARTI_6.getFileName());

        mockMvc.perform(
                get(url).characterEncoding(UTF_8)
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$.satellites.[*].number", hasSize(3)))
            .andExpect(jsonPath("$.satellites.[*].elevation", hasSize(3)))
            .andExpect(jsonPath("$.satellites.[*].md", hasSize(3)))
            .andExpect(jsonPath("$.satellites.[*].td", hasSize(3)))
            .andExpect(jsonPath("$.satellites.[*].mw", hasSize(3)))
            .andExpect(jsonPath("$.satellites.[*].tw", hasSize(3)));
    }

    @Test
    void shouldCheckExerciseInfoWrongInputFileName() throws Exception {
        String url = format(EXERCISE_INFO, "wrong_input_file_name");

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, "wrong_file_value", NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppFile() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, "wrong_oop_value", ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppManual() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, WITHOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, NOOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, WITHOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, NOOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, FILE, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            CPP, MANUAL, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaFileWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaFileWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaManualWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaManualWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, "wrong_oop_value", ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, "wrong_oop_value", ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaFileWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, "wrong_input_file_name", JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaFileWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, "wrong_input_file_name", JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaManualWithoopMain() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, "wrong_input_file_name", JAVA_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaManualWithoopGraphDrawer() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, "wrong_input_file_name", JAVA_GRAPH_DRAWER.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingPythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingPythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", FILE, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            "wrong_lang_value", MANUAL, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, "wrong_file_value", NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonFile() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, "wrong_oop_value", ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonManual() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, NOOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, WITHOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, NOOOP, "wrong_input_file_name", PYTHON_MAIN
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, FILE, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualWithoop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualNooop() throws Exception {
        String url = format(
            DOWNLOAD_LISTING,
            PYTHON, MANUAL, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadCharts() throws Exception {
        String url = format(DOWNLOAD_CHARTS, ARTI_6);

        mockMvc.perform(get(url))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    void shouldCheckDownloadChartsWrongInputFileName() throws Exception {
        String url = format(DOWNLOAD_CHARTS, "wrong_input_file_name");

        mockMvc.perform(get(url))
            .andExpect(status().isBadRequest());
    }

    @Test
    void shouldCheckDownloadResources() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }
}