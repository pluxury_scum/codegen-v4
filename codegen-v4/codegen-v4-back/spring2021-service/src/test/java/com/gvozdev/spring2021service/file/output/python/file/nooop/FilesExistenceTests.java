package com.gvozdev.spring2021service.file.output.python.file.nooop;

import com.gvozdev.spring2021service.entity.OutputFile;
import com.gvozdev.spring2021service.service.jpa.api.OutputFileService;
import com.gvozdev.spring2021service.util.queryparam.file.File;
import com.gvozdev.spring2021service.util.queryparam.lang.Lang;
import com.gvozdev.spring2021service.util.queryparam.oop.Oop;
import com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.spring2021service.util.queryparam.lang.Lang.PYTHON;
import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName.PYTHON_MAIN;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FilesExistenceTests {
    private static final Lang LANG = PYTHON;
    private static final File FILE = File.FILE;
    private static final Oop OOP = NOOOP;
    private static final OutputFileName OUTPUT_FILE_NAME = PYTHON_MAIN;

    private final OutputFileService outputFileService;

    @Autowired
    FilesExistenceTests(OutputFileService outputFileService) {
        this.outputFileService = outputFileService;
    }

    @Test
    void shouldCheckMainExists() {
        OutputFile outputFile = outputFileService.findByParameters(
            LANG.toString(), FILE.toString(), OOP.toString(),
            OUTPUT_FILE_NAME.getFileName()
        );

        assertNotNull(outputFile);
    }
}

