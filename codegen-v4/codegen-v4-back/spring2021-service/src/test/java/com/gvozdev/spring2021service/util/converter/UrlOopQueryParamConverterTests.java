package com.gvozdev.spring2021service.util.converter;

import com.gvozdev.spring2021service.util.queryparam.oop.Oop;
import com.gvozdev.spring2021service.util.queryparam.oop.UrlOopQueryParamConverter;
import org.junit.jupiter.api.Test;

import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.WITHOOP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlOopQueryParamConverterTests {
    private final UrlOopQueryParamConverter converter = new UrlOopQueryParamConverter();

    @Test
    void shouldCheckWithOopValueConversion() {
        Oop withoop = converter.convert("withoop");

        assertEquals(WITHOOP, withoop);
    }

    @Test
    void shouldCheckNoOopValueConversion() {
        Oop nooop = converter.convert("nooop");

        assertEquals(NOOOP, nooop);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> converter.convert("wrong_oop_value")
        );
    }
}
