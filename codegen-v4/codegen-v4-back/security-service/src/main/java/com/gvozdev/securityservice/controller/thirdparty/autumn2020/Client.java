package com.gvozdev.securityservice.controller.thirdparty.autumn2020;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/year/2020/autumn", name = "AUTUMN-2020-THIRD-PARTY-CLIENT")
public interface Client {

    @GetMapping("/actuator/health")
    ResponseEntity<String> getActuator();

    @GetMapping("/swagger-ui")
    ResponseEntity<String> getSwagger();
}
