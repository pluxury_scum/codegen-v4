package com.gvozdev.securityservice.controller.task.autumn2020.resource;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("ResourceController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2020/autumn/download")
public class PageController {
    private static final String SERVICE_NAME = "AUTUMN2020-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/resources")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadResources() {
        return client.downloadResources();
    }

    @GetMapping("/reference")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadReference() {
        return client.downloadReference();
    }

    private static ResponseEntity<String> autumn2020ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Autumn2020 недоступен", SERVICE_UNAVAILABLE);
    }
}
