package com.gvozdev.securityservice.util.thirdparty;

public class AntMatchers {
    public static final String AUTUMN2020_THIRD_PARTY = "/api/year/2020/autumn/**";

    public static final String SPRING2021_THIRD_PARTY = "/api/year/2021/spring/**";

    private AntMatchers() {
    }
}
