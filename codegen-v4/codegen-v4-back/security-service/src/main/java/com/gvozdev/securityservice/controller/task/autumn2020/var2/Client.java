package com.gvozdev.securityservice.controller.task.autumn2020.var2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/tasks/year/2020/autumn/var2", name = "AUTUMN2020-VAR2-CLIENT")
public interface Client {

    @GetMapping("/exercise-info/{inputFileName}")
    ResponseEntity<String> getExerciseInfo(@PathVariable("inputFileName") String inputFileName);

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("inputFileName") String inputFileName,
        @PathVariable("outputFileName") String outputFileName
    );

    @GetMapping("/download/charts/{inputFileName}")
    ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") String inputFileName);
}
