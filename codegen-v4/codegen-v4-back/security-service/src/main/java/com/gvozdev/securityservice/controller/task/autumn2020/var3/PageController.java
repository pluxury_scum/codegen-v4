package com.gvozdev.securityservice.controller.task.autumn2020.var3;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("Autumn2020Var3Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var3")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private static final String SERVICE_NAME = "AUTUMN2020-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/exercise-info")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<String> getExerciseInfo(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return client.getExerciseInfo(latitude, longitude, satelliteNumber);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{outputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("outputFileName") String outputFileName,
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return client.downloadListing(lang, file, oop, outputFileName, latitude, longitude, satelliteNumber);
    }

    @GetMapping("/download/charts")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadCharts(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return client.downloadCharts(latitude, longitude, satelliteNumber);
    }

    private static ResponseEntity<String> autumn2020ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Autumn2020 недоступен", SERVICE_UNAVAILABLE);
    }
}