package com.gvozdev.securityservice.controller.ping.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/year/2021/spring/actuator", name = "SPRING2021-PING-CLIENT")
public interface Spring2021Client extends BasePingClient {
}
