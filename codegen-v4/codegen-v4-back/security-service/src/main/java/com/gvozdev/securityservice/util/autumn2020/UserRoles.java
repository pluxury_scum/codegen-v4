package com.gvozdev.securityservice.util.autumn2020;

import static com.gvozdev.securityservice.domain.Authority.*;

public class UserRoles {
    public static final String[] AUTUMN2020_ALL = {
        ROLE_ADMIN.getRole(),
        ROLE_A20201.getRole(),
        ROLE_A20202.getRole(),
        ROLE_A20203.getRole()
    };

    public static final String[] AUTUMN2020_VAR1 = {
        ROLE_ADMIN.getRole(),
        ROLE_A20201.getRole()
    };

    public static final String[] AUTUMN2020_VAR2 = {
        ROLE_ADMIN.getRole(),
        ROLE_A20202.getRole()
    };

    public static final String[] AUTUMN2020_VAR3 = {
        ROLE_ADMIN.getRole(),
        ROLE_A20203.getRole()
    };

    private UserRoles() {
    }
}
