package com.gvozdev.securityservice.util.autumn2020;

public class AntMatchers {
    public static final String AUTUMN2020_VAR1 = "/api/tasks/year/2020/autumn/var1/**";

    public static final String AUTUMN2020_VAR2 = "/api/tasks/year/2020/autumn/var2/**";

    public static final String AUTUMN2020_VAR3 = "/api/tasks/year/2020/autumn/var3/**";

    public static final String AUTUMN2020_DOWNLOAD_RESOURCES = "/api/tasks/year/2020/autumn/download/resources";

    public static final String AUTUMN2020_DOWNLOAD_REFERENCE = "/api/tasks/year/2020/autumn/download/reference";

    private AntMatchers() {
    }
}
