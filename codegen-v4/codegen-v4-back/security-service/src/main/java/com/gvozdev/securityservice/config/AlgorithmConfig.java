package com.gvozdev.securityservice.config;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static java.nio.charset.StandardCharsets.UTF_8;

@Configuration
public class AlgorithmConfig {

    @Bean
    public Algorithm algorithm() {
        return HMAC256("secret".getBytes(UTF_8));
    }
}
