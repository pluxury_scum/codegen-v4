package com.gvozdev.securityservice.util.ping;

import static com.gvozdev.securityservice.domain.Authority.ROLE_ADMIN;

public class UserRoles {
    public static final String[] PING_ALL = {
        ROLE_ADMIN.getRole()
    };

    private UserRoles() {
    }
}
