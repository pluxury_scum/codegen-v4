package com.gvozdev.securityservice.controller.task.spring2021;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("Spring2021Controller")
@RequestMapping("/api/tasks/year/2021/spring")
public class PageController {
    private static final String SERVICE_NAME = "SPRING2021-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/exercise-info/{inputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "spring2021ServiceFallback")
    public ResponseEntity<String> getExerciseInfo(@PathVariable("inputFileName") String inputFileName) {
        return client.getExerciseInfo(inputFileName);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "spring2021ServiceFallback")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("inputFileName") String inputFileName,
        @PathVariable("outputFileName") String outputFileName
    ) {
        return client.downloadListing(lang, file, oop, inputFileName, outputFileName);
    }

    @GetMapping("/download/charts/{inputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "spring2021ServiceFallback")
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") String inputFileName) {
        return client.downloadCharts(inputFileName);
    }

    @GetMapping("/download/resources")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "spring2021ServiceFallback")
    public ResponseEntity<byte[]> downloadResources() {
        return client.downloadResources();
    }

    private static ResponseEntity<String> spring2021ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Spring2021 недоступен", SERVICE_UNAVAILABLE);
    }
}
