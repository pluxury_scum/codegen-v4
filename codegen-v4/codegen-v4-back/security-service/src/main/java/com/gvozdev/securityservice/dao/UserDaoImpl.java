package com.gvozdev.securityservice.dao;

import com.gvozdev.securityservice.domain.Student;
import com.gvozdev.securityservice.domain.StudentRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.gvozdev.securityservice.util.auth.SqlRequests.FIND_STUDENT_BY_USER_NAME;

@Repository
public class UserDaoImpl implements UserDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserDaoImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Optional<Student> findUserByUserName(String userName) {
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("userName", userName);

        return namedParameterJdbcTemplate.query(FIND_STUDENT_BY_USER_NAME, queryParams, new StudentRowMapper(namedParameterJdbcTemplate))
            .stream()
            .findFirst();
    }
}
