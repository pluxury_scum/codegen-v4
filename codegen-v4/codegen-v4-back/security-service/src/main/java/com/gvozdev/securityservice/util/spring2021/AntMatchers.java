package com.gvozdev.securityservice.util.spring2021;

public class AntMatchers {
    public static final String SPRING2021 = "/api/tasks/year/2021/spring/**";

    private AntMatchers() {
    }
}
