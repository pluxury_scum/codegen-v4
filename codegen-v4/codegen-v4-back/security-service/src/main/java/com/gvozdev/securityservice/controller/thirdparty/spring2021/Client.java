package com.gvozdev.securityservice.controller.thirdparty.spring2021;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/year/2021/spring", name = "SPRING-2021-THIRD-PARTY-CLIENT")
public interface Client {

    @GetMapping("/actuator/health")
    ResponseEntity<String> getActuator();

    @GetMapping("/swagger-ui")
    ResponseEntity<String> getSwagger();
}
