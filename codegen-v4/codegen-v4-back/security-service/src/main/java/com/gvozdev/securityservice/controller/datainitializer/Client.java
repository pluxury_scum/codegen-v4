package com.gvozdev.securityservice.controller.datainitializer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/initializer", name = "DATA-INITIALIZER-SERVICE-CLIENT-GATEWAY")
public interface Client {

    @GetMapping("/actuator/health")
    ResponseEntity<String> getActuator();

    @GetMapping("/security/initialize")
    ResponseEntity<String> initializeSecurityData();

    @GetMapping("/autumn2020/initialize")
    ResponseEntity<String> initializeAutumn2020Data();

    @GetMapping("/spring2021/initialize")
    ResponseEntity<String> initializeSpring2021Data();
}
