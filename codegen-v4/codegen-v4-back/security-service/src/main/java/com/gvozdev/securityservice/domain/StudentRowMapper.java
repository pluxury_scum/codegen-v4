package com.gvozdev.securityservice.domain;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.gvozdev.securityservice.util.auth.SqlRequests.GET_STUDENT_AUTHORITIES;
import static java.util.stream.Collectors.toSet;

public record StudentRowMapper(NamedParameterJdbcTemplate namedParameterJdbcTemplate) implements RowMapper<Student> {

    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        long id = resultSet.getLong("id");
        String userName = resultSet.getString("user_name");
        String password = resultSet.getString("password");
        boolean accountNonExpired = resultSet.getBoolean("account_non_expired");
        boolean accountNonLocked = resultSet.getBoolean("account_non_locked");
        boolean credentialsNonExpired = resultSet.getBoolean("credentials_non_expired");
        boolean enabled = resultSet.getBoolean("enabled");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("studentId", id);

        List<String> roles = namedParameterJdbcTemplate.queryForList(GET_STUDENT_AUTHORITIES, queryParams, String.class);

        Set<SimpleGrantedAuthority> authorities = roles.stream()
            .map(SimpleGrantedAuthority::new)
            .collect(toSet());

        return new Student(id, userName, password, accountNonExpired, accountNonLocked, credentialsNonExpired, enabled, authorities);
    }
}
