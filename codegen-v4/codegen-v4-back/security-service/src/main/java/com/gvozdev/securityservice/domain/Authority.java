package com.gvozdev.securityservice.domain;

public enum Authority {
    ROLE_ADMIN("ADMIN"),
    ROLE_A20201("A20201"),
    ROLE_A20202("A20202"),
    ROLE_A20203("A20203"),
    ROLE_S2021("S2021");

    private final String role;

    Authority(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
