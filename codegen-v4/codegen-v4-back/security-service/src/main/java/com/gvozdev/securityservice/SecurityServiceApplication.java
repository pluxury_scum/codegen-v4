package com.gvozdev.securityservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import static org.springframework.boot.SpringApplication.run;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class SecurityServiceApplication {
    public static void main(String[] args) {
        run(SecurityServiceApplication.class, args);
    }
}
