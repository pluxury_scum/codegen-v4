package com.gvozdev.securityservice.config;

import com.auth0.jwt.algorithms.Algorithm;
import com.gvozdev.securityservice.filter.CustomAuthenticationFilter;
import com.gvozdev.securityservice.filter.CustomAuthorizationFilter;
import com.gvozdev.securityservice.service.filter.api.AuthorizationFilterService;
import com.gvozdev.securityservice.util.auth.AuthUtil;
import com.gvozdev.securityservice.util.autumn2020.UserRoles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.gvozdev.securityservice.util.auth.AntMatchers.LOGIN;
import static com.gvozdev.securityservice.util.auth.AntMatchers.PUBLIC;
import static com.gvozdev.securityservice.util.autumn2020.AntMatchers.*;
import static com.gvozdev.securityservice.util.autumn2020.UserRoles.AUTUMN2020_ALL;
import static com.gvozdev.securityservice.util.datainitializer.AntMatchers.DATA_INITIALIZER;
import static com.gvozdev.securityservice.util.datainitializer.UserRoles.DATA_INITIALIZER_ALL;
import static com.gvozdev.securityservice.util.ping.AntMatchers.PING;
import static com.gvozdev.securityservice.util.ping.UserRoles.PING_ALL;
import static com.gvozdev.securityservice.util.spring2021.AntMatchers.SPRING2021;
import static com.gvozdev.securityservice.util.spring2021.UserRoles.SPRING2021_ALL;
import static com.gvozdev.securityservice.util.thirdparty.AntMatchers.AUTUMN2020_THIRD_PARTY;
import static com.gvozdev.securityservice.util.thirdparty.AntMatchers.SPRING2021_THIRD_PARTY;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final AuthorizationFilterService authorizationFilterService;
    private final PasswordEncoder passwordEncoder;
    private final AuthUtil authUtil;
    private final Algorithm algorithm;

    public SecurityConfig(
        UserDetailsService userDetailsService, AuthorizationFilterService authorizationFilterService,
        PasswordEncoder passwordEncoder, AuthUtil authUtil, Algorithm algorithm
    ) {
        this.userDetailsService = userDetailsService;
        this.authorizationFilterService = authorizationFilterService;
        this.passwordEncoder = passwordEncoder;
        this.authUtil = authUtil;
        this.algorithm = algorithm;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean(), authUtil, algorithm);
        customAuthenticationFilter.setFilterProcessesUrl(LOGIN);

        http
            .cors().and().csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .authorizeRequests().antMatchers(PUBLIC).permitAll()
            .and()
            .authorizeRequests().antMatchers(PING).hasAnyRole(PING_ALL)
            .and()
            .authorizeRequests().antMatchers(DATA_INITIALIZER).hasAnyRole(DATA_INITIALIZER_ALL)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_THIRD_PARTY).hasAnyRole(AUTUMN2020_ALL)
            .and()
            .authorizeRequests().antMatchers(SPRING2021_THIRD_PARTY).hasAnyRole(SPRING2021_ALL)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_VAR1).hasAnyRole(UserRoles.AUTUMN2020_VAR1)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_VAR2).hasAnyRole(UserRoles.AUTUMN2020_VAR2)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_VAR3).hasAnyRole(UserRoles.AUTUMN2020_VAR3)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_DOWNLOAD_RESOURCES).hasAnyRole(AUTUMN2020_ALL)
            .and()
            .authorizeRequests().antMatchers(AUTUMN2020_DOWNLOAD_REFERENCE).hasAnyRole(AUTUMN2020_ALL)
            .and()
            .authorizeRequests().antMatchers(SPRING2021).hasAnyRole(SPRING2021_ALL)
            .and()
            .authorizeRequests().anyRequest().authenticated()
            .and()
            .addFilter(customAuthenticationFilter)
            .addFilterBefore(
                new CustomAuthorizationFilter(authorizationFilterService, authUtil, algorithm),
                UsernamePasswordAuthenticationFilter.class
            );
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
