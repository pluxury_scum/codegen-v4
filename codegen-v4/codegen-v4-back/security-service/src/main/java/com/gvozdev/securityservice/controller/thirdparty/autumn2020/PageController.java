package com.gvozdev.securityservice.controller.thirdparty.autumn2020;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("Autumn2020ThirdPartyController")
@RequestMapping("/api/year/2020/autumn")
public class PageController {
    private static final String SERVICE_NAME = "AUTUMN2020-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/actuator/health")
    public ResponseEntity<String> getActuator() {
        return client.getActuator();
    }

    @GetMapping("/swagger-ui")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<String> getSwagger() {
        return client.getSwagger();
    }

    private static ResponseEntity<String> autumn2020ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Autumn2020 недоступен", SERVICE_UNAVAILABLE);
    }
}
