package com.gvozdev.securityservice.service.token.impl;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.gvozdev.securityservice.service.jdbc.UserService;
import com.gvozdev.securityservice.service.token.api.TokenService;
import com.gvozdev.securityservice.util.auth.AuthUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.auth0.jwt.JWT.require;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
public class TokenServiceImpl implements TokenService {
    private final Algorithm algorithm;
    private final AuthUtil authUtil;

    public TokenServiceImpl(Algorithm algorithm, AuthUtil authUtil) {
        this.algorithm = algorithm;
        this.authUtil = authUtil;
    }

    @Override
    public Map<String, String> getNewTokens(HttpServletRequest request, UserService userService) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        String refreshToken = authorizationHeader.substring("Bearer ".length());

        JWTVerifier jwtVerifier = require(algorithm).build();
        DecodedJWT decodedJWT = jwtVerifier.verify(refreshToken);

        String userName = decodedJWT.getSubject();
        UserDetails userDetails = userService.getUserByUserName(userName);
        String accessToken = authUtil.getAccessToken(request, userDetails, algorithm);

        Map<String, String> tokens = new HashMap<>();
        tokens.put("access_token", accessToken);
        tokens.put("refresh_token", refreshToken);

        return tokens;
    }

    @Override
    public void handleRefreshTokenError(HttpServletResponse response) {
        response.setStatus(FORBIDDEN.value());
        response.setContentType(APPLICATION_JSON_VALUE);
    }

    @Override
    public Map<String, String> getRefreshTokenErrorInfo(Exception exception) {
        Map<String, String> errorInfo = new HashMap<>();
        errorInfo.put("error_message", exception.getMessage());

        return errorInfo;
    }

    @Override
    public void handleInvalidRefreshToken(HttpServletResponse response) {
        response.setStatus(FORBIDDEN.value());
        response.setContentType(APPLICATION_JSON_VALUE);
    }

    @Override
    public Map<String, String> getInvalidRefreshTokenErrorInfo() {
        Map<String, String> errorInfo = new HashMap<>();
        errorInfo.put("error_message", "Refresh token is invalid");

        return errorInfo;
    }
}
