package com.gvozdev.securityservice.util.ping;

public class AntMatchers {
    public static final String PING = "/api/ping/service/**";

    private AntMatchers() {
    }
}
