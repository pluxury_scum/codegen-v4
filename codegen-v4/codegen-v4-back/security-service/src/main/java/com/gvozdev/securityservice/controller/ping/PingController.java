package com.gvozdev.securityservice.controller.ping;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.controller.ping.client.Autumn2020Client;
import com.gvozdev.securityservice.controller.ping.client.DataInitializerClient;
import com.gvozdev.securityservice.controller.ping.client.GatewayClient;
import com.gvozdev.securityservice.controller.ping.client.Spring2021Client;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@Controller("TestController")
@RequestMapping("/api/ping/service")
public class PingController {
    private final GatewayClient gatewayClient;
    private final DataInitializerClient dataInitializerClient;
    private final Autumn2020Client autumn2020Client;
    private final Spring2021Client spring2021Client;

    public PingController(
        GatewayClient gatewayClient, DataInitializerClient dataInitializerClient,
        Autumn2020Client autumn2020Client, Spring2021Client spring2021Client
    ) {
        this.gatewayClient = gatewayClient;
        this.dataInitializerClient = dataInitializerClient;
        this.autumn2020Client = autumn2020Client;
        this.spring2021Client = spring2021Client;
    }

    @GetMapping("/gateway")
    public ResponseEntity<String> pingGatewayService() {
        ResponseEntity<String> response = gatewayClient.getHealth();

        return new ResponseEntity<>(response.getBody(), OK);
    }

    @GetMapping("/data-initializer")
    public ResponseEntity<String> pingDataInitializerService() {
        ResponseEntity<String> response = dataInitializerClient.getHealth();

        return new ResponseEntity<>(response.getBody(), OK);
    }

    @GetMapping("/autumn2020")
    public ResponseEntity<String> pingAutumn2020Service() {
        ResponseEntity<String> response = autumn2020Client.getHealth();

        return new ResponseEntity<>(response.getBody(), OK);
    }

    @GetMapping("/spring2021")
    public ResponseEntity<String> pingSpring2021Service() {
        ResponseEntity<String> response = spring2021Client.getHealth();

        return new ResponseEntity<>(response.getBody(), OK);
    }
}
