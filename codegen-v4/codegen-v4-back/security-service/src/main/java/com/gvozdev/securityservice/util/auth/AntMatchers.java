package com.gvozdev.securityservice.util.auth;

public class AntMatchers {
    public static final String LOGIN = "/api/login";

    public static final String REFRESH_TOKEN = "/api/token/refresh";

    public static final String[] PUBLIC = {
        LOGIN, REFRESH_TOKEN
    };

    private AntMatchers() {
    }
}
