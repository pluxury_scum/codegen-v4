package com.gvozdev.securityservice.controller.task.autumn2020.var3;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/tasks/year/2020/autumn/var3", name = "AUTUMN2020-VAR3-CLIENT")
public interface Client {

    @GetMapping("/exercise-info")
    ResponseEntity<String> getExerciseInfo(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    );

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{outputFileName}")
    ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("outputFileName") String outputFileName,
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    );

    @GetMapping("/download/charts")
    ResponseEntity<byte[]> downloadCharts(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    );
}
