package com.gvozdev.securityservice.util.auth;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

import static com.auth0.jwt.JWT.create;
import static com.gvozdev.securityservice.util.auth.AntMatchers.LOGIN;
import static com.gvozdev.securityservice.util.auth.AntMatchers.REFRESH_TOKEN;
import static java.lang.System.currentTimeMillis;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.web.context.support.WebApplicationContextUtils.getWebApplicationContext;

@Component
public class AuthUtil {

    @Value("${access-token.expiration-time-in-ms}")
    private int accessTokenExpirationTime;

    @Value("${refresh-token.expiration-time-in-ms}")
    private int refreshTokenExpirationTime;

    public String getAccessToken(HttpServletRequest request, UserDetails userDetails, Algorithm algorithm) {
        Date expirationTime = new Date(currentTimeMillis() + accessTokenExpirationTime);
        List<String> userRoles = getUserRoles(userDetails);

        return create()
            .withSubject(userDetails.getUsername())
            .withExpiresAt(expirationTime)
            .withIssuer(request.getRequestURL().toString())
            .withClaim("roles", userRoles)
            .sign(algorithm);
    }

    public String getRefreshToken(HttpServletRequest request, UserDetails userDetails, Algorithm algorithm) {
        Date expirationTime = new Date(currentTimeMillis() + refreshTokenExpirationTime);
        List<String> userRoles = getUserRoles(userDetails);

        return create()
            .withSubject(userDetails.getUsername())
            .withExpiresAt(expirationTime)
            .withIssuer(request.getRequestURL().toString())
            .withClaim("roles", userRoles)
            .sign(algorithm);
    }

    public Algorithm getAlgorithm(ServletRequest request) {
        ServletContext servletContext = request.getServletContext();
        WebApplicationContext webApplicationContext = getWebApplicationContext(servletContext);
        return requireNonNull(webApplicationContext).getBean(Algorithm.class);
    }

    public boolean authorizationExists(SecurityContext securityContext) {
        return securityContext.getAuthentication() != null;
    }

    public boolean shouldSetAuthorization(HttpServletRequest request, String servletPath) {
        return isTokenValid(request) && isTokenNeeded(servletPath);
    }

    public boolean isTokenValid(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);

        return authorizationHeader != null && authorizationHeader.startsWith("Bearer ");
    }

    private static boolean isTokenNeeded(String servletPath) {
        return !isLoginPath(servletPath) && !isTokenRefreshPath(servletPath);
    }

    private static boolean isLoginPath(String servletPath) {
        return servletPath.equals(LOGIN);
    }

    private static boolean isTokenRefreshPath(String servletPath) {
        return servletPath.equals(REFRESH_TOKEN);
    }

    private static List<String> getUserRoles(UserDetails userDetails) {
        return userDetails.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(toList());
    }
}
