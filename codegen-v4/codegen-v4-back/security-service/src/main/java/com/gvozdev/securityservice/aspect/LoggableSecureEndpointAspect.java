package com.gvozdev.securityservice.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

import static java.lang.String.join;
import static java.lang.System.currentTimeMillis;
import static java.util.Arrays.stream;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Aspect
@Slf4j
@Component
public class LoggableSecureEndpointAspect {

    @Pointcut("@annotation(com.gvozdev.securityservice.annotation.LoggableSecureEndpoint)")
    public void loggableSecureEndpointsPointcut() {
    }

    @Around("loggableSecureEndpointsPointcut()")
    public Object logMethodExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long start = currentTimeMillis();
        Object methodResult = proceedingJoinPoint.proceed();
        long end = currentTimeMillis();
        long timeTaken = end - start;

        log.info("Время выполнения метода: {} миллисекунд", timeTaken);

        return methodResult;
    }

    @After("loggableSecureEndpointsPointcut()")
    public void logSecureEndpointUser(JoinPoint joinPoint) {
        Authentication authentication = getContext().getAuthentication();

        String principalName = authentication.getName();

        String fullPathAccessed = getFullPathAccessed(joinPoint);

        log.info("Пользователь {} обратился к конечной точке {}", principalName, fullPathAccessed);
    }

    private static String getFullPathAccessed(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        joinPoint.getArgs();

        return join("",
            getRequestMappingValue(methodSignature),
            getGetMappingValueIfExists(method),
            getPostMappingValueIfExists(method),
            getPutMappingValueIfExists(method),
            getPatchMappingValueIfExists(method),
            getDeleteMappingValueIfExists(method)
        );
    }

    private static String getRequestMappingValue(Signature signature) {
        Class<?> clazz = signature.getDeclaringType();
        RequestMapping requestMappingAnnotation = clazz.getAnnotation(RequestMapping.class);
        return stream(requestMappingAnnotation.value())
            .findFirst()
            .orElse("");
    }

    private static String getGetMappingValueIfExists(Method method) {
        GetMapping getMappingAnnotation = method.getAnnotation(GetMapping.class);

        if (getMappingAnnotation != null) {
            return stream(getMappingAnnotation.value())
                .findFirst()
                .orElse("");
        } else {
            return "";
        }
    }

    private static String getPostMappingValueIfExists(Method method) {
        PostMapping postMappingAnnotation = method.getAnnotation(PostMapping.class);

        if (postMappingAnnotation != null) {
            return stream(postMappingAnnotation.value())
                .findFirst()
                .orElse("");
        } else {
            return "";
        }
    }

    private static String getPutMappingValueIfExists(Method method) {
        PutMapping putMappingAnnotation = method.getAnnotation(PutMapping.class);

        if (putMappingAnnotation != null) {
            return stream(putMappingAnnotation.value())
                .findFirst()
                .orElse("");
        } else {
            return "";
        }
    }

    private static String getPatchMappingValueIfExists(Method method) {
        PatchMapping patchMappingAnnotation = method.getAnnotation(PatchMapping.class);

        if (patchMappingAnnotation != null) {
            return stream(patchMappingAnnotation.value())
                .findFirst()
                .orElse("");
        } else {
            return "";
        }
    }

    private static String getDeleteMappingValueIfExists(Method method) {
        DeleteMapping deleteMappingAnnotation = method.getAnnotation(DeleteMapping.class);

        if (deleteMappingAnnotation != null) {
            return stream(deleteMappingAnnotation.value())
                .findFirst()
                .orElse("");
        } else {
            return "";
        }
    }
}
