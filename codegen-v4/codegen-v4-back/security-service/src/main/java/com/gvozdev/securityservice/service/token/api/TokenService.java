package com.gvozdev.securityservice.service.token.api;

import com.gvozdev.securityservice.service.jdbc.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface TokenService {
    Map<String, String> getNewTokens(HttpServletRequest request, UserService userService);

    void handleRefreshTokenError(HttpServletResponse response);

    Map<String, String> getRefreshTokenErrorInfo(Exception exception);

    void handleInvalidRefreshToken(HttpServletResponse response);

    Map<String, String> getInvalidRefreshTokenErrorInfo();
}
