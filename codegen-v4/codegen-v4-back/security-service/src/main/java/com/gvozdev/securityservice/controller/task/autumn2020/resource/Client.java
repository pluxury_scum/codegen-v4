package com.gvozdev.securityservice.controller.task.autumn2020.resource;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/tasks/year/2020/autumn/download", name = "AUTUMN2020-RESOURCE-CLIENT-GATEWAY")
public interface Client {

    @GetMapping("/resources")
    ResponseEntity<byte[]> downloadResources();

    @GetMapping("/reference")
    ResponseEntity<byte[]> downloadReference();
}
