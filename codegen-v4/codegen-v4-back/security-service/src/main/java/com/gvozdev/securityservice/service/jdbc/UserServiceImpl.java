package com.gvozdev.securityservice.service.jdbc;

import com.gvozdev.securityservice.dao.UserDao;
import com.gvozdev.securityservice.domain.Student;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return userDao.findUserByUserName(userName)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Override
    public Student getUserByUserName(String userName) throws UsernameNotFoundException {
        return userDao.findUserByUserName(userName)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
