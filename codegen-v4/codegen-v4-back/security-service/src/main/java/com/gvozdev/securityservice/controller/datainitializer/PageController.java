package com.gvozdev.securityservice.controller.datainitializer;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@Controller("DataInitializerController")
@RequestMapping("/api/initializer")
public class PageController {
    private static final String SERVICE_NAME = "DATA-INITIALIZER-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/actuator/health")
    public ResponseEntity<String> getActuator() {
        return client.getActuator();
    }

    @GetMapping("/security/initialize")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "dataInitializerServiceFallback")
    public ResponseEntity<String> initializeSecurityData() {
        return client.initializeSecurityData();
    }

    @GetMapping("/autumn2020/initialize")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "dataInitializerServiceFallback")
    public ResponseEntity<String> initializeAutumn2020Data() {
        return client.initializeAutumn2020Data();
    }

    @GetMapping("/spring2021/initialize")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "dataInitializerServiceFallback")
    public ResponseEntity<String> initializeSpring2021Data() {
        return client.initializeSpring2021Data();
    }

    public ResponseEntity<String> dataInitializerServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис инициализации данных недоступен", SERVICE_UNAVAILABLE);
    }
}
