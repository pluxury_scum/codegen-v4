package com.gvozdev.securityservice.service.filter.impl;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.gvozdev.securityservice.service.filter.api.AuthorizationFilterService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.auth0.jwt.JWT.require;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
public class AuthorizationFilterServiceImpl implements AuthorizationFilterService {

    @Override
    public Authentication getJwtAuthentication(HttpServletRequest request, Algorithm algorithm) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        String token = authorizationHeader.substring("Bearer ".length());

        JWTVerifier jwtVerifier = require(algorithm).build();
        DecodedJWT decodedJWT = jwtVerifier.verify(token);

        String userName = decodedJWT.getSubject();
        List<String> tokenBearerRoles = decodedJWT.getClaim("roles").asList(String.class);

        List<SimpleGrantedAuthority> authorities = tokenBearerRoles.stream()
            .map(SimpleGrantedAuthority::new)
            .collect(toList());

        return new UsernamePasswordAuthenticationToken(userName, null, authorities);
    }

    @Override
    public void handleJwtAuthenticationError(HttpServletResponse response) {
        response.setStatus(FORBIDDEN.value());
        response.setContentType(APPLICATION_JSON_VALUE);
    }

    @Override
    public Map<String, String> getJwtAuthenticationErrorInfo(Exception exception) {
        Map<String, String> errorInfo = new HashMap<>();
        errorInfo.put("error_message", exception.getMessage());

        return errorInfo;
    }
}
