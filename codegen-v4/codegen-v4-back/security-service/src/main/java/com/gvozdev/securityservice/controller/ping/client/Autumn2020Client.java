package com.gvozdev.securityservice.controller.ping.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/year/2020/autumn/actuator", name = "AUTUMN2020-PING-CLIENT")
public interface Autumn2020Client extends BasePingClient {
}
