package com.gvozdev.securityservice.util.path.thirdparty.autumn2020;

public class Path {
    public static final String ACTUATOR = """
        /api/year/2020/autumn/actuator/health""";

    public static final String SWAGGER_UI = """
        /api/year/2020/autumn/swagger-ui""";

    private Path() {
    }
}
