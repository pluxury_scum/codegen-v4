package com.gvozdev.securityservice.access.task.spring2021;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.config.annotation.*;
import com.gvozdev.securityservice.controller.task.spring2021.PageController;
import com.gvozdev.securityservice.util.ServerPingUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Method;

import static com.gvozdev.securityservice.util.path.task.spring2021.Path.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccessTests {
    private final ServerPingUtil serverPingUtil;
    private final MockMvc mockMvc;

    @Autowired
    AccessTests(ServerPingUtil serverPingUtil, MockMvc mockMvc) {
        this.serverPingUtil = serverPingUtil;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void checkIfSpring2021ServiceOnline() {
        assumeTrue(
            serverPingUtil.isSpring2021ServiceOnline(),
            "Сервис Spring2021 недоступен"
        );
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessExerciseInfoPath() throws Exception {
        mockMvc.perform(get(EXERCISE_INFO))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON));
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessDownloadListingPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_LISTING))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessDownloadChartsPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_CHARTS))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessExerciseInfoPath() throws Exception {
        mockMvc.perform(get(EXERCISE_INFO))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessDownloadListingPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_LISTING))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessDownloadChartsPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_CHARTS))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessExerciseInfoPath() throws Exception {
        mockMvc.perform(get(EXERCISE_INFO))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessDownloadListingPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_LISTING))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessDownloadChartsPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_CHARTS))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessExerciseInfoPath() throws Exception {
        mockMvc.perform(get(EXERCISE_INFO))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessDownloadListingPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_LISTING))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessDownloadChartsPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_CHARTS))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessExerciseInfoPath() throws Exception {
        mockMvc.perform(get(EXERCISE_INFO))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON));
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessDownloadListingPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_LISTING))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessDownloadChartsPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_CHARTS))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method getSatellites = PageController.class.getMethod("getExerciseInfo", String.class);
        Method downloadListing = PageController.class.getMethod("downloadListing", String.class, String.class, String.class, String.class, String.class);
        Method downloadCharts = PageController.class.getMethod("downloadCharts", String.class);
        Method downloadResources = PageController.class.getMethod("downloadResources");

        assertTrue(getSatellites.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadListing.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadCharts.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadResources.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
