package com.gvozdev.securityservice.config.mockuser;

import com.gvozdev.securityservice.domain.Student;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serial;
import java.util.Set;

public class MockUser extends Student {

    @Serial
    private static final long serialVersionUID = -8435066132612443584L;

    public MockUser(Set<SimpleGrantedAuthority> authorities) {
        super(-1, "[MOCK]", "",
            true, true, true, true,
            authorities
        );
    }
}
