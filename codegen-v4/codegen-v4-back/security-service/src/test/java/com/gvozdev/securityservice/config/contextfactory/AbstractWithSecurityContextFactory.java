package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.mockuser.MockUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.lang.annotation.Annotation;

import static org.springframework.security.core.context.SecurityContextHolder.createEmptyContext;

public abstract class AbstractWithSecurityContextFactory<T extends Annotation> implements WithSecurityContextFactory<T> {

    @Override
    public SecurityContext createSecurityContext(T t) {
        SecurityContext context = createEmptyContext();
        MockUser mockUser = getMockUser();

        Authentication authentication = new UsernamePasswordAuthenticationToken(
            mockUser.getUsername(), mockUser.getPassword(), mockUser.getAuthorities()
        );
        context.setAuthentication(authentication);

        return context;
    }

    protected abstract MockUser getMockUser();
}
