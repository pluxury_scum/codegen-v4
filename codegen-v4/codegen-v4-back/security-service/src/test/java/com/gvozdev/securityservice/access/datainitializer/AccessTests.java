package com.gvozdev.securityservice.access.datainitializer;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.config.annotation.*;
import com.gvozdev.securityservice.controller.datainitializer.PageController;
import com.gvozdev.securityservice.util.ServerPingUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Method;

import static com.gvozdev.securityservice.util.path.datainitializer.Path.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccessTests {
    private final ServerPingUtil serverPingUtil;
    private final MockMvc mockMvc;

    @Autowired
    AccessTests(ServerPingUtil serverPingUtil, MockMvc mockMvc) {
        this.serverPingUtil = serverPingUtil;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void checkIfDataInitializerServiceOnline() {
        assumeTrue(
            serverPingUtil.isDataInitializerServiceOnline(),
            "Сервис инициализации данных недоступен"
        );
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessSecurityDataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessAutumn2020DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessSpring2021DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessSecurityDataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessAutumn2020DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessSpring2021DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessSecurityDataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessAutumn2020DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessSpring2021DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessSecurityDataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessAutumn2020DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessSpring2021DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessSecurityDataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SECURITY))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessAutumn2020DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_AUTUMN2020))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessSpring2021DataInitializerPath() throws Exception {
        mockMvc.perform(get(INITIALIZE_SPRING2021))
            .andExpect(status().isForbidden());
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method initializeSecurityData = PageController.class.getMethod("initializeSecurityData");
        Method initializeAutumn2020Data = PageController.class.getMethod("initializeAutumn2020Data");
        Method initializeSpring2021Data = PageController.class.getMethod("initializeSpring2021Data");

        assertTrue(initializeSecurityData.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(initializeAutumn2020Data.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(initializeSpring2021Data.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
