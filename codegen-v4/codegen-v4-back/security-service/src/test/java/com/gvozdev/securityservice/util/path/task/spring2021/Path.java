package com.gvozdev.securityservice.util.path.task.spring2021;

public class Path {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2021/spring/exercise-info/arti_6hours.dat""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2021/spring/download/listing/cpp/file/withoop/arti_6hours.dat/main.cpp""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2021/spring/download/charts/arti_6hours.dat""";

    public static final String DOWNLOAD_RESOURCES = """
        /api/tasks/year/2021/spring/download/resources""";

    private Path() {
    }
}
