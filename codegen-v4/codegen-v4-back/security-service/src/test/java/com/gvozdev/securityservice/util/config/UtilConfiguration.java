package com.gvozdev.securityservice.util.config;

import com.gvozdev.securityservice.util.ServerPingUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.web.servlet.MockMvc;

@Configuration
public class UtilConfiguration {
    private final MockMvc mockMvc;

    public UtilConfiguration(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Bean
    public ServerPingUtil serverPingUtil() {
        return new ServerPingUtil(mockMvc);
    }
}
