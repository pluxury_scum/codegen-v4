package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.annotation.WithUserA20202;
import com.gvozdev.securityservice.config.mockuser.MockUserFactory;
import com.gvozdev.securityservice.config.mockuser.MockUser;

public class UserA20202ContextFactory extends AbstractWithSecurityContextFactory<WithUserA20202> {

    @Override
    public MockUser getMockUser() {
        return MockUserFactory.createA20202User();
    }
}
