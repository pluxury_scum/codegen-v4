package com.gvozdev.securityservice.util;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.securityservice.util.path.ping.Path.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc
public class ServerPingUtil {
    private final MockMvc mockMvc;

    public ServerPingUtil(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    public boolean isDataInitializerServiceOnline() {
        try {
            pingGatewayService();
            pingDataInitializerService();

            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean isAutumn2020ServiceOnline() {
        try {
            pingGatewayService();
            pingAutumn2020Service();

            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean isSpring2021ServiceOnline() {
        try {
            pingGatewayService();
            pingSpring2021Service();

            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    private void pingGatewayService() throws Exception {
        mockMvc.perform(get(GATEWAY_SERVICE));
    }

    private void pingDataInitializerService() throws Exception {
        mockMvc.perform(get(DATA_INITIALIZER_SERVICE));
    }

    private void pingAutumn2020Service() throws Exception {
        mockMvc.perform(get(AUTUMN2020_SERVICE));
    }

    private void pingSpring2021Service() throws Exception {
        mockMvc.perform(get(SPRING2021_SERVICE));
    }
}
