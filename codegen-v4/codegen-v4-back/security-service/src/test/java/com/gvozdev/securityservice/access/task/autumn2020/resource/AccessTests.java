package com.gvozdev.securityservice.access.task.autumn2020.resource;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.config.annotation.*;
import com.gvozdev.securityservice.controller.task.autumn2020.resource.PageController;
import com.gvozdev.securityservice.util.ServerPingUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Method;

import static com.gvozdev.securityservice.util.path.task.autumn2020.resource.Path.DOWNLOAD_REFERENCE;
import static com.gvozdev.securityservice.util.path.task.autumn2020.resource.Path.DOWNLOAD_RESOURCES;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccessTests {
    private final ServerPingUtil serverPingUtil;
    private final MockMvc mockMvc;

    @Autowired
    AccessTests(ServerPingUtil serverPingUtil, MockMvc mockMvc) {
        this.serverPingUtil = serverPingUtil;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void checkIfAutumn2020ServiceOnline() {
        assumeTrue(
            serverPingUtil.isAutumn2020ServiceOnline(),
            "Сервис Autumn2020 недоступен"
        );
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessDownloadReferencePath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessDownloadReferencePath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessDownloadReferencePath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_OCTET_STREAM));
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessDownloadReferencePath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_PDF));
    }

    @Test
    @WithUserS2021
    void shouldCheckS20201AccessDownloadResourcesPath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_RESOURCES))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS20201AccessDownloadReferencePath() throws Exception {
        mockMvc.perform(get(DOWNLOAD_REFERENCE))
            .andExpect(status().isForbidden());
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method downloadResources = PageController.class.getMethod("downloadResources");
        Method downloadReference = PageController.class.getMethod("downloadReference");

        assertTrue(downloadResources.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadReference.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
