package com.gvozdev.securityservice.util.path.thirdparty.spring2021;

public class Path {
    public static final String ACTUATOR = """
        /api/year/2021/spring/actuator/health""";

    public static final String SWAGGER_UI = """
        /api/year/2021/spring/swagger-ui""";

    private Path() {
    }
}
