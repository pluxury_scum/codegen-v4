package com.gvozdev.securityservice.config.annotation;

import com.gvozdev.securityservice.config.contextfactory.UserA20201ContextFactory;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@WithSecurityContext(factory = UserA20201ContextFactory.class)
public @interface WithUserA20201 {
}
