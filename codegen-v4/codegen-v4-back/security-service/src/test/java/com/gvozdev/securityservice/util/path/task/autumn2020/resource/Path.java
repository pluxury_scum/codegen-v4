package com.gvozdev.securityservice.util.path.task.autumn2020.resource;

public class Path {
    public static final String DOWNLOAD_RESOURCES = """
        /api/tasks/year/2020/autumn/download/resources""";

    public static final String DOWNLOAD_REFERENCE = """
        /api/tasks/year/2020/autumn/download/reference""";

    private Path() {
    }
}
