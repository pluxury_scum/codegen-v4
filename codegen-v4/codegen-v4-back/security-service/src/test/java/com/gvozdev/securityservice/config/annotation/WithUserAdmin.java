package com.gvozdev.securityservice.config.annotation;

import com.gvozdev.securityservice.config.contextfactory.UserAdminContextFactory;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@WithSecurityContext(factory = UserAdminContextFactory.class)
public @interface WithUserAdmin {
}
