package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.annotation.WithUserAdmin;
import com.gvozdev.securityservice.config.mockuser.MockUser;

import static com.gvozdev.securityservice.config.mockuser.MockUserFactory.createAdminUser;

public class UserAdminContextFactory extends AbstractWithSecurityContextFactory<WithUserAdmin> {

    @Override
    public MockUser getMockUser() {
        return createAdminUser();
    }
}
