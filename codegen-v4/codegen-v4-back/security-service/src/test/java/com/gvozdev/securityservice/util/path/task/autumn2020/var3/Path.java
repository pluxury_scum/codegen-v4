package com.gvozdev.securityservice.util.path.task.autumn2020.var3;

public class Path {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2020/autumn/var3/exercise-info?lat=55.0&lon=162&satnum=7""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2020/autumn/var3/download/listing/cpp/file/withoop/main.cpp?lat=55.0&lon=162&satnum=7""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2020/autumn/var3/download/charts?lat=55.0&lon=162&satnum=7""";

    private Path() {
    }
}
