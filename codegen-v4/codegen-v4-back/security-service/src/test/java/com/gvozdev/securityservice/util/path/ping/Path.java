package com.gvozdev.securityservice.util.path.ping;

public class Path {
    public static final String GATEWAY_SERVICE = """
        /api/ping/service/gateway""";

    public static final String DATA_INITIALIZER_SERVICE = """
        /api/ping/service/data-initializer""";

    public static final String AUTUMN2020_SERVICE = """
        /api/ping/service/autumn2020""";

    public static final String SPRING2021_SERVICE = """
        /api/ping/service/spring2021""";

    private Path() {
    }
}
