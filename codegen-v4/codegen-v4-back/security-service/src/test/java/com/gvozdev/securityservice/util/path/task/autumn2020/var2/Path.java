package com.gvozdev.securityservice.util.path.task.autumn2020.var2;

public class Path {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2020/autumn/var2/exercise-info/POTS_6hours.dat""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2020/autumn/var2/download/listing/cpp/file/withoop/POTS_6hours.dat/main.cpp""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2020/autumn/var2/download/charts/POTS_6hours.dat""";

    private Path() {
    }
}
