package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.annotation.WithUserA20203;
import com.gvozdev.securityservice.config.mockuser.MockUser;

import static com.gvozdev.securityservice.config.mockuser.MockUserFactory.createA20203User;

public class UserA20203ContextFactory extends AbstractWithSecurityContextFactory<WithUserA20203> {

    @Override
    public MockUser getMockUser() {
        return createA20203User();
    }
}
