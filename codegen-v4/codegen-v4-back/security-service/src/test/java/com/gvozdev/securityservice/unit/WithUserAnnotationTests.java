package com.gvozdev.securityservice.unit;

import com.gvozdev.securityservice.config.annotation.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

import static com.gvozdev.securityservice.domain.Authority.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@SpringBootTest
@AutoConfigureMockMvc
class WithUserAnnotationTests {

    @Test
    @WithUserAdmin
    void shouldCheckWithUserAdminAnnotation() {
        Authentication authentication = getContext().getAuthentication();

        List<? extends GrantedAuthority> actualAuthorities = authentication.getAuthorities()
            .stream()
            .toList();
        List<SimpleGrantedAuthority> expectedAuthorities = List.of(
            new SimpleGrantedAuthority(ROLE_ADMIN.name())
        );

        assertEquals(expectedAuthorities, actualAuthorities);
    }

    @Test
    @WithUserA20201
    void shouldCheckWithUserA20201Annotation() {
        Authentication authentication = getContext().getAuthentication();

        List<? extends GrantedAuthority> actualAuthorities = authentication.getAuthorities()
            .stream()
            .toList();
        List<SimpleGrantedAuthority> expectedAuthorities = List.of(
            new SimpleGrantedAuthority(ROLE_A20201.name())
        );

        assertEquals(expectedAuthorities, actualAuthorities);
    }

    @Test
    @WithUserA20202
    void shouldCheckWithUserA20202Annotation() {
        Authentication authentication = getContext().getAuthentication();

        List<? extends GrantedAuthority> actualAuthorities = authentication.getAuthorities()
            .stream()
            .toList();
        List<SimpleGrantedAuthority> expectedAuthorities = List.of(
            new SimpleGrantedAuthority(ROLE_A20202.name())
        );

        assertEquals(expectedAuthorities, actualAuthorities);
    }

    @Test
    @WithUserA20203
    void shouldCheckWithUserA20203Annotation() {
        Authentication authentication = getContext().getAuthentication();

        List<? extends GrantedAuthority> actualAuthorities = authentication.getAuthorities()
            .stream()
            .toList();
        List<SimpleGrantedAuthority> expectedAuthorities = List.of(
            new SimpleGrantedAuthority(ROLE_A20203.name())
        );

        assertEquals(expectedAuthorities, actualAuthorities);
    }

    @Test
    @WithUserS2021
    void shouldCheckWithUserS2021Annotation() {
        Authentication authentication = getContext().getAuthentication();

        List<? extends GrantedAuthority> actualAuthorities = authentication.getAuthorities()
            .stream()
            .toList();
        List<SimpleGrantedAuthority> expectedAuthorities = List.of(
            new SimpleGrantedAuthority(ROLE_S2021.name())
        );

        assertEquals(expectedAuthorities, actualAuthorities);
    }
}
